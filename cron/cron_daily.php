<?php

set_include_path(__DIR__ . "/..");
require_once("includes/startup.php");

// Once a day, reset all user's record report counts to the base (10)
User::ResetAllRecordReports();
