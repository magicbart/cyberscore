<?php
require_once("includes/startup.php");

Authorization::authorize('Admin');

$league = intval($_GET['league'] ?? 0);
$league_info = db_get_result(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league]));
$game = $league_info['game_id'];

$users_involved = database_select("
  SELECT users.user_id, users.username
  FROM league_maybe
  JOIN users USING(user_id)
  WHERE game_id = ?
", 'i', [$game]);

$page_title = 'League Admin';
?>

<?php require_once("includes/html_top.php"); ?>

<div id="pageleft">
  <div id="breadcrumb" class="clearfix">
    <a href="/">Home</a></a>
  </div>

  <h1>Users Involved</h1>

  <table class="scoreboard" id="scoreboard_classic">
    <?php while ($user = db_get_result($users_involved)) { ?>
    <tr>
      <td><?php echo $user['username'] ?></td>
      <td><?php echo $user['user_id'] ?></td>
    </tr>
    <?php } ?>
  </table>

  <br><br>
  Type here the user id of the person you want to be the leader <br><br>
  <form action="/leagues/update.php?league=<?php echo $league ?>" method="post">
    <input type="hidden" name="section" value="leader" />
    <input type="text" name="leader_id" value="" size="10" />
    <input type="submit" value="Save" />
  </form>
</div>

<?php require_once("includes/html_bottom.php"); ?>
