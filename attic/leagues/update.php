<?php
require_once("../includes/startup.php");

$league = intval($_GET['league'] ?? 0);
$league_info = db_get_result(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league]));
$iammanager = $logged_in && $league_info['manager_id'] == $user_id;

if (!$league_info) {
  echo "not found";
  exit;
}

$section = $_POST['section'] ?? '';

if ($section == 'settings') {
  if (!$iammanager) {
    header("Location: /index.php");
    exit;
  }

  //save each of the values
  $length = intval($_POST['length']);
  $start = intval($_POST['start']);
  $round_length = intval($_POST['round_length']);

  if ($length < 3 || $length > 10) {
    $length = 4;
  }

  if ($start < 1 || $start > 14) {
    $start = 3;
  }

  if ($round_length < 3 || $round_length > 14) {
    $round_length = 7;
  }

  database_update_by(
    'league_manager',
    [
      'name' => $_POST['league_name'],
      'length' => $length,
      'gap' => $start,
      'time' => $round_length,
      'notes' => $_POST['notes'],
    ],
    ['league_id' => $league, 'manager_id' => $user_id]
  );

  header("Location: /leagues/view.php?league=$league");
}

if ($section == 'challenges') {
  if (!$iammanager) {
    header("Location: /index.php");
    exit;
  }

  $i = 1;
  while ($i < $league_info['length'] + 1) { // 1-n round
    $j = 1;
    while ($j < 4) { // 1-3 challenge
      $this_info = ${'p_round' . $i . 't' . $j . 'a'};
      $this_chart = ${'p_round' . $i . 't' . $j . 'b'};
      $this_chart = $this_chart == '' ? "NULL" : intval($this_chart);

      if (db_num_rows(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $j])) == 0) {
        if ($this_info != "") {
          database_insert('league_charts', ['league_id' => $league, 'round' => $i, 'number' => $j, 'information' => $this_info, 'chart_id' => $this_chart]);
        }
      } else {
        database_update_by('league_charts', ['information' => $this_info, 'chart_id' => $this_chart], ['league_id' => $league, 'round' => $i, 'number' => $j]);
      }

      $j++;
    }

    $i++;
  }

  header("Location: /leagues/view.php?league=$league");
}

if ($_GET['section'] == 'leader') {
  if (!Authorization::has_role('Admin')) {
    header("Location: /index.php");
    exit;
  }

  $leader_id = intval($_POST['leader_id'] ?? 0);
  if ($leader_id != 0) {
    database_update_by('league_manager', ['manager_id' => $leader_id], ['league_id' => $league, 'finished' => 0]);
  }

  header("Location: /leagues/admin.php?league=$league");
}
