<?php
require_once("../includes/startup.php");

$league = intval($_GET['league'] ?? 0);
$league_info = db_get_result(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league]));
$game_id = $league_info['game_id'];

if (!$league_info) {
  echo "not found";
  exit;
}

if ($logged_in) {
  // If user is in league
  if (db_num_rows(database_select("SELECT * FROM league_maybe WHERE user_id = ? AND league_id = ?", 'ii', [$user_id, $league])) == 1) {

    // ... if user is not the manager and the league hasn't started?
    if (db_num_rows(database_select("SELECT * FROM league_manager WHERE league_id = ? AND finished < 2 AND manager_id != ?", 'ii', [$league, $user_id])) == 1) {
      database_delete('league_maybe', 'user_id = ? AND league_id = ?', [$user_id, $league]);
    }
  } else {
    // TODO can you join midleague?
    database_insert('league_maybe', ['user_id' => $user_id, 'game_id' => $game_id, 'league_id' => $league]);
  }

  header("Location: /leagues/select.php");
} else {
  header("Location: /index.php");
}
?>
