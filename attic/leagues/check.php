<?php
require_once("../includes/startup.php");

Authorization::Authorize('Admin');

$page_title = 'League Check';

require_once("includes/html_top.php");
?>

<div id="pageleft">
	<div id="breadcrumb" class="clearfix">
		<a href="/">Home</a></a>
	</div>

<?php
$get_games_chosen = database_select("
  SELECT
    league_maybe.game_id,
    COUNT(league_maybe.league_maybe_id) AS voters,
    games.game_name
  FROM league_maybe
  JOIN games USING(game_id)
  GROUP BY league_maybe.game_id
  ORDER BY voters DESC, league_maybe.game_id ASC
", '', []);
?>

			<h1>Games chosen so far</h1>
      
<table>



</table>
     
      <table class="scoreboard" id="scoreboard_classic">
      <?php
			while($game_list = db_get_result($get_games_chosen)){
      $this_game = $game_list['game_id'];
      $voters = $game_list['voters'];
      $game_name = $game_list['game_name'];
      ?>
      <tr>
      <td>
      <a href="/game/<?echo $this_game?>"><?php echo $game_list['game_name'] ?></a>
      </td>
      <td>
      <?php echo $voters ?>
      </td>
      
      <td>
      [list of names]
      </td>
      
      <td>
      <?php
      if(db_num_rows(database_select("SELECT * FROM league_manager WHERE game_id = ? AND finished = 1", 'i', [$this_game])) == 1){
      ?>
      League in progress
      <?php
      } else if (db_num_rows(database_select("SELECT * FROM league_manager WHERE game_id = ? AND finished = 0", 'i', [$this_game])) == 1){
      $this_league = db_get_result(database_select("SELECT * FROM league_manager WHERE game_id = ? AND finished = 0", 'i', [$this_game]));
      
      if($this_league['manager_id'] == 0){
      ?>
      <a href="/leagues/admin.php?league=<?php echo $this_league['league_id']?>"><h3>Assign a leader</h3></a>
      <?php
      } else {
      ?>
      <h2>Leader assigned</h2>
      <?php     
      }
      
      
      } else {
      ?>
      <a href="/leagues/create.php?game=<?php echo $this_game?>">Start League</a>  
      <?php
      }
      ?>
      </td>
      
      </tr>
      <?php
          
      }
      ?>
      </table>
			
			
		


		</div>

<?php require_once("includes/html_bottom.php");?>
