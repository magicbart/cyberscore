<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$g_game_id = db_extract("SELECT game_id FROM predictions_matches WHERE match_id = $g_match_id");
list($match_name, $match_date, $match_teamA, $match_teamB, $match_countryA, $match_countryB) = db_extract("SELECT match_name, date, team_A, team_B, country_code_A, country_code_B FROM predictions_matches WHERE match_id = $g_match_id");
$page_title = $t['predictions_edit_match']." - ".$match_name;
        $countryA_id = db_extract("SELECT country_id FROM countries WHERE country_code = '$match_countryA'");
        $countryB_id = db_extract("SELECT country_id FROM countries WHERE country_code = '$match_countryB'");
$t->CacheCountryNames();
require_once("includes/html_top.php");
?>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div><br /><br />
<form action="predictionscript.php" method="post">
<input type="hidden" name="function" value="editmatch">
<input type="hidden" name="match_id" value="<?php echo $g_match_id;?>">
* <?php echo $t['predictions_create_match_name'];?> <input type="text" name="game_name" value="<?php echo $match_name;?>"><br /><br />
* <?php echo $t['predictions_create_match_date'];?> <input type="datetime-local" name="game_datetime" value="<?php echo $date;?>"> <small><?php echo $t['predictions_create_match_date_notice'];?></small><b> <?php echo $t['predictions_match_edit_notice'];?></b><br /><br />
* <?php echo $t['predictions_create_match_teamA'];?> <input type="text" name="teamA" value="<?php echo $match_teamA;?>"><br /><br />
* <?php echo $t['predictions_create_match_teamB'];?> <input type="text" name="teamB" value="<?php echo $match_teamB;?>"><br /><br />
<?php echo $t['predictions_create_match_type'];?>  <select id="match_type" name="match_type">
<option value=""><?php echo $t['general_na'];?></option>
<option value="group"><?php echo $t['predictions_create_match_date_group'];?></option>
<option value="knockout"><?php echo $t['predictions_create_match_type_knockout'];?></option>
</select><small><?php echo $t['predictions_create_match_points_notice'];?></small><br /><br /><hr><hr>
    <?php echo $t['predictions_create_match_country_notice'];?><br /><br />
	<label for="country_id"><?php echo $t['predictions_country_A'];?></label>
				<select id="country_id" name="country_id">
					<option value="1" id="blankcountry" ><?php echo $t->GetCountryName(1);?></option>
					<option value="1">------------------------------</option>
					<?php
					$translated_countries = $t->GetCountryNames();
					foreach($translated_countries as $country_id => $country_name)
					{
					?>
						<option <?php if($country_id == $countryA_id) echo 'selected="selected"';?> value="<?php echo $country_id;?>">
								<?php echo $country_name;?>
						</option>
					<?php
					}
					?>
				</select><br/>


   <label for="country_id2"><?php echo $t['predictions_country_B'];?></label>
				<select id="country_id2" name="country_id2">
					<option value="1" id="blankcountry" ><?php echo $t->GetCountryName(1);?></option>
					<option value="1">------------------------------</option>
					<?php
					$translated_countries = $t->GetCountryNames();
					foreach($translated_countries as $country_id => $country_name)
					{
					?>
						<option <?php if($country_id == $countryB_id) echo 'selected="selected"';?> value="<?php echo $country_id;?>">
								<?php echo $country_name;?>
						</option>
					<?php
					}
					?>
				</select><br/>
                
<br /><br /><input type="submit" name="submit" value="<?php echo $t['predictions_edit_match'];?>">
</form>

	</div>
<?php 
require_once("includes/html_bottom.php");?>
