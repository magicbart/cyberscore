<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");
$core_admin = db_extract("SELECT admin_user_id FROM predictions_games WHERE game_id = $g_game_id");
$page_title = $t['predictions_handle_users'];
$users = db_query("SELECT user_id, user_status FROM predictions_users WHERE game_id = $g_game_id ORDER BY user_status DESC");
require_once("includes/html_top.php");
?>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div><br /><br />
        <table class="zebra"><th><?php echo $t['general_username'];?></th><th><?php echo $t['general_userlist_user_roles'];?></th><th></th><th></th>
        <?php while($user = db_get_result($users)){ ?>
        <form action="predictionscript.php" method="post">
            <input type="hidden" name="function" value="changerole">
            <input type="hidden" name="game_id" value="<?php echo $g_game_id;?>">
            <input type="hidden" name="change_user_id" value="<?php echo $user['user_id'];?>">
            <tr>
                <td><a href="../stats/<?php echo $user['user_id'];?>"><?php echo $cs->GetUsername($user['user_id']);?></a></td>
                <td><?php if($core_admin == $user['user_id']) { echo $t['predictions_coread']; } 
                          else {?>  <select name="role">
                                        <option value="enrolled" <?php if($user['user_status'] == 'enrolled') echo 'selected="selected"';?>><?php echo $t['predictions_users_en'];?></option>
                                        <option value="barred" <?php if($user['user_status'] == 'barred') echo 'selected="selected"';?>><?php echo $t['predictions_users_ba'];?></option>
                                        <option value="administrator" <?php if($user['user_status'] == 'administrator') echo 'selected="selected"';?>><?php echo $t['predictions_ad'];?></option>
                                    </select></td>
                <td><input type="submit" name="submit" value="<?php echo $t['predictions_users_change'];?>"></form></td><td>
                         <?php if($user_id == $core_admin && $user['user_status'] == 'administrator') {?> 
                                    <form action="predictionscript.php" method="post">
                                        <input type="hidden" name="function" value="newcoreadmin">
                                        <input type="hidden" name="new_admin_id" value="<?php echo $user['user_id'];?>">
                                        <input type="hidden" name="game_id" value="<?php echo $g_game_id;?>">
                                        <input type="submit" name="submit" value="<?php echo $t['predictions_users_promote_coread'];?>" onclick="return confirm('<?php echo $t['predictions_users_notice'];?>')"> <?php } ?></td>
            </tr>                   </form><?php
            }
        } ?>
        </table>

	</div>
<?php 
require_once("includes/html_bottom.php");?>
