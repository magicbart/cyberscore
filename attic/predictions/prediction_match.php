<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");
$page_title = db_extract("SELECT match_name FROM predictions_matches WHERE match_id = $g_match_id"). " ".$t['predictions_match_page'];

$t->CacheCountryNames();
require_once("includes/html_top.php");


?>

<style>
td.column {
text-align: center;
width: 100px;
max-width: 250px;
margin-left: 150px;
margin-right: 150px;
}

td.centrecolumn {
text-align: center;
width: 100px;
max-width: 150px;
}
</style>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div><br />
        <h1>
        <?php echo $page_title;?>
        </h1>
        <br /><br />
        <?php list($teamA, $teamB, $countryA, $countryB, $time, $game_type) = db_extract("SELECT team_A, team_B, country_code_A, country_code_B, date, game_type FROM predictions_matches WHERE match_id = $g_match_id");
        list($resultA, $resultB, $subresultA, $subresultB, $match_winner, $finished) = db_extract("SELECT result_A, result_B, sub_result_A, sub_result_B, winner, result_finished FROM predictions_results WHERE match_id = $g_match_id");
        list($A, $B, $winner, $points) = db_extract("SELECT submission_A, submission_B, winning_team, points_awarded FROM predictions_records WHERE user_id = $user_id AND match_id = $g_match_id");
        $role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $g_game_id AND user_id = $user_id");
        $countryA_id = db_extract("SELECT country_id FROM countries WHERE country_code = '$countryA'");
        $countryB_id = db_extract("SELECT country_id FROM countries WHERE country_code = '$countryB'");?>

        <table class="zebra" style="text-align: center;">
        <tr style="text-align: center;">
            <td class="column" style="padding-left: 250px;"><?php if(!empty($countryA)) {?> <img src="../../flags/<?php echo $countryA;?>.png" alt="<?php echo $countryA;?>" title="<?php echo $t->GetCountryName($countryA_id);?>" /> <?php } ?></td>
            <td class="centrecolumn"><?php echo $time;?></td>
            <td class="column" style="padding-right: 250px;"> <?php if(!empty($countryB)) {?> <img src="../../flags/<?php echo $countryB;?>.png" alt="<?php echo $countryB;?>" title="<?php echo $t->GetCountryName($countryB_id);?>" /><?php } ?></td>
        </tr>
        <tr style="text-align: center;">
            <td class="column" style="padding-left: 250px;"><?php echo $teamA; ?></td>
            <td class="centrecolumn"><b>&ndash;</b></td>
            <td class="column" style="padding-right: 250px;"><?php echo $teamB;?></td>
        </tr>
        <?php if(isset($resultA) && isset($resultB)) {?>
        <tr style="text-align: center;">
            <td class="column"></td>
            <td class="centrecolumn"><b><?php if($finished != 'yes' && isset($resultA) && isset($resultB)) echo "Live Results"; else if($finished == 'yes' && isset($resultA) && isset($resultB)) echo $t['predictions_results'];?></b></td>
            <td class="column"></td>
        </tr>
        <tr style="text-align: center;">
            <td class="column" style="padding-left: 250px;"></td style="text-align: center; width: 100px;">
            <td class="centrecolumn"><b><?php echo $resultA;?></b><b> &ndash; </b><b><?php echo $resultB;?></b></td>
            <td class="column" style="padding-right: 250px;"></td>
        </tr><?php } ?>
        <tr style="text-align: center;">
            <td class="column" style="padding-left: 250px;"></td>
            <td class="centrecolumn"><b><?php echo $t['predictions_results_mine'];?></b></td>
            <td class="column" style="padding-right: 250px;"></td>
        </tr>
        <tr style="text-align: center;">
            <td class="column" style="padding-left: 250px;"></td>
            <td class="centrecolumn"><b><?php echo $A;?></b><b> &ndash; </b><b><?php echo $B;?></b></td>
            <td class="column" style="padding-right: 250px;"></td>
        </tr>

</table><br /><br /><br />
<hr>

<?php $new = new DateTime();
$old = new DateTime($time);
if($new > $old || $role == 'administrator'){?>
<h2>Predictions</h2>
<table class="zebra">   
                        <th style="text-align: center;"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="text-align: center;"><?php echo $t['general_user'];?></th>
                        <th style="text-align: center; padding-left: 150px;"><?php if(!empty($countryA)) {?> <img src="../../flags/<?php echo $countryA;?>.png" alt="<?php echo $countryA;?>" title="<?php echo $t->GetCountryName($countryA_id);?>" /> <?php } else echo $teamA;?><?php if(!empty($countryB)) {?> <img src="../../flags/<?php echo $countryB;?>.png" alt="<?php echo $countryB;?>" title="<?php echo $t->GetCountryName($countryB_id);?>" /> <?php } else echo $teamB;?></th>
<?php if($game_type == 'knockout') {?><th style="text-align: center;"><?php echo $t['predictions_winner'];?></th><?php } ?>
                        <th style="text-align: center; padding-left: 150px;"><?php echo $t['predictions_results_award'];?></th>
                        <th style="text-align: center;"><?php echo $t['predictions_results_total_award'];?></th>

                        

<?php
//First figure out the leaderboard following this match. Store them indexed by user.
$points = db_query("SELECT SUM(predictions_records.points_awarded) AS score, predictions_records.user_id FROM predictions_records JOIN predictions_matches USING(match_id) WHERE predictions_matches.date <= '$time' GROUP BY predictions_records.user_id ORDER BY score DESC, user_id DESC");
$i = 0;
$previous_score = -1;
while($point = db_get_result($points)) {
	$i++;//row, but not necessarily rank.
	if ($previous_score != $point['score'])
	{
		$rank = $i;//Set rank to current row number only if the score is different, otherwise it remains the same.
	}
	$new_leaderboard[$point['user_id']]['rank'] = $rank;
	$new_leaderboard[$point['user_id']]['score'] = $point['score'];
	$previous_score = $point['score'];
}

//Now figure out the leaderboard before this match. Store them indexed by user.
$lastpoints = db_query("SELECT SUM(predictions_records.points_awarded) AS score, predictions_records.user_id FROM predictions_records JOIN predictions_matches USING(match_id) WHERE predictions_matches.date < '$time' GROUP BY predictions_records.user_id ORDER BY score DESC, user_id DESC");
$first_match = false;
if (db_num_rows($lastpoints) == 0)
{
	//This is the first match, there is no previous leaderboard.
	$first_match = true;
}
$i = 0;
$previous_score = -1;
while($lastpoint = db_get_result($lastpoints)) {
	$i++;//row, but not necessarily rank.
	if ($previous_score != $lastpoint['score'])
	{
		$old_rank = $i;//Set rank to current row number only if the score is different, otherwise it remains the same.
	}
	$old_leaderboard[$lastpoint['user_id']]['rank'] = $old_rank;
	$previous_score = $lastpoint['score'];
}

//Now list the predictions for this match. To display the positions, refer to the arrays created by the previous loops.
$predictions = db_query("SELECT *, users.country_id, user_groups, username, countries.country_code, (IF(submission_A-$resultA < 0,256,submission_A-$resultA)+IF(submission_B-$resultB < 0,256,submission_B-$resultB)) AS distance FROM predictions_records LEFT JOIN users USING(user_id) LEFT JOIN countries ON users.country_id = countries.country_id WHERE match_id = $g_match_id ORDER BY points_awarded DESC, distance, submission_A, submission_B");//Distance is a bit of a work in progress. Better than randomly sorting the list before a match is finished, is sorting the users by how close their prediction is to being correct. This calculation should ideally put first those who have the current score, then those who have a possible but not current score (initial idea is to have these ordered by the number of goals that need to be scored to get to their prediction, which is a lot easier than trying to relate it to the outcome prediction somehow), and finally those whose predictions are now impossible.

while($prediction = db_get_result($predictions))
{
	$country_code = $prediction['country_code'];
    $country_id =  db_extract("SELECT country_id FROM countries WHERE country_code = $country_code");
	$user_groups = $prediction['user_groups'];
	$username = $prediction['username'];
	$rank = $new_leaderboard[$prediction['user_id']]['rank'];
	$old_rank = ( $first_match ? 1 : $old_leaderboard[$prediction['user_id']]['rank']);
	
	if($rank >= $old_rank) $rank_shift = ($rank - $old_rank);
	if($rank < $old_rank) $rank_shift = ($old_rank - $rank);
	$total_points = $new_leaderboard[$prediction['user_id']]['score'];



	?>
	<tr>
		<td style="text-align: center; width: 150px;"><h3><?php echo nth($rank);?></h3><?php if($rank < $old_rank) {echo '<span style="color: #32CD32;">(−'.$rank_shift.')</span>'; } 
																							  else if($rank > $old_rank) {echo '<span style="color: #ff0000;">(+'.$rank_shift.')</span>'; } 
																							  else if($rank_shift == 0) {} ?></td>
    <td style="text-align: center; width: 60px;"><a href="../../user/<?php echo $prediction['user_id'];?>"><img src="/userpics/<?php echo $cs->GetUserPic(array('user_id' => $prediction['user_id'], 'gender' => 'm'))?>" width="50" height="50" /></a></td>
    <td style="text-align: center; width: 30px; padding-left: 20px;"><a href="/scoreboards/medal?country=<?php echo $country_id;?>"><img src="/flags/<?php echo $country_code;?>.png" alt="<?php echo $country_code;?>" title="<?php echo $t->GetCountryName($country_id);?>" /></td>
    <td style="text-align: center; width: 50px;"><?php
                        $stars = $cs->GetStarIcons($user_groups, $prediction['user_id']);
							foreach($stars as $star)
							{
								$star = $t->FormatTranslatorStar($star, $username);
								?>
								<img src="<?php echo $star['src'];?>" title="<?php echo $star['title'];?>" alt="<?php echo $star['alt'];?>" />
							<?php } ?></td>
		<td style="text-align: center; width: 250px;"><h3><a href="../../user/<?php echo $prediction['user_id'];?>"><?php echo $cs->GetUsername($prediction['user_id']);?></a></h3></td>
		<td style="text-align: center; width: 100px; padding-left: 150px;"><h3><?php if(($prediction['submission_A'] > $prediction['submission_B'] && $game_type == 'group') || $game_type == 'knockout' && $prediction['winning_team'] == 'A') echo "<u>".$prediction['submission_A']."</u>"; else echo $prediction['submission_A'];?> – <?php if(($prediction['submission_B'] > $prediction['submission_A'] && $game_type == 'group') || $game_type == 'knockout' && $prediction['winning_team'] == 'B') echo "<u>".$prediction['submission_B']."</u>"; else echo $prediction['submission_B'];?></h3></td>
		<?php if($game_type == 'knockout'){?><td style="text-align: center; width: 150px;"><h3><?php if($prediction['winning_team'] == 'A') echo $teamA; else if($prediction['winning_team'] == 'B') echo $teamB;?></h3></td><?php } ?>
		<td style="text-align: center; width: 150px; padding-left: 150px;"><h3><?php switch($prediction['points_awarded'])
	{
					case 0: echo '<small>0 '.$t['predictions_points'].'</small>'; break;
                    case 1: echo '<small style="white-space:nowrap;color: #ffa500"">1 '.$t['predictions_point'].'</small>'; break;
					case 2: echo '<small style="color: #008000;">2 '.$t['predictions_points'].'</small>'; break;
					case 3: echo '<small style="color: #32CD32;">3 '.$t['predictions_points'].'</small>'; break;
					case 4: echo '<small style="color: #9400D3;">4 '.$t['predictions_points'].'</small>'; break;
					}?></h3></td>
		<td style="text-align: center; width: 150px;"><h3><small><?php echo $total_points . " "; if($total_points == 1) echo $t['predictions_point']; else echo $t['predictions_points'];?></small></h3></td>

		<td></td></tr>

	<?php
}?></table><?php }?>



        <?php /*
        * At the top, the necessary match info is displayed. This means team names, flags and the result / match ID is displayed large at the top of the page.
* The score is live-updated during the match (the updates being supplied by me), and the score should be displayed in italics when it is not final yet (same on matchlist).
* Underneath the match info, there's leaderboard showing the standings of the players after that match. 
* Leaderboard contains: rank, player, prediction, points, winner after extra time (only in knockout matches)
* Extra columns for places moved compared to the previous match's leaderboard, and for points earned for that match.
* This leaderboard does not change with the results of later matches.
* Predictions are hidden until the match starts.
*/?>




	</div>
<?php 
require_once("includes/html_bottom.php");?>
