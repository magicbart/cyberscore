<?php

render_with('sessions/new', [
  'page_title' => "Login",
  'referrer' => $_SERVER['HTTP_REFERER'] ?? "/",
]);
