<?php

Authorization::authorize('UnverifiedUser');

if ($current_user['user_id'] != $_GET['id']) {
  $cs->RedirectToPreviousPage();
}

// To avoid any issues with session.name, we're using
// setcookie directly instead of using the Cookies class.
Session::Clear('user_id');
setcookie(session_name(), "", -1, "/");
unset($_COOKIE[session_name()]);
session_destroy();

$cs->WriteNote(true, "Logged out");
redirect_to('/');
