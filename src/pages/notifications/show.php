<?php
Authorization::authorize('User');

$notification_id = intval($_GET['id']);
$notification = database_find_by('notifications', [
  'notification_id' => $notification_id,
  'user_id' => $current_user['user_id'],
]);

if ($notification == NULL) {
  not_found();
} else {
  NotificationsRepository::update($notification['notification_id'], ['note_seen' => 'y']);
  redirect_to(Notification::TargetURL($notification));
}
