<?php

Authorization::authorize('User');

//Initialise some stuff
$params['user_id'] = $current_user['user_id'];
$params['type'] = 'ygb';
$count = 0;

// Query for all first place charts.
$first_places = database_get_all(database_select("
SELECT level_id FROM records WHERE chart_pos = 1 AND user_id = ?",
's', 
[$current_user['user_id']]));

// Now we query to find notifications that are for charts where we're in first place. We prepare a filter with our first places query.
$level_filter = implode(",", array_column($first_places, 'level_id'));
$notifications = database_get_all(database_select("
SELECT chart_id FROM notifications WHERE chart_id IN($level_filter) AND user_id = ? AND type = 'ygb'",
's', 
[$current_user['user_id']]));


// Iterate on these pre-filtered notifications.
foreach($notifications AS $obsolete_notification)
{
    // Remove the obsolete notification. We also want to keep track of our obsolete count for status reporting.
    $params['chart_id'] = $obsolete_notification['chart_id'];
    database_delete_by('notifications', $params);
    $count++;
}

// Status Reporting
if($count == 0) {
  $cs->WriteNote(false, "There were no obsolete notifications to remove!");
}
else $cs->WriteNote(true, $count." obsolete notifications deleted!");

$cs->RedirectToPreviousPage();
