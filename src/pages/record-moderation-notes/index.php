<?php

Authorization::authorize('ProofMod');

$records = database_get_all(database_select("
  SELECT
    records.record_id, records.submission, records.submission2, records.last_update,
    records.record_note, records.record_note_date, records.record_note_mod_id,
    levels.level_id, levels.level_name, levels.chart_type, levels.chart_type2,
    levels.decimals, users.user_id, users.username,
    levels.num_decimals,
    levels.score_suffix,
    levels.score_prefix,
    levels.num_decimals2,
    levels.score_suffix2,
    levels.score_prefix2,
    moderators.username AS moderator_username
  FROM records
  JOIN levels USING (level_id)
  JOIN users USING (user_id)
  LEFT JOIN users moderators ON records.record_note_mod_id = moderators.user_id
  WHERE record_note != ''
  ORDER BY record_id ASC
", '', []));

render_with('record-moderation-notes/index', [
  'page_title' => 'Record Notes',
  'records' => $records,
]);
