<?php

$params = filter_post_params([
  Params::number('comment_id'),
  Params::str('commentable_type'),
]);

$comment = Comment::get($params['commentable_type'], $params['comment_id']);

Authorization::authorize('GlobalMod', $comment['user_id']);

Comment::delete($params['commentable_type'], $params['comment_id']);
$cs->WriteNote(true, $t['general_comments_delete']);

$cs->RedirectToPreviousPage();
