<?php

Authorization::authorize('Admin');

$countries = database_get_all(database_select("
  SELECT countries.*, COUNT(users.user_id) AS num_users
  FROM countries
  LEFT JOIN users USING (country_id)
  GROUP BY countries.country_id
  ORDER BY country_name ASC
", '', []));
foreach ($countries as &$country) {
  $country['continent_name'] = $t->GetContinentName($country['continent']);
}
unset($country);

$continents = [
  ['id' => 0, 'name' => $t->GetContinentName(0)],
  ['id' => 1, 'name' => $t->GetContinentName(1)],
  ['id' => 2, 'name' => $t->GetContinentName(2)],
  ['id' => 3, 'name' => $t->GetContinentName(3)],
  ['id' => 4, 'name' => $t->GetContinentName(4)],
  ['id' => 5, 'name' => $t->GetContinentName(5)],
  ['id' => 6, 'name' => $t->GetContinentName(6)],
];

render_with('admin/countries/index', [
  'page_title' => 'Countries',
  'countries' => $countries,
  'continents' => $continents,
]);
