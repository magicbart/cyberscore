<?php

Authorization::authorize('GameMod');

if ($_POST['scoreboard'] ?? null) {
  $scoreboard = $_POST['scoreboard'];
  switch ($scoreboard) {
  case 'starboard':          ScoreboardRebuildClass::RebuildMainboard(); break;
  case 'medal':              ScoreboardRebuildClass::RebuildMedalTable(); break;
  case 'arcade':             ScoreboardRebuildClass::RebuildArcadeCache(); break;
  case 'speedrun':           ScoreboardRebuildClass::RebuildSpeedrunCache(); break;
  case 'solution':           ScoreboardRebuildClass::RebuildSolutionCache(); break;
  case 'trophy':             ScoreboardRebuildClass::RebuildTrophyTable(); break;
  case 'submissions':        ScoreboardRebuildClass::RebuildTotalSubmissionsCache(); break;
  case 'proof':              ScoreboardRebuildClass::RebuildProofCache(); break;
  case 'vproof':             ScoreboardRebuildClass::RebuildVideoProofCache(); break;
  case 'challenge':          ScoreboardRebuildClass::RebuildChallengeCache(); break;
  case 'incremental':        ScoreboardRebuildClass::RebuildIncrementalCache(); break;
  case 'collectible':        ScoreboardRebuildClass::RebuildCollectibleCache(); break;
  case 'rainbow':            ScoreboardRebuildClass::RebuildRainbowScoreboard(); break;
  default:
    $cs->PageNotFound();
  }

  Flash::AddSuccess("Rebuilt $scoreboard cache");
}

if ($_POST['game_id'] ?? null) {
  $game_id = intval($_POST['game_id']);

  GameRebuildClass::RebuildGame($game_id);
  database_delete_by('cacherebuilder', ['game_id' => $game_id]);

  Flash::AddSuccess("Rebuilt game ID #$game_id.");
}

if ($_POST['from_game_id'] ?? null && $_POST['to_game_id'] ?? null) {
  $from_game_id = intval($_POST['from_game_id']);
  $to_game_id = intval($_POST['to_game_id']);

  $game_rebuild->QueueGameRangeForRebuild($from_game_id, $to_game_id);
  Flash::AddSuccess("Queued games between ID #$from_game_id & #$to_game_id for rebuild.");
}

if ($_POST['every_game'] ?? null) {
  $game_rebuild->QueueAllGamesForRebuild();
  Flash::AddSuccess("Queued all games for rebuild.");
}

if ($_POST['chart_id'] ?? null) {
  $chart_id = intval($_POST['chart_id']);
  $chart_rebuild->RebuildChart($chart_id);
  Flash::AddSuccess("Rebuilt chart ID #$chart_id.");
}

if ($_POST['from_chart_id'] ?? null && $_POST['to_chart_id'] ?? null) {
  $from_chart_id = intval($_POST['from_chart_id']);
  $to_chart_id = intval($_POST['to_chart_id']);

  $chart_rebuild->QueueChartRangeForRebuild($from_chart_id, $to_chart_id);
  Flash::AddSuccess("Queued charts between ID #$from_chart_id & #$to_chart_id for rebuild.");
}

if ($_POST['every_chart'] ?? null) {
  $chart_rebuild->QueueAllChartsForRebuild();
  Flash::AddSuccess("Queued all charts for rebuild.");
}

$cs->RedirectToPreviousPage();
