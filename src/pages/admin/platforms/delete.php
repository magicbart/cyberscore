<?php

Authorization::authorize('Admin');

$platform_id = intval($_GET['id']);

$game_count = database_single_value("
  SELECT COUNT(*)
  FROM game_platforms
  WHERE platform_id = ?
", 's', [$platform_id]);

$record_count = database_single_value("
  SELECT COUNT(*)
  FROM records
  WHERE platform_id = ?
", 's', [$platform_id]);

if ($game_count + $record_count > 0) {
  $cs->WriteNote(false, "Unable to delete platform because it is in use.");
} else {
  database_delete_by('platforms', ['platform_id' => $platform_id]);
  $cs->WriteNote(true, "Platform deleted");
}

redirect_to('/admin/platforms');
