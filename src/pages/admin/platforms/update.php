<?php

Authorization::authorize('Admin');

$platform = database_find_by('platforms', ['platform_id' => $_GET['id']]);

if (!$platform) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('manufacturer'),
  Params::str('platform_name'),
  Params::str('short_name'),
]);

database_update_by('platforms', $params, ['platform_id' => $platform['platform_id']]);

$cs->WriteNote(true, 'Platform updated');
redirect_to('/admin/platforms');
