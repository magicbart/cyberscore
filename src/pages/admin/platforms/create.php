<?php

Authorization::authorize('Admin');

$params = filter_post_params([
  Params::str('manufacturer'),
  Params::str('platform_name'),
  Params::str('short_name'),
]);

$platform_id = database_insert('platforms', $params);
$cs->WriteNote(true, 'Platform added');

if (is_uploaded_file($_FILES['icon']['tmp_name'])) {
  $img = @imagecreatefromgif($_FILES['icon']['tmp_name']);

  if (!$img) {
    $cs->WriteNote(false, 'Failed to upload icon: image must be a 88x31 GIF');
  } else {
    $w = imagesx($img);
    $h = imagesy($img);

    if ($w != 88 || $h != 31) {
      $cs->WriteNote(false, 'Failed to upload icon: image must be 88x31 pixels');
    } else if (!move_uploaded_file($_FILES['icon']['tmp_name'], "{$config['app']['root']}/public/uploads/platforms/$platform_id.gif")) {
      $cs->WriteNote(false, "Failed to upload icon: couldn't save file to disk");
    }
  }
}

redirect_to('/admin/platforms');
