<?php

Authorization::authorize('GameMod');

require_once("includes/cron_update_calculator.php");

$games = database_get_all(database_select("
  SELECT games.*
  FROM games
  JOIN cacherebuilder USING (game_id)
", '', []));

$scoreboard_rebuilders = [
  ['scoreboard' => 'starboard',   'name' => 'Starboard'],
  ['scoreboard' => 'medal',       'name' => 'Medal'],
  ['scoreboard' => 'arcade',      'name' => 'Arcade'],
  ['scoreboard' => 'speedrun',    'name' => 'Speedrun'],
  ['scoreboard' => 'solution',    'name' => 'Solution'],
  ['scoreboard' => 'trophy',      'name' => 'Trophy'],
  ['scoreboard' => 'submissions', 'name' => 'Total Submissions'],
  ['scoreboard' => 'proof',       'name' => 'Proof'],
  ['scoreboard' => 'vproof',      'name' => 'Video Proof'],
  ['scoreboard' => 'challenge',   'name' => 'Challenge'],
  ['scoreboard' => 'collectible',      'name' => 'Collectible'],
  ['scoreboard' => 'incremental',   'name' => 'Incremental'],
  ['scoreboard' => 'rainbow',     'name' => 'Rainbow'],
];

render_with('admin/rebuild_center', [
  'page_title' => 'Rebuild center',
  'games' => $games,
  'scoreboard_rebuilders' => $scoreboard_rebuilders,
  'time_to_next_rebuild' => [
    'game' => Rebuilders::game()->time_to_next(),
    'chart' => Rebuilders::chart()->time_to_next(),
  ],
]);
