<?php

Authorization::authorize('GlobalMod');

$games = database_get_all(database_select("
  SELECT game_name, game_id, GROUP_CONCAT(user_id SEPARATOR ',') AS user_ids
  FROM games
  JOIN records USING (game_id)
  GROUP BY game_id
  ORDER BY game_name ASC
", '', []));

render_with('admin/csmails/new', [
  'page_title' => 'Multi CS Mail',
  'games' => $games,
]);
