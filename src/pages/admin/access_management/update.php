<?php

Authorization::authorize('Admin');

$params = filter_post_params([
  Params::array_of_numbers('user_group'),
]);

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->WriteNote(false, "User does not exist.");
  $cs->RedirectToPreviousPage();
}

if ($user['user_groups'] == 0) {
  $cs->WriteNote(false, "User with no access. Add them instead.");
  $cs->RedirectToPreviousPage();
}

$new_user_group = array_sum($params['user_group']);

database_update_by(
  'users',
  ['user_groups' => $new_user_group],
  ['user_id' => $user['user_id']]
);

if ($new_user_group == 0) {
  database_update_by(
    'staff_history',
    ['end_date' => database_now()],
    ['staff_id' => $user['user_id']]
  );
}

$cs->WriteNote(true, "User access updated");
$cs->RedirectToPreviousPage();
