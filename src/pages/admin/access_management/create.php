<?php

Authorization::authorize('Admin');

$params = filter_post_params([
  Params::number('user_id'),
  Params::number('user_group'),
]);

$user = UsersRepository::get($params['user_id']);

if (!$user) {
  $cs->WriteNote(false, "User does not exist.");
  $cs->RedirectToPreviousPage();
}

if ($user['user_groups'] != 0) {
  $cs->WriteNote(false, "User already has access. Edit it instead.");
  $cs->RedirectToPreviousPage();
}

database_update_by(
  'users',
  ['user_groups' => $params['user_group']],
  ['user_id' => $params['user_id']]
);

database_insert('staff_history', ['staff_id' => $user['user_id'], 'start_date' => database_now()]);

$cs->RedirectToPreviousPage();
