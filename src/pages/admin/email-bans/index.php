<?php

Authorization::authorize('Dev');

$email_bans = EmailBansRepository::all();

render_with('admin/email-bans/index', [
  'email_bans' => $email_bans,
]);
