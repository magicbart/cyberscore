<?php

Authorization::authorize('Guest');

$user = database_find_by('users', ['registration_token' => $_GET['token']]);

if ($user == NULL || $user['registration_token_expiration'] < time()) {
  $cs->WriteNote(false, $t['register_confirmation_failure']);
  $cs->LeavePage("/");
} else {
  UsersRepository::update($user['user_id'], [
    'registration_token' => '',
    'registration_token_expiration' => 0,
    'auth_verified' => 'y',
  ]);

  $cs->WriteNote(true, $t['register_confirmation_successful']);
  $cs->LeavePage("/");
}
