<?php

Authorization::authorize('Guest');

$view_user_id   = $_GET['user_id'] ?? $current_user['user_id'];
$selectsort     = $_GET['selectsort'] ?? 7;
$only_platinums = $_GET['platinums'] ?? 'no';
$proof_filter   = $_GET['proof_type'] ?? 'all';
$type_filter    = $_GET['chartstatus'] ?? 'all';
$status_filter  = $_GET['selectstatus'] ?? -1;
$reload         = $_GET['reload'] ?? 'no';

$games = $_GET['games'] ?? "";

if (is_string($games) && !empty($games)) {
  $games = array_map(fn($g) => intval($g), explode(',', $games));
} else if (is_array($games)) {
  $games = array_map(fn($g) => intval($g), $games);
} else {
  $games = [];
}

$filter_comp   = $_GET['filter_comp'] ?? NULL;
$filter_field  = $_GET['filter_field'] ?? NULL;
$filter_value  = !empty($_GET['filter_value']) ? floatval($_GET['filter_value']) : NULL;
if (!in_array($filter_field, ['csp', 'ucsp', 'chart_pos', 'chart_subs'])) {
  $filter_field = NULL;
}

// If we're scheduled for a reload (which we manually enforce on the form selector), we reload the page with our sanitised filters.
if (isset($reload) && $reload == 'yes') {
  // Redirect to pretty up URL, we only want what we actually need. GET forms are a little overzealous.
  $all_filters = "user_id=$view_user_id";
  $selectsort != 7 ? $all_filters .= "&selectsort=$selectsort" : '';
  $only_platinums != 'no' ? $all_filters .= "&platinums=$only_platinums" : '';
  $proof_filter != 'all' ? $all_filters .= "&proof_type=$proof_filter" : '';
  $type_filter != 'all' ? $all_filters .= "&chartstatus=$type_filter" : '';
  $status_filter != -1 ? $all_filters .= "&selectstatus=$status_filter" : '';

  // Handle the games filter
  if (is_array($_GET['games'] ?? null)) {
    $all_filters .= "&games=" . implode(',', $_GET['games']);
  }

  // Handle custom operational filters
  $filter_value != NULL ? $all_filters .= "&filter_field=$filter_field&filter_comp=$filter_comp&filter_value=$filter_value" : '';

  // Redirect to a cleaned URL
  $cs->LeavePage("/records?$all_filters");
}

$user = UsersRepository::get($view_user_id);
if (!$user) {
  $cs->LeavePage('/', 'User not found.');
}

$filters = ["1=1"];
$title = [];

foreach (Modifiers::$chart_flags as [$chart_flag,]) {
  if ($chart_flag == $type_filter) {
    $filters []= "chart_modifiers2.{$type_filter}";
  }
  else if ($type_filter == 'all_ranked')
  {
    $filters []= "NOT chart_modifiers2.unranked";
  }
}

// record status
$status = [
  0 => [[0,1,2], "unproven records"],
  1 => [[1,5], "records pending investigation"],
  2 => [[2,6], "records under investigation"],
  3 => [[3], "approved records"],
  4 => [[4,5,6], "records pending approval"],
];

if ($status_filter >= 0) {
  $filters []= "records.rec_status IN (" . implode(",", $status[$status_filter][0]) . ")";
  $title []= $status[$status_filter][1];
} else {
  $title []= "records";
}

// Custom filters?
$operators = [
  'b' => '>',
  'w' => '<',
  'e' => '=',
  'be' => '>=',
  'we' => '<=',
];
$filter_sign = $operators[$filter_comp] ?? '>';

if (isset($filter_value)) {
  $filters []= "records.$filter_field $filter_sign $filter_value";
}

switch ($selectsort) {
case 1:
  $sort_order = "records.chart_pos ASC, records.platinum DESC, records.csp DESC";
  $title []= 'sorted by best chart position';
  break;
case 2:
  $sort_order = "records.chart_pos DESC, records.platinum ASC, records.csp ASC";
  $title []= 'sorted by worst chart position';
  break;
case 3:
  $sort_order = "records.csp ASC";
  $title []= 'sorted by worst CSP value';
  break;
case 4:
  $sort_order = "records.csp DESC";
  $title []= 'sorted by best CSP value';
  break;
case 5:
  $sort_order = "records.last_update ASC";
  $title []= 'sorted by oldest submission date';
  break;
case 6:
  $sort_order = "records.last_update DESC";
  $title []= 'sorted by latest submission date';
  break;
case 7:
  $sort_order = "games.game_name ASC, level_groups.group_pos ASC, levels.level_pos ASC";
  $title []= 'sorted by game name ascending';
  break;
case 8:
  $sort_order = "games.game_name DESC, level_groups.group_pos DESC, levels.level_pos DESC";
  $title []= 'sorted by game name descending';
  break;
case 9:
  $sort_order = "records.ucsp DESC";
  $title []= 'sorted by UCSP descending';
  break;
case 10:
  $sort_order = "records.ucsp ASC";
  $title []= 'sorted by UCSP ascending';
  break;
default:
  $sort_order = "records.chart_pos ASC, records.platinum DESC, records.csp DESC";
  $title []= 'sorted by best chart position';
  break;
}

switch ($proof_filter) {
case "all":
  break;
case "pictures":
  $filters []= "records.proof_type = 'p' AND records.rec_status = 3";
  $title []= 'filtered by approved photo proofs';
  break;
case "videos":
  $filters []= "(records.proof_type = 'v' OR records.proof_type = 's' OR records.proof_type = 'r') AND records.rec_status = 3";
  $title []= 'filtered by approved video/livestream/replay proofs';
  break;
}

if ($only_platinums == 'yes') {
  $filters []= "records.platinum = 1";
}

if (!empty($games)) {
  $game_filter = implode(",", $games);
  $filters []= "records.game_id IN ($game_filter)";

  if (count($games) == 1) {
    $title []= 'for ' . $t->GetGameName($games[0]);
  } else {
    $title []= 'for ' . count($games) . ' selected games';
  }
}


$query_filter = implode(" AND ", $filters);
$records = database_get_all(database_select("
    SELECT
        records.record_id,
        records.rec_status,
        records.proof_type,
        records.linked_proof,
        records.chart_pos,
        records.chart_subs,
        records.comment,
        records.submission,
        records.submission2,
        records.last_update,
        records.csp,
        records.ucsp,
        records.platinum,
        records.user_id,
        records.ranked,
        chart_modifiers2.*,
        levels.game_id,
        levels.group_id,
        levels.level_id,
        levels.chart_type,
        levels.num_decimals,
        levels.score_prefix,
        levels.score_suffix,
        levels.chart_type2,
        levels.num_decimals2,
        levels.score_prefix2,
        levels.score_suffix2,
        levels.conversion_factor,
        levels.decimals,
        levels.ranked,
        MAX(top_records.submission) as top_submission,
        MAX(top_records.submission2) as top_submission2
    FROM records
    JOIN games USING(game_id)
    JOIN levels USING(level_id)
    JOIN chart_modifiers2 USING(level_id)
    LEFT JOIN level_groups USING(group_id)
    LEFT JOIN records AS top_records ON (records.level_id = top_records.level_id AND top_records.chart_pos = 1)
    WHERE records.user_id = $view_user_id AND $query_filter
    GROUP BY records.record_id
    ORDER BY $sort_order
", '', []));

foreach ($records as &$record) {
  $t->CacheGame($record['game_id']);
  $record['awards'] = fetch_chart_awards($record['level_id']);
  $record['rank'] = Modifiers::ChartFlagColouration($record);
  $record['proof_details'] = ProofManager::ProofDetails($record);
  $record['proof_type'] = $cs->ReadProofType($record['proof_type']);
}
unset($record);

$title = $user['username'] . "'s " . count($records) . " " . implode(' ', $title);

render_with('records/index', [
  'page_title' => $title,
  'title' => $title,
  'user' => $user,
  'records' => $records,
]);
