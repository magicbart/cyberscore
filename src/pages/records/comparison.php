<?php

Authorization::authorize('Guest');

if (isset($_GET['usernames'])) {
  $usernames = explode(' ', $_GET['usernames']);
  if (count($usernames) >= 2) {
    $user1 = UsersRepository::find_by(['username' => $usernames[0]]);
    $user2 = UsersRepository::find_by(['username' => $usernames[1]]);
  }
} else {
  $user1 = UsersRepository::get($_GET['user_id1'] ?? NULL);
  $user2 = UsersRepository::get($_GET['user_id2'] ?? NULL);
}

if (!$user1 || !$user2) {
  $cs->WriteNote(false, 'Must specify two existing users');
  $cs->RedirectToPreviousPage();
}

if (isset($_GET['game_id'])) {
  $single_game_id = $_GET['game_id'];
  $game_ids = [$single_game_id];
} else if (isset($_GET['game_ids'])) {
  $single_game_id = $_GET['game_ids'][0] ?? 0;
  $game_ids = $_GET['game_ids'];
} else {
  $single_game_id = 0;
  $game_ids = [];
}

$all_games = database_get_all(database_select("
  SELECT game_id, game_name
  FROM games
  ORDER BY game_name ASC
", '', []));

$players_in_game_id = database_get_all(database_select("
  SELECT DISTINCT users.user_id, users.username
  FROM records
  JOIN users ON records.user_id = users.user_id
  WHERE records.game_id = ?
  ORDER BY username
", 'i', [$single_game_id]));

$games_in_common = database_get_all(database_select("
  SELECT games.game_id, game_name
  FROM records
  JOIN games ON records.game_id = games.game_id
  WHERE user_id IN (?, ?)
  GROUP BY game_id
  HAVING COUNT(DISTINCT user_id) = 2
  ORDER BY games.game_name ASC
", 'ii', [$user1['user_id'], $user2['user_id']]));

$require_both = isset($_GET['both']);

$games = [];

$totals = [
  'win1' => 0,
  'win2' => 0,
  'tie' => 0,
];

foreach ($game_ids as $game_id) {
  $t->CacheGame($game_id);
  $charts = database_get_all(database_select("
    SELECT levels.*
    FROM levels
    LEFT JOIN level_groups USING(group_id)
    WHERE levels.game_id = ?
    ORDER BY level_groups.group_pos ASC, level_groups.group_name ASC, levels.level_pos ASC, levels.level_name ASC
  ", 'i', [$game_id]));

  // now get the scores from the two people
  $record1 = index_by(database_get_all(database_select("SELECT * FROM records WHERE user_id = ? AND game_id = ?", 'ss', [$user1['user_id'], $game_id])), 'level_id');
  $record2 = index_by(database_get_all(database_select("SELECT * FROM records WHERE user_id = ? AND game_id = ?", 'ss', [$user2['user_id'], $game_id])), 'level_id');

  foreach ($charts as &$chart) {
    $r1 = $record1[$chart['level_id']] ?? null;
    $r2 = $record2[$chart['level_id']] ?? null;

    if ($r1) $r1 = array_merge($r1, $chart);
    if ($r2) $r2 = array_merge($r2, $chart);

    $chart['p1'] = $r1;
    $chart['p2'] = $r2;

    $chart_pos1 = $chart['p1']['chart_pos'] ?? null;
    $chart_pos2 = $chart['p2']['chart_pos'] ?? null;

    if (($chart_pos1 === null && $chart_pos2 === null) || ($require_both && ($chart_pos1 === null || $chart_pos2 === null))) {
      $chart['result'] = 'no-one';
      $color = '#000000';
    } else if ($chart_pos1 === $chart_pos2)	{
      $chart['result'] = 'tie';
      $totals['tie']++;
    } else if ($chart_pos1 === null) {
      $chart['result'] = 'win2';
      $totals['win2']++;
    } else if ($chart_pos2 === null) {
      $chart['result'] = 'win1';
      $totals['win1']++;
    } else if ($chart_pos1 > $chart_pos2) {
      $chart['result'] = 'win2';
      $totals['win2']++;
    } else if ($chart_pos1 < $chart_pos2) {
      $chart['result'] = 'win1';
      $totals['win1']++;
    }
  }
  unset($chart);

  $games []= ['game_id' => $game_id, 'charts' => $charts];
}

render_with('records/comparison', [
  'page_title' => t('compare_title'),
  'user1' => $user1,
  'user2' => $user2,
  'games' => $games,
  'games_in_common' => $games_in_common,
  'game_ids' => $game_ids,
  'require_both' => $require_both,
  'totals' => $totals,
]);
