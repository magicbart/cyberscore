<?php

$requests = database_get_all(database_select("
  SELECT
    game_requests.gamereq_id,
    submitter_id,
    submitter.user_id,
    submitter.username,
    game_name, date_requested,
    game_id,
    handled,
    MAX(comment_time) AS last_comment_time,
    COUNT(comment_time) AS comment_count
  FROM game_requests
  LEFT JOIN users submitter ON submitter.user_id = game_requests.submitter_id
  LEFT JOIN request_comments ON (request_comments.gamereq_id = game_requests.gamereq_id)
  WHERE handled = 0
  GROUP BY game_requests.gamereq_id
  ORDER BY game_requests.gamereq_id ASC
", '', []));

render_with('game_requests/index', [
  'page_title' => 'View game requests',
  'requests' => $requests,
]);
