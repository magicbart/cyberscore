<?php

// TODO: automatic "Separate charts" must go.
// We need to explicitly create a group_id for them.
if (str_starts_with($_GET['id'], 's')) {
  $game = GamesRepository::get(substr($_GET['id'], 1));
  $group = ['group_id' => 0, 'game_id' => $game['game_id']];
} else {
  $group = GroupsRepository::get($_GET['id']);
  $game = GamesRepository::get($group['game_id']);
}

if (!$group) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('name_string_filter'),
  Params::str('sort_alphabetically'),
  Params::number('sort_source_group_id'),
  Params::number('dlc'),
  Params::array_of_numbers('import_chart_ids'),
  Params::number('lang_id'),
  Params::enum('advanced', ['normal', 'game']),
]);

if (isset($params['name_string_filter'])) {
  $charts = LevelsRepository::where(['game_id' => $game['game_id'], 'group_id' => $group['group_id']]);

  foreach ($charts as $chart) {
    $new_name = preg_replace("/\s+/", " ", trim(str_replace($params['name_string_filter'], "", $chart['level_name'])));

    if ($new_name != $chart['level_name']) {
      database_update_by('levels', ['level_name' => $new_name], ['level_id' => $chart['level_id']]);
    }
  }
}

if (isset($params['sort_alphabetically'])) {
  $charts = LevelsRepository::where(['game_id' => $game['game_id'], 'group_id' => $group['group_id']]);

  usort($charts, fn($a, $b) => strtolower($a['level_name']) <=> strtolower($b['level_name']));

  foreach ($charts as $i => $chart) {
    database_update_by('levels', ['level_pos' => $i + 1], ['level_id' => $chart['level_id']]);
  }
}

if (isset($params['sort_source_group_id'])) {
  $charts = LevelsRepository::where(['game_id' => $game['game_id'], 'group_id' => $group['group_id']]);
  $other_group = GroupsRepository::get($params['sort_source_group_id']);
  $other_charts = index_by(
    LevelsRepository::where(['game_id' => $game['game_id'], 'group_id' => $params['sort_source_group_id']]),
    'level_name',
    'level_pos'
  );

  if (!$other_group) {
    $cs->WriteNote(false, "Group not found");
    $cs->RedirectToPreviousPage();
  }

  foreach ($charts as $chart) {
    if (!isset($other_charts[$chart['level_name']])) {
      $cs->WriteNote(false, "Couldn't find all chart names from the current group in the other group");
      $cs->RedirectToPreviousPage();
    }
  }

  foreach ($charts as $chart) {
    $pos = $other_charts[$chart['level_name']];
    database_update_by('levels', ['level_pos' => $pos], ['level_id' => $chart['level_id']]);
  }

  $cs->WriteNote(true, "Chart sorting copied from " . h($other_group['group_name']) . "'");
}

if (isset($params['dlc'])) {
  if ($params['dlc'] != 0 && $params['dlc'] != 1) {
    $cs->WriteNote(false, "Invalid DLC value.");
    $cs->RedirectToPreviousPage();
  }

  database_update_by(
    'levels',
    ['dlc' => $params['dlc']],
    ['game_id' => $game['game_id'], 'group_id' => $group['group_id']]
  );
  GameRebuildClass::QueueGameForRebuild($game['game_id']);

  $cs->WriteNote(true, "DLC flag changed.");
}

if (!empty($params['import_chart_ids'])) {
  $max_pos = database_single_value("
    SELECT MAX(level_pos) FROM levels WHERE game_id = ? AND group_id = ?
  ", 'ss', [$game['game_id'], $group['group_id']]);

  $current_chart_ids = pluck(database_get_all(database_select("
    SELECT level_id FROM levels WHERE game_id = ? AND group_id = ?
  ", 'ss', [$game['game_id'], $group['group_id']])), 'level_id');

  // no deselection!
  $new_chart_ids = array_diff($params['import_chart_ids'], $current_chart_ids);

  foreach ($new_chart_ids as $i => $chart_id) {
    database_update_by('levels', [
      'level_pos' => $max_pos + $i + 1,
      'group_id' => $group['group_id'],
    ], ['level_id' => $chart_id]);
  }
  // TODO: this may disrupt the continuity of level_pos in the groups that lose these charts.
  GameRebuildClass::QueueGameForRebuild($game['game_id']);
}

if (isset($_POST['charts'])) {
  $charts = array_map(
    fn($c) => filter_params([
      Params::number('chart_id'),
      Params::number('dlc'),
      Params::number('players'),
      Params::number('level_pos'),
      Params::str('name'),
    ], $c),
    $_POST['charts'],
  );

  $existing_charts = group_by(LevelsRepository::where(['game_id' => $game['game_id']]), fn($c) => $c['level_name']);

  usort($charts, fn($a, $b) => $a['level_pos'] <=> $b['level_pos']);

  foreach ($charts as $i => $chart) {
    database_update_by('levels', [
      'level_pos' => $i + 1,
      'players' => $chart['players'],
      'dlc' => $chart['dlc'],
    ], [
      'level_id' => $chart['chart_id'],
      'group_id' => $group['group_id'], # to ensure that we're not editing outside our scope
      'game_id' => $group['game_id'],   # TODO: separate charts hack
    ]);

    $original_name = database_find_by('levels', [
      'level_id' => $chart['chart_id'],
      'group_id' => $group['group_id'], # to ensure that we're not editing outside our scope
      'game_id' => $group['game_id'],   # TODO: separate charts hack
    ])['level_name'];

    if ($chart['name'] != $original_name) {
      $t->ChangeChartName($params['lang_id'], $chart['chart_id'], $chart['name'], $group['game_id']);

      if ($params['advanced'] == 'game') {
        foreach ($existing_charts[$original_name] ?? [] as $other_chart) {
          $t->ChangeChartName($params['lang_id'], $other_chart['level_id'], $chart['name'], $group['game_id']);
        }
      }
    }

    ChartRebuildClass::RebuildChart($chart['chart_id']);
  }

  GameRebuildClass::QueueGameForRebuild($group['game_id']);
  $cs->WriteNote(true, "Group changed");
}

$cs->RedirectToPreviousPage();
