<?php

Authorization::authorize('Designer');

$rankbutton = RankbuttonsRepository::get($_POST['rankbutton_id'] ?? NULL);

if (!$rankbutton) {
  $cs->PageNotFound();
}

database_update_by('rankbuttons', ['active' => false], ['game_id' => $rankbutton['game_id']]);
database_update_by('rankbuttons', ['active' => true], ['rankbutton_id' => $rankbutton['rankbutton_id']]);

$cs->WriteNote(true, "Rankbutton activated");
redirect_to('/rankbuttons');
