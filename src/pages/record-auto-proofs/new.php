<?php

Authorization::authorize('User');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

if (!$game['auto_proofer_module']) {
  $cs->LeavePage("/games/{$game['game_id']}", "This feature is only enabled for a few select games");
}

render_with('record-auto-proofs/new', [
  'page_title' => 'Detect record from proofs',
  'game' => $game,
]);
