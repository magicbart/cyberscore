<?php

Authorization::authorize('GlobalMod');

$suggestions = database_get_all(database_select("
  SELECT translation_db.*, rules.rule_text, languages.*
  FROM translation_db
  JOIN rules ON rules.rule_id = translation_db.table_id
  JOIN languages USING(language_id)
  WHERE table_field = 'rule_text' AND is_active = 'n'
", '', []));

render_with('translations/rule-suggestions/index', [
  'page_title' => 'Translation - pending rule suggestions',
  'suggestions' => $suggestions,
]);
