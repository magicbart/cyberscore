<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$all_news = database_get_all(database_select("
  SELECT
    news_id, headline,
    (SELECT COUNT(*) FROM translation_db WHERE table_field IN('headline', 'news_text') AND table_id = news.news_id AND is_active = 'y' AND language_id = ?) AS count_t,
    (SELECT COUNT(*) FROM translation_db WHERE table_field IN('headline', 'news_text') AND table_id = news.news_id AND is_active = 'n' AND language_id = ?) AS count_s
  FROM news
  ORDER BY news_id DESC
", 'ss', [$translate_lang_id, $translate_lang_id]));

$news_id = $_GET['news_id'] ?? NULL;

if (!empty($news_id)) {
  $news_article = database_find_by('news', ['news_id' => $news_id]);

  $news_article['translated_headline'] = $t->RetrieveNewsHeadline($translate_lang_id, $news_id);
  $news_article['translated_text'] = $t->RetrieveNewsText($translate_lang_id, $news_id);

  $news_article['headline_suggestions'] = $t->RetrieveDBSuggestions($translate_lang_id, 'headline', $news_id);
  $news_article['news_text_suggestions'] = $t->RetrieveDBSuggestions($translate_lang_id, 'news_text', $news_id);
} else {
  $news_article = NULL;
}

render_with('translations/news/index', [
  'page_title' => 'Translation',
  'language_name' => $t->GetLangName($translate_lang_id),
  'all_news' => $all_news,
  'news_article' => $news_article,
  'translate_lang_id' => $translate_lang_id,
]);
