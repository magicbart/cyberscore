<?php

$params = filter_post_params([
  Params::number('language_id'),
  Params::number('news_id'),
  Params::str('text'),
  Params::str('headline'),
]);

$news = database_find_by('news', ['news_id' => $params['news_id']]);

if ($news == NULL) {
  $cs->LeavePage("/translations/news", 'News item not found');
}

$t->AddTranslationDB(
  $params['language_id'],
  'headline', $params['news_id'], $params['headline'],
  $current_user['user_id']
);

$t->AddTranslationDB(
  $params['language_id'],
  'news_text', $params['news_id'], $params['text'],
  $current_user['user_id']
);

$cs->RedirectToPreviousPage();
