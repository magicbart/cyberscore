<?php

Authorization::authorize('GlobalMod');

$suggestions = database_get_all(database_select("
  SELECT translation_db.*, proof_rules.rule_text, languages.*
  FROM translation_db
  JOIN proof_rules ON proof_rules.rule_id = translation_db.table_id
  JOIN languages USING(language_id)
  WHERE table_field = 'proof_rule_text' AND is_active = 'n'
", '', []));

render_with('translations/proof-rule-suggestions/index', [
  'page_title' => 'Translation - pending proof guideline suggestions',
  'suggestions' => $suggestions,
]);
