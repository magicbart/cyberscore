<?php

Authorization::authorize('Translator');

$language_id = intval($_POST['language_id']);
foreach ($_POST['text'] as $country_id => $translation) {
  $t->AddTranslationDB($language_id, 'country_name', $country_id, $translation, $current_user['user_id']);
}

$cs->RedirectToPreviousPage();
