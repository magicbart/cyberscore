<?php

Authorization::authorize('Translator');

render_with('translations/faq', [
  'page_title' => 'Translation FAQ',
]);
