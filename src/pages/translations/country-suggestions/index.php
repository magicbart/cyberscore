<?php

Authorization::authorize('GlobalMod');

$countries = database_get_all(database_select("
  SELECT
    translation_db.id AS suggestion_id,
    languages.language_name,
    translation_db.translation,
    countries.country_name AS english_name
  FROM translation_db
  JOIN languages USING(language_id)
  JOIN countries ON translation_db.table_id = countries.country_id
  WHERE translation_db.table_field = 'country_name' AND translation_db.is_active = 'n'
  ORDER BY translation_db.language_id ASC, translation_db.translation
", '', []));

render_with('translations/country-suggestions/index', [
  'page_title' => 'Translation',
  'countries' => $countries,
]);
