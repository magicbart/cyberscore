<?php

Authorization::authorize('GlobalMod');

$key = $_POST['key'];

database_select("
  DELETE FROM translation_pages_votes
  WHERE translation_pages_id IN (SELECT id FROM translation_pages WHERE `key` = ?)
", 's', [$key]);

database_delete_by('translation_pages', ['key' => $key]);

$cs->WriteNote(true, 'Page string deleted');
$cs->LeavePage('/translations/page-strings');
