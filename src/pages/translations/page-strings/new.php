<?php

Authorization::authorize('GlobalMod');

$key = $_GET['key'] ?? "";
$group = explode("_", $key)[0] ?? "";

render_with('translations/page-strings/new', [
  'page_title' => 'Add page string',
  'key' => $key,
  'group' => $group,
]);
