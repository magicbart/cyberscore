<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$group = $_GET['group'] ?? 'general';
$search = $_GET['search'] ?? '';
$filter_t = $_GET['filter_t'] ?? 'all';
$filter_s = $_GET['filter_s'] ?? 'all';

$wheres = [
  "english.language_id" => 1,
  "english.is_active" => 'y',
];

if ($group != 'all') {
  $wheres["english.`group`"] = $group;
}

[$where_sql, $where_binds] = database_where_sql_bind($wheres);

$results = database_fetch_all("
  SELECT
    english.`key`,
    MIN(english.translation) AS english_text,
    MIN(translated.translation) AS translated_text,

    COUNT(suggestion.id) AS num_suggestions,
    SUM(IF(users.user_id = ?, 0, 1)) AS num_suggestions_from_other_users,
    GROUP_CONCAT(users.username ORDER BY users.username ASC SEPARATOR ', ') AS suggester
  FROM translation_pages english
  LEFT JOIN translation_pages translated ON (english.`key` = translated.`key` AND translated.language_id = ? AND translated.is_active = 'y')
  LEFT JOIN translation_pages suggestion ON (english.`key` = suggestion.`key` AND suggestion.language_id = ? AND suggestion.is_active = 'n')
  LEFT JOIN users ON users.user_id = suggestion.suggester_id
  WHERE $where_sql
  GROUP BY english.`key`
", array_merge([$current_user['user_id'], $translate_lang_id, $translate_lang_id], $where_binds));

$entries = [];
foreach ($results as $entry) {
  $english_text = $entry['english_text'];
  $translation = $entry['translated_text'];
  $num_suggestions = $entry['num_suggestions'];
  $num_suggestions_f = $entry['num_suggestions_from_other_users'];

  if ($filter_t == 'no' && $translation != null) {
    continue;
  } else if ($filter_t == 'yes' && $translation == null) {
    continue;
  }

  if ($filter_s == 'yes' && $num_suggestions == 0) {
    continue;
  } else if ($filter_s == 'yes_f' && $num_suggestions_f == 0) {
    continue;
  } else if ($filter_s == 'no' && $num_suggestions > 0) {
    continue;
  }

  if ($search != '' && substr_count(strtoupper($english_text), strtoupper($search)) == 0 && substr_count(strtoupper($translation), strtoupper($search)) == 0) {
    continue;
  }

  $entries []= $entry;
}

$page_groups = $t->RetrievePageGroups();

render_with('translations/page-strings/index', [
  'page_title' => 'Translation info',
  'page_groups' => $page_groups,
  'selected_group' => $group,
  'filter_t' => $filter_t,
  'filter_s' => $filter_s,
  'search' => $search,
  'entries' => $entries,
]);
