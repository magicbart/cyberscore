<?php

Authorization::authorize('Translator');

$params = filter_post_params([
  Params::number('suggestion_id'),
  Params::enum('vote', ['1', '-1']),
]);

$suggestion = database_find_by('translation_pages', ['id' => $params['suggestion_id']]);

if (!$suggestion || $params['vote'] == NULL) {
  $cs->WriteNote(false, "Invalid vote!");
  $cs->RedirectToPreviousPage();
}

// delete old vote
database_delete_by('translation_pages_votes', [
  'translation_pages_id' => $params['suggestion_id'],
  'user_id' => $current_user['user_id'],
]);

database_insert('translation_pages_votes', [
  'translation_pages_id' => $params['suggestion_id'],
  'user_id' => $current_user['user_id'],
  'vote' => $params['vote'],
]);

$cs->RedirectToPreviousPage();
