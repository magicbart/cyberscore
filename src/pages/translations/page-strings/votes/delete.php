<?php

Authorization::authorize('Translator');

$suggestion = database_find_by('translation_pages', ['id' => $_GET['id']]);

if (!$suggestion) {
  $cs->WriteNote(false, "Invalid vote!");
  $cs->RedirectToPreviousPage();
}

database_delete_by('translation_pages_votes', [
  'translation_pages_id' => $suggestion['id'],
  'user_id' => $current_user['user_id'],
]);

$cs->RedirectToPreviousPage();
