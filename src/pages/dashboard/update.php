<?php

Authorization::authorize('User');

$dashboard_config = ['widgets' => []];

foreach ($_POST['widgets'] as $index => $widget) {
  if (!in_array($widget['widget'], Dashboard::$available_widgets)) {
    $cs->FlashError("Unknown widget: " . h($widget['widget']));
    redirect_to_back();
  }

  // TODO: Validate widget params properly
  if ($widget['widget'] == 'favourite-games') {
    $widget['params'] = ['game_ids' => array_filter($widget['params']['game_ids'] ?? [], fn($x) => is_int(intval($x)))];
  } else {
    $widget['params'] = [];
  }

  $dashboard_config['widgets'] []= $widget;
}

$dashboard = database_find_by('user_dashboards', ['user_id' => $current_user['user_id']]);
if ($dashboard) {
  database_update_by('user_dashboards', ['config' => json_encode($dashboard_config)], ['user_id' => $current_user['user_id']]);
} else {
  database_insert('user_dashboards', ['config' => json_encode($dashboard_config), 'user_id' => $current_user['user_id']]);
}

$cs->FlashSuccess("Dashboard saved!");
redirect_to('/dashboard');
