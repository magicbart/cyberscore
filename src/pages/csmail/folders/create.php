<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::str("name"),
]);

$folder = database_find_by('csmail_folders', [
  'folder_name' => $params['name'],
  'user_id' => $current_user['user_id']
]);

if ($folder != NULL) {
  $cs->LeavePage('/csmails', h($t['csmail_error_duplicate_folder']));
}

$custom_folder_count = database_single_value("
  SELECT COUNT(1) FROM csmail_folders
  WHERE user_id = ?
", 's', [$current_user['user_id']]);

if ($custom_folder_count > 8) {
  $cs->LeavePage('/csmails', h($t['csmail_error_max_folders']));
}

database_insert('csmail_folders', [
  'folder_name' => $params['name'],
  'user_id' => $current_user['user_id'],
]);

$cs->RedirectToPreviousPage();
