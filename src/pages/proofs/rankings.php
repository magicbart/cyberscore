<?php

Authorization::authorize('ProofMod');

$sort = $_GET['sort'] ?? '';

if ($sort == 'day') {
  $mods = database_get_all(database_select("
    SELECT DISTINCT
      users.user_id, users.username, users.user_groups,
      CONCAT(YEAR(approval_date), '-', MONTH(approval_date), '-', DAY(approval_date)) AS date,
      COUNT(*) AS num_approved_recently
    FROM record_approvals
    JOIN users USING(user_id)
    GROUP BY users.user_id, CONCAT(YEAR(approval_date), '-', MONTH(approval_date), '-', DAY(approval_date))
    ORDER BY COUNT(*) DESC
  ", '', []));
} else if ($sort == 'month') {
  $mods = database_get_all(database_select("
    SELECT
      users.user_id, users.username, users.user_groups,
      COUNT(*) AS num_approved_recently
    FROM record_approvals
    JOIN users USING(user_id)
    WHERE record_approvals.approval_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()
    GROUP BY users.user_id
    ORDER BY num_approved_recently DESC
  ", '', []));
} else if($sort == 'vids') {
  $mods = database_get_all(database_select("
    SELECT
      users.user_id, users.username, users.user_groups,
      COUNT(*) AS vids_approved
    FROM record_approvals
    JOIN users USING(user_id)
    JOIN records ON record_approvals.record_id = records.record_id
    WHERE records.proof_type IN('v', 's', 'r')
    GROUP BY users.user_id
    ORDER BY vids_approved DESC
  ", '', []));
} else if ($sort == 'refused') {
  $mods = database_get_all(database_select("
    SELECT
      users.user_id, users.username, users.user_groups,
      staff_tasks.tasks_type,
      COUNT(*) AS proofs_refused
    FROM staff_tasks
    JOIN users USING(user_id)
    WHERE staff_tasks.tasks_type = 'proof_refused'
    GROUP BY users.user_id
    ORDER BY proofs_refused DESC
  ", '', []));
} else {
  $mods = database_get_all(database_select("
    SELECT
      users.user_id, users.username, users.user_groups,
      COUNT(*) AS num_approved
    FROM record_approvals
    JOIN users USING(user_id)
    GROUP BY users.user_id
    ORDER BY num_approved DESC
  ", '', []));
}

$mod_total = index_by(database_get_all(database_select('
  SELECT record_approvals.user_id, count(1) AS count
  FROM record_approvals
  JOIN records USING(record_id)
  GROUP BY record_approvals.user_id
', '', [])), 'user_id', 'count');

$mod_recent = index_by(database_get_all(database_select('
  SELECT record_approvals.user_id, count(1) AS count
  FROM record_approvals
  JOIN records USING(record_id)
  WHERE approval_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()
  GROUP BY record_approvals.user_id
', '', [])), 'user_id', 'count');

$mod_video = index_by(database_get_all(database_select("
  SELECT record_approvals.user_id, count(1) AS count
  FROM record_approvals
  JOIN records USING(record_id)
  WHERE records.proof_type IN ('v', 's', 'r') GROUP BY record_approvals.user_id
", '', [])), 'user_id', 'count');

$results = database_get_all(database_select("
  SELECT COUNT(*) AS refused_proofs, user_id
  FROM staff_tasks
  WHERE tasks_type = 'proof_refused'
  GROUP BY user_id
", '', []));
$refused_proofs = index_by($results, 'user_id', 'refused_proofs');

$results = database_get_all(database_select("
  SELECT
    user_id,
    DATE(approval_date) AS date,
    COUNT(*) AS num_approved
  FROM record_approvals
  GROUP BY user_id, date
  ORDER BY COUNT(*) ASC
", '', []));

foreach ($results as $row) {
  $best_day[$row['user_id']] = $row;
}

render_with('proofs/rankings', [
  'page_title' => 'Pending proofs',
  'mods' => $mods,
  'mod_total' => $mod_total,
  'mod_recent' => $mod_recent,
  'mod_video' => $mod_video,
  'best_day' => $best_day,
  'refused_proofs' => $refused_proofs,
]);
