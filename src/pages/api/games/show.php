<?php

Authorization::authorize('Guest');

$game_id = intval($_GET['id']);

$game = database_get(database_select("
    SELECT
      games.*,
      GROUP_CONCAT(game_platforms.platform_id, '_', platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ',') AS platforms
    FROM games
    LEFT JOIN game_platforms USING(game_id)
    LEFT JOIN platforms ON (game_platforms.platform_id = platforms.platform_id AND original = 1)
    WHERE games.game_id = ?
    GROUP BY games.game_id
", 'i', [$game_id]));

if (!$game) {
  http_response_code(404);
  render_json(['error' => 'Game not found']);
  exit;
}

$t->CacheGame($game_id);

if ($game['platforms'] != "") {
  $game['platforms'] = array_map(function($e) { return explode('_', $e); }, explode(',', $game['platforms']));
} else {
  $game['platforms'] = [];
}

$game_groups = database_get_all(database_select("
  SELECT
    levels.game_id,
    levels.group_id,
    SUM(case levels.dlc when 0 then 1 else 0 end) as num_non_dlc_levels,
    COUNT(levels.level_id) AS num_levels,
    SUM(levels.num_subs) AS total_subs,
    level_groups.ranked,
    level_groups.group_name
  FROM levels
  LEFT JOIN level_groups USING(group_id)
  WHERE levels.game_id = ?
  GROUP BY levels.game_id, levels.group_id
  ORDER BY levels.group_id != 0 DESC, level_groups.group_pos ASC
", 's', [$game_id]));

$group_levels = database_get_all(database_select("
  SELECT
    levels.*,
    MIN(records.record_id) AS record_id,
    MIN(records.chart_pos) AS chart_pos,
    MIN(records.platinum) AS platinum,
    MIN(records.rec_status) AS rec_status,
    MIN(records.submission) AS rec_submission,
    MIN(records.submission2) AS rec_submission2,
    MIN(best.submission) AS best_submission,
    MIN(best.submission2) AS best_submission2,
    chart_modifiers2.*
  FROM levels
  LEFT JOIN records ON records.level_id = levels.level_id AND records.user_id = ?
  LEFT JOIN records best ON best.level_id = levels.level_id AND best.chart_pos = 1
  LEFT JOIN chart_modifiers2 ON chart_modifiers2.level_id = levels.level_id
  WHERE levels.game_id = ?
  GROUP BY levels.level_id
  ORDER BY levels.level_pos ASC, levels.level_name ASC
", 'ii', [$current_user['user_id'] ?? 0, $game_id]));

$group_levels = group_by($group_levels, fn ($e) => $e['group_id']);

$game_name = $t->GetGameName($game_id);

$groups = [];
foreach ($game_groups as $group) {
  $charts = [];
  foreach ($group_levels[$group['group_id']] as $level) {
    $level['chart_id'] = $level['level_id'];
    $chart = [
      'chart_id' => $level['level_id'],
      'chart_name' => $t->GetChartName($level['level_id']),
      'english_chart_name' => $level['level_name'],
      'chart_type' => $level['chart_type'],
      'chart_type2' => $level['chart_type2'],
      'prefix' => $level['score_prefix'],
      'suffix' => $level['score_suffix'],
      'conversion_factor' => $level['conversion_factor'],
      'num_decimals' => $level['num_decimals'],
      'num_decimals2' => $level['num_decimals2'],
      'chart_url' => [
        'html' => url_for(['chart' => $level]),
        'json' => url_for(['chart' => $level]) . ".json",
      ],
    ];

    if (Authorization::has_all_permissions('records:read')) {
      $chart['record'] = [
        'record_id' => $level['record_id'],
        'submission' => $level['rec_submission'],
        'submission2' => $level['rec_submission2'],
        'rec_status' => $level['rec_status'],
      ];
    }

    $charts []= $chart;
  }

  $groups []= [
    'group_id' => $group['group_id'],
    'group_name' => $t->GetGroupName($group['group_id']),
    'english_group_name' => $group['group_name'],
    'group_pos' => $group['group_id'],
    'charts' => $charts,
    'group_url' => [
      'html' => url_for_group($group),
    ]
  ];
}

$boxart = $cs->GetBoxArts($game_id)[0] ?? null;

render_json([
  'game_id' => $game_id,
  'game_name' => $game_name,
  'auto_proofer_module' => $game['auto_proofer_module'],
  'game_boxart' => $boxart['url'] ?? null,
  'chart_groups' => $groups,
  'platforms' => array_map(fn ($p) => ['platform_id' => $p[0], 'platform_name' => $p[1]], $game['platforms']),
]);
