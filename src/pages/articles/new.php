<?php

Authorization::authorize('User');

render_with("articles/new", [
  'page_title' => "New article",
]);
