<?php

Authorization::authorize('Guest');

require_once("src/scoreboards/first.php");

[$year, $month] = explode('-', $_GET['date'] ?? date("Y-n"));

$dates = database_get_all(database_select('SELECT DISTINCT year, month FROM sb_cache_first ORDER BY 1, 2', '', []));

foreach ($dates as &$date) {
  $date['label'] = date("F Y",mktime(0,0,0,$date['month'],1,$date['year']));
  $date['id'] = $date['year'] . "-" . $date['month'];
}
unset($date);


$game_ids = pluck(GamesRepository::all(), 'game_id');
$country_ids = pluck(database_get_all(database_select('select country_id from countries', '', [])), 'country_id');

$sb = new FirstScoreboard($game_ids, $country_ids, [$year, $month], "");
$scoreboard = $sb->fetch(100, 0);
$scoreboard = $cs->FormatRanking($scoreboard);

render_with('scoreboards/first', [
  'page_title' => 'Monthly Chart Challenge',
  'year' => $year,
  'month' => $month,
  'dates' => $dates,
  'scoreboard' => $scoreboard,
]);
