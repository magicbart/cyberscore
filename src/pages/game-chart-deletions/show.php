<?php

Authorization::authorize('GameMod');

// get the game info from the chart number that was clicked.
$game = GamesRepository::get($_GET['id']);

$game_charts = database_get_all(database_select("
  SELECT
    levels.level_id,
    levels.group_id,
    level_name,
    num_subs,
    group_name,
    CASE level_groups.ranked WHEN 1 THEN 'standard' ELSE 'standard' END as group_ranked
  FROM levels
  LEFT JOIN level_groups USING (group_id)
  WHERE levels.game_id = ?
  ORDER BY level_groups.group_pos, levels.level_pos
", 'i', [$game['game_id']]));

$game_charts = group_by($game_charts, function($e) { return $e['group_id']; });

render_with('game-chart-deletions/show', [
  'page_title' => 'Delete Charts',
  'game_charts' => $game_charts,
  'game' => $game,
]);
