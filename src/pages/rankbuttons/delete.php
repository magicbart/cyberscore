<?php

Authorization::authorize('Admin');

$rankbutton = RankbuttonsRepository::get($_GET['id'] ?? NULL);

if (!$rankbutton) {
  $cs->PageNotFound();
}

if ($rankbutton['active']) {
  $cs->LeavePage('/rankbuttons', "Can't delete the active rankbutton");
}

database_delete_by('rankbuttons', ['rankbutton_id' => $rankbutton['rankbutton_id']]);

$cs->WriteNote(true, "Rankbutton deleted");
redirect_to('/rankbuttons');
