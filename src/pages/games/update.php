<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

// Support for the date of game publication implemented on October 15 2018

// hardcoded game id check is to deny all games published before this support was added from having their publication date updated
// Hardcoded game ID checks in the OR clauses are used to exclude older, legitimate games in the workshop that may be published in the future (and thus receive a date)
$older_games_with_date_published = [
  1735, 1784, 1785, 1814, 1822, 1923,
  1924, 2052, 2102, 2171, 2346
];

if ($_POST['site_id'] == 1 && ($game['date_published'] == "0000-00-00 00:00:00" || $game['date_published'] == NULL) && ($game['game_id'] > 2407 || in_array($game['game_id'], $older_games_with_date_published))) {
  database_update_by('games', ['date_published' => database_now()], ['game_id' => $game['game_id']]);
}

database_update_by('games', [
  'notes' => $_POST['notes'],
  'mod_notes' => $_POST['mod_notes'],
  'game_patches' => $_POST['patches'],
  'site_id' => $_POST['site_id'],
], [
  'game_id' => $game['game_id']
]);

$game_patches_before = $t->GetGamePatches($game['game_patches']);
$game_patches_after  = $t->GetGamePatches($_POST['patches']);

// Update patch data for each record that has one set, if a specific patch name is changed or removed
foreach ($game_patches_before as $index => $game_patch_before) {
  if ($game_patch_before != $game_patches_after[$index] && $game_patch_before != '') {
    database_update_by('records', ['game_patch' => $game_patches_after[$index]], ['game_id' => $game['game_id'], 'game_patch' => $game_patch_before]);
  }
}

// Platforms
//
// Secondary platforms implied by primary platforms due to backwards compatibility
$secondary_platforms = [
  18 => [19, 5, 33],  // Game Boy         -> Game boy Color, Game Boy Advance, Game Boy Player
  19 => [5, 33],      // Game Boy Color   -> Game Boy Advance, Game Boy Player
   5 => [1, 33],      // Game Boy Advance -> DS, Game Boy Player
   1 => [31, 28, 44], // NDS              -> DSi, 3DS, New 3DS
  31 => [28, 44],     // DSi              -> 3DS, New 3DS
  28 => [44],         // 3DS              -> New 3DS
   4 => [16],         // Gamecube         -> Wii
  16 => [36],         // Wii              -> Wii U
  10 => [2],          // PS1              -> PS2
   2 => [17],         // PS2              -> PS3
  40 => [71],         // PS4              -> PS5
  39 => [72],         // Xbox One         -> Xbox Series X|S
];

$platform_ids = $_POST['platform_id'] ?? [];
$platform_ids2 = $_POST['platform_id2'] ?? [];

foreach ($platform_ids as $platform_id) {
  $platform_ids2 = array_merge($platform_ids2, $secondary_platforms[$platform_id] ?? []);
}
$platform_ids2 = array_diff($platform_ids2, $platform_ids);
$platform_ids2 = array_unique($platform_ids2);

database_delete_by('game_platforms', ['game_id' => $game['game_id']]);

foreach ($platform_ids as $platform_id) {
  database_insert('game_platforms', ['game_id' => $game['game_id'], 'platform_id' => $platform_id, 'original' => true]);
}

foreach ($platform_ids2 as $platform_id) {
  database_insert('game_platforms', ['game_id' => $game['game_id'], 'platform_id' => $platform_id, 'original' => false]);
}

// Related games
database_delete_by('games_related', ['game_id' => $game['game_id']]);
database_delete_by('games_related', ['related_game_id' => $game['game_id']]);

$related_games = $_POST['related_games'] ?? [];
$super_related_games = $_POST['super_related_games'] ?? [];

foreach ($related_games as $related_game) {
  if (!isset($super_related_games) || !(in_array($related_game, $super_related_games))) {
    database_insert('games_related', ['game_id' => $game['game_id'], 'related_game_id' => $related_game, 'super_related' => false]);
  }
}

foreach ($super_related_games as $related_game) {
  database_insert('games_related', ['game_id' => $game['game_id'], 'related_game_id' => $related_game, 'super_related' => true]);
}

redirect_to("/game/{$game['game_id']}");
