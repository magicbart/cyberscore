<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);
$game = GamesRepository::get($game_id);

$level_count = database_single_value("
  SELECT COUNT(1) FROM levels WHERE game_id = ?
", 's', [$game_id]);

if ($level_count > 0) {
  $cs->WriteNote(
    false,
    "{$game_name['game_name']} can't be deleted because it has charts. " .
    "Delete all charts first, then come back and try again."
  );
  $cs->LeavePage("/game/$game_id");
}

database_delete_by('boxarts',            ['game_id' => $game_id]);
database_delete_by('rankbuttons',        ['game_id' => $game_id]);
database_delete_by('game_platforms',     ['game_id' => $game_id]);
database_delete_by('game_entities',      ['game_id' => $game_id]);
database_delete_by('translation_games',  ['game_id' => $game_id]);
database_delete_by('gsb_cache_csp',      ['game_id' => $game_id]);
database_delete_by('gsb_cache_medals',   ['game_id' => $game_id]);
database_delete_by('gsb_cache_arcade',   ['game_id' => $game_id]);
database_delete_by('gsb_cache_speedrun', ['game_id' => $game_id]);
database_delete_by('gsb_cache_solution', ['game_id' => $game_id]);
database_delete_by('level_groups',       ['game_id' => $game_id]);
database_delete_by('games_related',      ['game_id' => $game_id]);
database_delete_by('games_related',      ['related_game_id' => $game_id]);
database_delete_by('games',              ['game_id' => $game_id]);
database_update_by('staff_tasks', ['result' => 'Game deleted'], ['tasks_type' => 'game_added', 'task_id' => $game_id]);

$cs->WriteNote(true, h($game['game_name']) . " has been deleted.");
$cs->LeavePage("/games.php");
