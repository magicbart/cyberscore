<?php

$game = GamesRepository::get($_GET['game_id'] ?? $_GET['id']);

if (!$game) {
  $cs->PageNotFound();
}

switch ($_GET['type'] ?? 'p') {
case 'v':		$type_name = 'Video'; $proof_type = 'v'; break;
case 'p':		$type_name = 'Image'; $proof_type = 'p'; break;
default:		$type_name = 'Image'; $proof_type = 'p'; break;
}

$records_by_user_id = group_by(database_get_all(database_select("
  SELECT *
  FROM records
  JOIN levels USING (level_id)
  JOIN users USING (user_id)
  LEFT JOIN level_groups USING (group_id)
  WHERE records.game_id = ? AND records.rec_status = 3 AND records.proof_type = ? AND records.linked_proof != ''
  ORDER BY level_groups.group_pos ASC, levels.level_pos ASC
", 'ss', [$game['game_id'], $proof_type])), fn($r) => $r['user_id']);

$t->CacheGame($game['game_id']);
$game_name = $t->GetGameName($game['game_id']);

render_with('game-proofs/show', [
  'page_title' => 'Proofs for ' . $game_name,
  'records_by_user_id' => $records_by_user_id,
  'type_name' => $type_name,
  'proof_type' => $proof_type,
  'game' => $game,
]);
