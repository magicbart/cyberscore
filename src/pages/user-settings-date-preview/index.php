<?php

Authorization::authorize('User');

$format = $_GET['format'] ?? '';

$formatter = new DateFormatter($format, $current_user['timezone']);

render_json(['preview' => $formatter->Format(time(), 'date', false)]);
