<?php

if (($_GET['key'] ?? NULL) !== $config['email']['notification_key']) {
  http_response_code(403);
  render_json(['error' => 'invalid key']);
  $cs->Exit();
}

log_event('email/notification', json_decode(file_get_contents('php://input'), true));
render_json(['ok' => 'yes']);
