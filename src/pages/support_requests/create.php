<?php

if (recaptcha_verify()) {
  $message = $_POST['message'];

  if (!$current_user) {
    $message .= "\n" . $current_user['email'];
  }

  $support_id = database_insert('support', [
    'user_id' => $user_id,
    'message' => $message,
    'message_date' => database_now(),
    'ip' => $_SERVER['REMOTE_ADDR'],
  ]);

  Notification::SupportRequest($support_id)->DeliverToGroups(['GlobalMod', 'Admin']);

  $cs->WriteNote(true, 'Support request sent');
} else {
  $cs->WriteNote(false, "reCAPTCHA verification failed. Support Request was not sent");
}

$cs->LeavePage("/support.php");
