<?php

render_with('support_requests/new', [
  'page_title' => $t['support_title'],
  'text' => str_replace(['[link]', '[/link]'], ['<a href="https://discord.gg/0ltq5mHzEqHnRc6N">', '</a>'], t('support_intro')),
]);
