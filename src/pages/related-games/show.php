<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

if (!$cs->GameExists($game_id)) {
  $cs->LeavePage('/games', 'The requested game does not exist.');
}

$related_games = database_get_all(database_select("
  SELECT
    games.game_id,
    games.game_name,
    COALESCE(r1.super_related, r2.super_related) AS check_relation
  FROM games
  LEFT JOIN games_related r1 ON games.game_id = r1.game_id AND r1.related_game_id = ?
  LEFT JOIN games_related r2 ON games.game_id = r2.related_game_id AND r2.game_id = ?
  WHERE games.game_id != ?
  ORDER BY check_relation DESC, games.game_name ASC
", 'sss', [$game_id, $game_id, $game_id]));

render_with('related-games/show', [
  'page_title' => 'Edit related games',
  'game_id' => $game_id,
  'related_games' => $related_games,
]);
