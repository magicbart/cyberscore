<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

// Build changes from POST

$related_games = [];
$super_related_games = [];
if (isset($_POST['related'])) {
  foreach ($_POST['related'] as $related_game_id) {
    $related_games []= [
      'game_id'         => $game_id,
      'related_game_id' => intval($related_game_id),
      'super_related'   => in_array($related_game_id, $_POST['superrelated'] ?? []) ? 1 : 0,
    ];
  }
}

// Validate changes

if (!GamesRepository::exists($game_id)) {
  $cs->WriteNote(false, "Unknown game");
  $cs->RedirectToPreviousPage();
  exit;
}

// Apply changes
database_delete_by('games_related', ['game_id' => $game_id]);
database_insert_all('games_related', $related_games);
$cs->WriteNote(true, "Related games updated");

$cs->RedirectToPreviousPage();
