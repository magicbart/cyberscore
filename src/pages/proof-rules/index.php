<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$rule_filter = $_GET['rule_filter'] ?? '';

//Put guideline IDs into the WHERE clause here to make them show up at the top of the list of guidelines
//(Guideline 0 doesn't exist, so this doesn't give anything right now)
$sample_rules = database_get_all(database_select("
  SELECT rule_id, rule_text
  FROM proof_rules
  WHERE rule_id IN (1, 2, 3, 4, 6, 7)
  ORDER BY rule_id
", '', []));

$rules = database_get_all(database_select("
  SELECT *
  FROM proof_rules
  WHERE rule_text LIKE ?
  ORDER by rule_id
", 's', ["%$rule_filter%"]));

render_with("proof-rules/index", [
  'page_title' => 'Proof Guidelines editor',
  'sample_rules' => $sample_rules,
  'rules' => $rules,
  'rule_filter' => $rule_filter,
]);
