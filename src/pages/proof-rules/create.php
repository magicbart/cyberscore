<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$params = filter_post_params([
  Params::str('rule_text'),
]);

if (ProofRulesRepository::create($params)) {
  $new_id = database_single_value("SELECT rule_id FROM proof_rules ORDER BY rule_id DESC LIMIT 1", '', []);
  $cs->WriteNote(true, "Proof guideline ID #".$new_id." has been added.");
} else {
  $cs->WriteNote(false, "The guideline's text matches an existing guideline's text.");
}

header("Location: /proof-rules/new");
