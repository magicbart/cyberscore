<?php

Authorization::authorize('Guest');

render_with('home/faq', [
  'page_title' => t('general_faq'),
]);
