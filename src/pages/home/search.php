<?php

Authorization::authorize('Guest');

$query = $_GET['q'] ?? "";

function IsValidSearch($query) {
  $query = strtolower($query);
  return $query == 'q' || $query == 'io' || strlen($query) >= 3;
}

$query_valid = IsValidSearch($query);

if ($query_valid) {
  [$users, $games] = search($query);
} else {
  $users = [];
  $games = [];
}

if (($_GET['format'] ?? NULL) == 'json') {
  if (isset($_GET['live'])) {
    render_json([
      'users' => $users,
      'games' => $games,
      'rendered' => render_partial('components/home/live-search', [
        'games' => $games,
        'users' => $users,
        'term' => $query,
      ]),
    ]);
  } else {
    render_json([
      'users' => $users,
      'games' => $games,
    ]);
  }
} else {
  // Found only 1 result? redirect there!
  if (count($users) == 0 && count($games) == 1) {
    redirect_to("/game/" . $games[0]['game_id']);
  }

  if (count($users) == 1 && count($games) == 0) {
    redirect_to("/user/" . $users[0]['user_id']);
  }

  render_with('home/search', [
    'page_title' => t('search_title'),
    'query_valid' => $query_valid,
    'query' => $query,
    'users' => $users,
    'games' => $games,
  ]);
}
