<?php

Authorization::authorize('Guest');

render_with('home/site_rules', [
  'page_title' => t('general_site_rules'),
]);
