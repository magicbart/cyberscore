<?php

Authorization::authorize('Guest');

global $t;
$t->CacheGameNames();

render_with('home/index', [
  'page_title' => t('general_home'),
]);
