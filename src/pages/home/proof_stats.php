<?php

Authorization::authorize('Guest');

// How to add new modules:
// - Create a new module in src/components/proofs/module_name.php
// - Create the twig templaate in src/templates/components/proofs/module_name.php
// - Add an entry to the $sidebar variable below

$sidebar = [
  "Proof Statistics" => [
    'last_30_days' => "Recent stats (30 days)",
    'month_history' => "Monthly history",
    'best_days' => "Best days (site)",
    'best_days_user' => "Best days (mods)",
  ],
  "Individual user stats" => [
    'your_users' => "Most approved users",
    'your_games' => "Most approved games",
  ],
];

$src = validate_module($sidebar, "proof_stats", $_GET['src']);

// TODO: select proof mod

render_with("home/proof_stats", [
  'page_title' => "Detailed proof stats",
  'sidebar' => $sidebar,
  'src' => $src,
]);
