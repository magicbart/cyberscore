<?php

Authorization::authorize('Guest');

render_with('home/history', [
  'page_title' => t('general_history'),
]);
