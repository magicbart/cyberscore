<?php

require_once("src/lib/rankbutton_generator.php");

$im = RankbuttonGenerator::global_rankbutton($_GET['type'] ?? null, $_GET['user_id'] ?? null);

if ($im) {
  header("Content-type: image/gif");
  imagegif($im);
  imagedestroy($im);
}

