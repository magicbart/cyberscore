<?php

Authorization::authorize('Guest');

$news = database_fetch_all("
  SELECT news.*, COUNT(news_comments.news_id) AS num_comments
  FROM news
  LEFT JOIN news_comments USING(news_id)
  GROUP BY news.news_id
  ORDER BY news.news_id DESC
", []);

$display = Article::ArticlePermissions($current_user);

$news = array_filter($news, fn($a) => in_array($a['article_type'], $display));

foreach ($news as &$article) {
  $article['stars'] = Article::TargettedArticleStars($article['article_type']);
  $article['headline'] = $t->GetNewsHeadline($article['news_id']);
}
unset($article);

render_with('news/index', [
  'page_title' => t('news_archive'),
  'news' => $news,
  'display' => $display,
]);
