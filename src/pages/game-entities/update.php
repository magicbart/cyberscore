<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

foreach ($_POST['entity_names'] as $entity_id => $entity_name) {
  $t->ChangeEntityName($_POST['lang_id'], $entity_id, $entity_name, $game['game_id']);
}

foreach ($_POST['entity_groups'] as $entity_id => $entity_group) {
  database_update_by('game_entities', ['entity_group' => $entity_group], ['game_entity_id' => $entity_id]);
}

foreach ($_POST['delete'] as $entity_id => $entity_delete) {
  database_delete_by('game_entities', ['game_entity_id' => $entity_id]);
  $cs->WriteNote(false, "Deleted Entity ID: " . h($entity_id));
}

$cs->WriteNote(true, "Translations: " . $t->GetChanges());
redirect_to_back();
