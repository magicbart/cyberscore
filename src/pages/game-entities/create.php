<?php

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::number('game_id'),
  Params::str('entity_names'),
  Params::str('entity_ids'),
  Params::str('entity_group'),
]);

$game = GamesRepository::get($params['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

if (trim($params['entity_names']) != "") {
  $num_entities = 0;

  $entity_names = array_filter(
    array_map(fn($e) => trim($e), explode("\n", $params['entity_names'])),
    fn($e) => $e != '' && $e != 'N/A'
  );

  foreach ($entity_names as $entity_name) {
    $existing = database_single_value("
      SELECT COUNT(*) FROM game_entities
      WHERE game_id = ? AND entity_name = ?
    ", 'ss', [$game['game_id'], $entity_name]);

    if ($existing == 0) {
      database_insert('game_entities', [
        'game_id' => $game['game_id'],
        'entity_name' => $entity_name,
        'entity_group' => $params['entity_group'],
      ]);
      $num_entities++;
    }
  }

  if ($num_entities > 0) {
    $cs->WriteNote(true, "$num_entities entities added");
  }
}

if (trim($params['entity_ids']) != "") {
  $num_entities = 0;

  $entity_ids = array_filter(
    array_map(fn($e) => intval($e), explode("\n", $params['entity_ids'])),
    fn($e) => $e != 0
  );

  foreach ($entity_ids as $entity_id) {
    $existing = database_value(
      "SELECT COUNT(*) FROM game_entities WHERE game_entity_id = ?",
      [$entity_id]);

    if ($existing != 0) {
      database_insert('game_entities', [
        'game_id' => $game['game_id'],
        'entity_group' => $params['entity_group'],
        'entity_name' => '',
        'inherited_entity_id' => $entity_id,
      ]);
      $num_entities++;
    }
  }

  if ($num_entities > 0) {
    $cs->WriteNote(true, "$num_entities entities added");
  }
}

redirect_to_back();
