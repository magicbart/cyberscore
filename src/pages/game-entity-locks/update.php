<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->PageNotFound();
}

$entity_lock = isset($_POST['entity_ids']) ? implode(',', $_POST['entity_ids']) : '';

$chart_ids = [];
foreach ($_POST['gc_ids'] as $gc_id) {
  $load_id = substr($gc_id, 1);
  switch (substr($gc_id, 0, 1)) {
  case 'g':
    $group_chart_ids = pluck(database_get_all(database_select("
      SELECT level_id FROM levels WHERE game_id = ? AND group_id = ?
    ", 'ss', [$game['game_id'], $load_id])), 'level_id');
    $chart_ids = array_merge($chart_ids, $group_chart_ids);
    break;
  case 'c':
    $chart_ids []= intval($load_id);
    break;
  }
}

database_update_by('levels', [
  'min_entities' => $_POST['min_entities'],
  'max_entities' => $_POST['max_entities'],
  'entity_lock' => $entity_lock,
], [
  'level_id' => $chart_ids,
  'game_id' => $game['game_id'],
]);

$cs->WriteNote(TRUE, "Entity locks successfully updated");

redirect_to_back();
