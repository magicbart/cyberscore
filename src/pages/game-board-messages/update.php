<?php

$message = database_find_by('game_board_messages', ['message_id' => $_GET['id']]);

Authorization::authorize('Admin', $message['user_id']);

$params = filter_post_params([
  Params::str('message_text'),
]);

if (empty($params['message_text'])) {
  exit;
}

database_update_by('game_board_messages', [
  'message_text' => $params['message_text'],
  'edit_date' => database_now(),
], [
  'message_id' => $message['message_id'],
]);

$cs->WriteNote(true, 'Message edited');

redirect_to("/game-board-topics/{$message['topic_id']}");
