<?php

Authorization::authorize('GameMod');

if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
  $cs->LeavePage("/homepics/new", "No file uploaded");
}

$image = imagecreatefrompng($_FILES['file']['tmp_name']) ?: imagecreatefromjpeg($_FILES['file']['tmp_name']);
if (!$image) {
  $cs->WriteNote(false, "File type not supported, rankbutton must be a PNG or a JPG");
}

$width = imagesx($image);
$height = imagesy($image);

var_dump([$width, $height]);

if ($width == 127 && $height == 190) {
  $folder = 'small';
} else if ($width == 209 && $height == 200) {
  $folder = 'big';
} else {
  $cs->WriteNote(false, "The picture doesn't have the correct dimensions: {$width}x{$height}px");
  redirect_to_back();
}

$sha256 = hash('sha256', file_get_contents($_FILES['file']['tmp_name']));

$root = $config['app']['root'];
if (!move_uploaded_file($_FILES['file']['tmp_name'], "$root/public/uploads/homepics/$folder/$sha256")) {
  $cs->WriteNote(false, "Error uploading file.");
  redirect_to_back();
}

$cs->WriteNote(true, "Upload successful");
redirect_to_back();
