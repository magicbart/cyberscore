<?php

Authorization::authorize('GameMod');

$series = database_get_all(database_select("SELECT * FROM series_info ORDER BY series_name ASC", '', []));

render_with('game-series/index', [
  'page_title' => 'Series Management',
  'series' => $series,
]);
