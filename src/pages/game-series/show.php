<?php

Authorization::authorize('GameMod');

$series = database_find_by('series_info', ['series_id' => $_GET['id'] ?? null]);

$games = database_fetch_all("
  SELECT games.game_id, game_name, series.entry_id AS selected
  FROM games
  LEFT JOIN series ON (series.game_id = games.game_id AND series.series_id = ?)
  ORDER BY game_name ASC
", [$series['series_id']]);

render_with('game-series/show', [
  'page_title' => 'Series Management',
  'series' => $series,
  'games' => $games,
]);

