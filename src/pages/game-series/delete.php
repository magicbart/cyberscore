<?php

Authorization::authorize('GameMod');

$series = database_find_by('series_info', ['series_id' => $_GET['id']]);
if (!$series) {
  $cs->PageNotFound();
}

database_delete_by('series', ['series_id' => $series['series_id']]);
database_delete_by('series_info', ['series_id' => $series['series_id']]);

$cs->WriteNote(false, "Series deleted");
redirect_to('/game-series');
