<?php

Authorization::authorize('Admin');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('password'),
  Params::str('password_confirmation'),
]);

if ($params['password'] != $params['password_confirmation']) {
  $cs->WriteNote(false, "Password mismatch");
  $cs->RedirectToPreviousPage();
}

if (strlen($params['password']) < 8) {
  $cs->WriteNote(false, "Password invalid");
  $cs->RedirectToPreviousPage();
}

Authentication::store_password($params['password'], $user);

$cs->WriteNote(true, "Password reset");
redirect_to('/users');
