<?php

Authorization::authorize('GameMod');

$rule_id = intval($_GET['id']);
$rule = RulesRepository::get($rule_id);

if (!isset($rule)) {
  $cs->LeavePage('/rules');
}

$level_info = database_get(database_select("
  SELECT COUNT(level_rules) as num_levels
  FROM levels
  WHERE level_rules LIKE ?
    OR level_rules LIKE ?
    OR level_rules LIKE ?
    OR level_rules LIKE ?
", 'ssss', ["$rule_id", "%,$rule_id", "$rule_id,%", "%,$rule_id,%"]));

$game_info = database_get_all(database_select("
  SELECT COUNT(levels.game_id) as num_levels, games.game_id
  FROM levels
  JOIN games USING (game_id)
  WHERE level_rules LIKE ?
    OR level_rules LIKE ?
    OR level_rules LIKE ?
    OR level_rules LIKE ?
  GROUP BY levels.game_id
", 'ssss', ["$rule_id", "%,$rule_id", "$rule_id,%", "%,$rule_id,%"]));

render_with('rules/edit', [
  'page_title' => 'Rule editor',
  'rule' => $rule,
  'level_info' => $level_info,
  'game_info' => $game_info,
]);
