<?php

Authorization::authorize('Dev');

$user_to_impersonate = UsersRepository::get($_POST['user_id']);

if (!$user_to_impersonate) {
  $cs->WriteNote(false, "Couldn't find user");
  $cs->RedirectToPreviousPage();
}

Session::Store('impersonator_id', $current_user['user_id']);
Session::Store('user_id', $user_to_impersonate['user_id']);

$cs->WriteNote(true, "You're now acting as " . h($user_to_impersonate['username']));
$cs->RedirectToPreviousPage();
