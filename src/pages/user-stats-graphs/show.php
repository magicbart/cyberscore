<?php

$user = UsersRepository::get($_GET['id']);
$is_small = isset($_GET['small']);

if (!$user) exit;

// 1-30 places
// This stores how many charts the player is in first place, second place, etc, until the 30th place.
// Position 0 is the number of platinums the player has.
$values = array_fill(0, 31, 0);

$user_records = database_get_all(database_select("
SELECT records.chart_pos, COUNT(records.chart_pos) AS num_subs
FROM records JOIN games USING(game_id)
WHERE games.site_id < 3 AND records.chart_subs >= 3 AND records.user_id = ? AND records.ranked = 1 AND records.chart_pos <= 30
GROUP BY records.chart_pos
", 's', [$user['user_id']]));

$max_num_subs = 10;
foreach ($user_records as $record) {
  $values[$record['chart_pos']] = $record['num_subs'];
  $max_num_subs = max($record['num_subs'] + 100 - $record['num_subs'] % 50, $max_num_subs);
}
$values[0] = database_single_value("SELECT SUM(records.platinum) FROM records WHERE user_id = ? AND ranked = 1", 's', [$user['user_id']]);

// Current scoreboard position, with a default to last place.
$current_sb_pos = database_single_value("SELECT scoreboard_pos FROM sb_cache WHERE user_id = ?", 's', [$user['user_id']]);
if ($current_sb_pos == null) {
  $current_sb_pos = database_single_value("SELECT COUNT(*) FROM sb_cache", '', []) + 1;
}

// Scoreboard History of the last 24 days
$sb_entries = [$current_sb_pos];
$sb_history = database_fetch_all("
  SELECT scoreboard_pos
  FROM sb_history
  WHERE user_id = ?
  ORDER BY history_date DESC
  LIMIT 24
", [$user['user_id']]);

foreach ($sb_history as $sb_entry) {
  $sb_entries []= $sb_entry['scoreboard_pos'];
}

$sb_entries = array_reverse($sb_entries);

// Draw the image

//size of the image
$img_width = $is_small ? 390 : 600;
$img_height = 400;

//put a border around it
$margins = $is_small ? 25 : 40;

//find the height and width of the actual image
$graph_width = $img_width - $margins * 2;
$graph_height = $img_height - $margins * 2;

$img = imagecreate($img_width,$img_height);

$bar_width = $is_small ? 10 : 15;
$total_bars = $is_small ? 15 : 30;
$gap = ($graph_width - $total_bars * $bar_width) / ($total_bars + 1);

$bar_color = imagecolorallocate($img, 0, 102, 153);
$alt_bar_color = imagecolorallocate($img, 0, 153, 204);
$plat_bar_color = imagecolorallocate($img, 230, 230, 230);
$gold_bar_color = imagecolorallocate($img, 204, 204, 153);
$silver_bar_color = imagecolorallocate($img, 204, 204, 204);
$bronze_bar_color = imagecolorallocate($img, 204, 153, 102);
$background_color = imagecolorallocate($img, 255, 255, 255);
$border_color = imagecolorallocate($img, 200, 200, 200);
$line_color = imagecolorallocate($img, 220, 220, 220);

$black = imagecolorallocate($img, 0, 0, 0);
$green = imagecolorallocate($img, 0, 200, 0);

imagefilledrectangle($img,0,0,$img_width,$img_height,$black);
//grey bit
imagefilledrectangle($img,1,1,$img_width-2,$img_height-2,$border_color);
//middle bit
imagefilledrectangle($img,$margins,$margins,$img_width-1-$margins,$img_height-1-$margins,$background_color); 

$root = $config['app']['root'];
$font = "$root/includes/ARIALUNI.ttf";
imagettftextc($img, $is_small ? 10 : 13, 0, $is_small ? 195 : 300, $is_small ? 15 : 20, $black, $font, $t['profile_record_positions']);
imagettftextc($img, $is_small ? 6 : 8, 0, $is_small ? 195 : 300, $img_height - ($is_small ? 10 : 13), $black, $font, $t['profile_chart_positions']);

# ------- Max value is required to adjust the scale -------
$ratio = $graph_height / $max_num_subs;

$min_sb_pos = min($sb_entries) - 1;
$max_sb_pos = max($sb_entries) + 1;
$adjustment = 10 - (($max_sb_pos - $min_sb_pos) % 10);

$min_sb_pos -= floor($adjustment / 2);
$max_sb_pos += ceil($adjustment / 2);
$ratio_sb = $max_sb_pos - $min_sb_pos;
// Finally fix for negatives
if ($min_sb_pos < 1) {
  $min_sb_pos = 1;
  $max_rb_pos = 1 + $ratio_sb;
}

# -------- Create scale and draw horizontal lines  --------
$horizontal_lines = 10;
$horizontal_gap = $graph_height / $horizontal_lines;

for ($i = 0; $i <= $horizontal_lines; $i++) {
  $y = $img_height - $margins - $horizontal_gap * $i;
  imageline($img, $margins, $y, $img_width - $margins, $y, $line_color);

  // 1-30 places count
  imagestring($img, 0, $is_small ? 3 : 20, $y - 5, (int)($horizontal_gap * $i / $ratio), $black);
  // Scoreboard pos
  imagestring($img, 0, $img_width - ($is_small ? 20 : 30), $y - 5, (int)($min_sb_pos + (10 - $i) / 10 * $ratio_sb), $black);
}

// Add some shadow to the text

# ----------- Draw the bars here ------
for ($i = $total_bars; $i >= 0; $i--) {
  $x1 = $margins + $gap + max($i - 1, 0) * ($gap + $bar_width);
  $x2 = $x1 + $bar_width;
  $y1 = $margins + $graph_height - (int)($values[$i] * $ratio) ;
  $y2 = $img_height - $margins;

  imagestring($img, 0, round($x1 + 8 - imagefontwidth(1) * strlen($values[$i]) / 2, 0), $y1 - 10, $values[$i], $black);
  if ($i > 0) {
    imagestring($img, 0, round($x1 + 8 - imagefontwidth(1) * strlen($i) / 2, 0), $img_height - ($is_small ? 22 : 35), $i, $black);
  }

  switch ($i) {
  case 0:				$this_color = $plat_bar_color; break;
  case 1:				$this_color = $gold_bar_color; break;
  case 2:				$this_color = $silver_bar_color; break;
  case 3:				$this_color = $bronze_bar_color; break;
  case ($i%2 == 0):	$this_color = $bar_color; break;
  default:			$this_color = $alt_bar_color; break;
  }
  imagefilledrectangle($img, round($x1, 0), round($y1, 0), round($x2, 0), round($y2, 0), $this_color);
}

//---------------------------------- SCOREBOARD HISTORY -----------------------------------------
for ($i = 0; $i < count($sb_entries); $i++) {
  $x = $margins + $graph_width * ($i + 1) / (count($sb_entries) + 1);
  $y = $margins + $graph_height * ($sb_entries[$i] - $min_sb_pos) / $ratio_sb;
  imagefilledellipse($img, round($x, 0), round($y, 0), 6, 6, $green);

  if ($i > 0) { imageline($img, round($x, 0), round($y, 0), round($last_x, 0), round($last_y, 0), $green);}

  $last_x = $x;
  $last_y = $y;
}
//-------------------------------- SCOREBOARD HISTORY END -----------------------------------------

header("Content-type: image/png");
imagepng($img);
imagedestroy($img);
