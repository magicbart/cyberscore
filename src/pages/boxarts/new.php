<?php

Authorization::authorize('GameMod');

$t->CacheGameNames();

$game = GamesRepository::get($_GET['game_id'] ?? database_single_value("SELECT MAX(game_id) FROM games WHERE site_id = 1", '', []));

$game_name = $t->GetGameName($game['game_id']);

$latest_games_without_boxart = database_get_all(database_select("
  SELECT *
  FROM games
  LEFT JOIN boxarts USING (game_id)
  WHERE boxarts.boxart_id IS NULL
  ORDER BY date_published DESC
  LIMIT 7
", '', []));


$game_list = database_get_all(database_select("
  SELECT games.game_id, GROUP_CONCAT(platforms.platform_id, '_', platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ',') AS platforms
  FROM games
  JOIN game_platforms USING (game_id)
  JOIN platforms USING(platform_id)
  WHERE (games.site_id < 4 OR games.game_id = ?) AND game_platforms.original = 1
  GROUP BY games.game_id
  ORDER BY games.game_name ASC
", 's', [$game['game_id']]));
foreach ($game_list as &$g) {
  $g['name'] = $t->GetGameName($g['game_id']);
}
unset($g);

$game_boxarts = group_by($cs->GetBoxArts($game['game_id']), fn($b) => $b['platform_id']);

$game_platforms = database_get_all(database_select("
  SELECT game_platforms.platform_id, platforms.platform_name
  FROM game_platforms
  JOIN platforms USING(platform_id)
  WHERE game_platforms.game_id = ? AND game_platforms.original = 1
  ORDER BY platforms.platform_name ASC
", 's', [$game['game_id']]));

$game_platforms = index_by($game_platforms, 'platform_id', 'platform_name');

render_with('boxarts/new', [
  'page_title' => 'Upload boxart',
  'game' => $game,
  'game_name' => $game_name,
  'games' => $game_list,
  'game_platforms' => $game_platforms,
  'latest_games_without_boxart' => $latest_games_without_boxart,
  'game_boxarts' => $game_boxarts,
]);
