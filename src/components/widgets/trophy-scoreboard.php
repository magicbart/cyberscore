<?php
$entries = database_fetch_all("
  SELECT
    users.user_id,
    users.username,
    sb_cache_trophy.scoreboard_pos,
    sb_cache_trophy.trophy_points,
    sb_cache_trophy.platinum,
    sb_cache_trophy.gold,
    sb_cache_trophy.silver,
    sb_cache_trophy.bronze
  FROM sb_cache_trophy
  JOIN users USING(user_id)
  WHERE sb_cache_trophy.scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache_trophy WHERE user_id = ?),
    1
  )
  ORDER BY sb_cache_trophy.scoreboard_pos ASC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/trophy-scoreboard', [
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
