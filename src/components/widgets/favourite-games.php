<?php

render_component_template('widgets/favourite-games', [
  'game_ids' => array_filter($game_ids, fn($x) => $x),
  'widget_user' => $widget_user,
]);
