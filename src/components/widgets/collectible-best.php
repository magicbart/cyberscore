<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'collectible', 'top');

render_component_template('widgets/submissions', [
  'id' => 'collectible-best',
  'title' => t('widget_title_collectible_best'),
  'scoreboard' => 'collectible',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
