<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'speedrun', 'top');

render_component_template('widgets/submissions', [
  'id' => 'speedrun-best',
  'title' => t('widget_title_speedrun_best'),
  'scoreboard' => 'speedrun',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
