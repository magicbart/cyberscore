<?php
$entries = database_fetch_all("
  SELECT
    users.user_id,
    users.username,
    sb_cache_incremental.scoreboard_pos,
    sb_cache_incremental.cxp,
    sb_cache_incremental.vs_cxp,
    sb_cache_incremental.base_level
  FROM sb_cache_incremental
  JOIN users USING(user_id)
  WHERE sb_cache_incremental.scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache_incremental WHERE user_id = ?),
    1
  )
  ORDER BY sb_cache_incremental.scoreboard_pos ASC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/incremental-scoreboard', [
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
