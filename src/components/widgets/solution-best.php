<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'solution', 'top');

render_component_template('widgets/submissions', [
  'id' => 'solution-best',
  'title' => t('widget_title_solution_best'),
  'scoreboard' => 'solution',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
