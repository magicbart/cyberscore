<?php

render_component_template('site-stats/general_users', [
  'user' => database_get(database_select("SELECT COUNT(user_id) AS num_users, COUNT(DISTINCT country_code) AS num_countries FROM users", '', [])),
  'user24' => database_get(database_select("SELECT COUNT(user_id) AS num_users FROM users WHERE date_lastseen >= DATE_SUB(NOW(),INTERVAL 24 HOUR)", '', [])),
  'user30' => database_get(database_select("SELECT COUNT(user_id) AS num_users FROM users WHERE date_lastseen >= DATE_SUB(NOW(),INTERVAL 30 DAY)", '', [])),
  'suser' => database_get(database_select("SELECT COUNT(DISTINCT user_id) AS num_users FROM records", '', [])),
]);
