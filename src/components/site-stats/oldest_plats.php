<?php

$remainingplats = database_get_all(database_select("
  SELECT record_id, last_update, level_id, game_id, user_id
  FROM records
  WHERE platinum = 1
  ORDER BY last_update ASC
  LIMIT 50
", '', []));

$now = time();
foreach ($remainingplats as &$plat) {
  $plat['username']   = database_single_value("SELECT username FROM users WHERE user_id = ?", 's', [$plat['user_id']]);
  $plat['chartname']  = database_single_value("SELECT level_name FROM levels WHERE level_id = ?", 's', [$plat['level_id']]);
  $plat['game_name']  = database_single_value("SELECT game_name FROM games WHERE game_id = ?", 's', [$plat['game_id']]);
  $plat['group_id']   = database_single_value("SELECT group_id FROM levels WHERE level_id = ?", 's', [$plat['level_id']]);
  $plat['group_name'] = database_single_value("SELECT group_name FROM level_groups WHERE group_id = ?", 's', [$plat['group_id']]);

  $diff = $now - strtotime($plat['last_update']);
  $plat['days'] = round($diff / 60 / 60 / 24,0);
}
unset($plat);

render_component_template('site-stats/oldest_plats', [
  'remainingplats' => $remainingplats,
]);
