<?php
$mostprovensubmissions = database_get_all(database_select("
  SELECT COUNT(user_id) AS count, game_id, game_name
  FROM records
  LEFT JOIN games USING (game_id)
  WHERE rec_status = 3 GROUP BY game_id
  ORDER BY count DESC
  LIMIT 100
", '', []));

render_component_template('site-stats/most_proven_submissions', [
  'mostprovensubmissions' => $mostprovensubmissions,
]);
