<?php
$mostplayersgames = database_get_all(database_select("
  SELECT COUNT(DISTINCT user_id) AS count, records.game_id, game_name
  FROM records
  LEFT JOIN games ON records.game_id = games.game_id
  GROUP BY game_id
  ORDER BY count DESC
  LIMIT 100
", '', []));

render_component_template('site-stats/most_player_game', [
  'mostplayersgames' => $mostplayersgames,
]);
