<?php
// Look to optimize the below query's performance in the future
$most_unique_plats = database_get_all(database_select("
  SELECT COUNT(DISTINCT user_id) AS player_count, games.game_id, game_name
  FROM gsb_cache_medals
  LEFT JOIN games ON gsb_cache_medals.game_id = games.game_id
  WHERE platinum >= 1
  GROUP BY game_id
  ORDER BY player_count DESC, game_id ASC
  LIMIT 100
", '', []));

render_component_template('site-stats/most_platinum_medal_holders', [
  'most_unique_plats' => $most_unique_plats,
]);
