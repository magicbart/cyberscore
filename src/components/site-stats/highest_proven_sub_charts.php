<?php
$mostprovencharts = database_get_all(database_select("
  SELECT COUNT(*) AS count, level_id
  FROM records WHERE rec_status = 3
  GROUP BY level_id ORDER BY count DESC LIMIT 100
", '', []));

foreach ($mostprovencharts as &$level) {
  $level['chartinfo'] = database_get(database_select("SELECT level_name, game_id, group_id FROM levels WHERE level_id = ?", 's', [$level['level_id']]));
  $level['groupname'] = database_single_value("SELECT group_name FROM level_groups WHERE group_id = ?", 's', [$level['chartinfo']['group_id']]);
  $level['gamename'] = database_single_value("SELECT game_name FROM games WHERE game_id = ?", 's', [$level['chartinfo']['game_id']]);
}
unset($level);

render_component_template('site-stats/highest_proven_sub_charts', [
  'mostprovencharts' => $mostprovencharts,
]);
