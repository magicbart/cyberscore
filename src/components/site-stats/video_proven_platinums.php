<?php

render_component_template('site-stats/video_proven_platinums', [
  'mostprovenplatinumsvideo' => database_get_all(database_select("
    SELECT COUNT(platinum) AS count, records.user_id, users.username
    FROM records
    LEFT JOIN users ON records.user_id = users.user_id
    WHERE rec_status = 3 AND platinum = 1 AND ranked = 1 AND proof_type IN('v', 's', 'r')
    GROUP BY user_id
    ORDER BY count DESC
    LIMIT 100
", '', [])),
]);
