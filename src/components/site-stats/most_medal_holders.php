<?php
// Look to optimize the below query's performance in the future
$most_unique_medals = database_get_all(database_select("
SELECT COUNT(DISTINCT user_id) AS player_count, game_id, game_name
FROM gsb_cache_medals
LEFT JOIN games USING (game_id)
WHERE gold > 0 OR silver > 0 OR bronze > 0
GROUP BY game_id
ORDER BY player_count DESC, game_id ASC
LIMIT 100
", '', []));

render_component_template('site-stats/most_medal_holders', [
  'most_unique_medals' => $most_unique_medals,
]);
