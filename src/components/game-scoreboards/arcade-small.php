<?php

$scoreboard = $cs->GetGameArcadeboard($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/arcade-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
