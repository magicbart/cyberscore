<?php

render_component_template('tabbed-sidebar', [
  'sidebar' => $sidebar,
  'src' => $src,
  'base_path' => $name,
]);
