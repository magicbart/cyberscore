<?php

$user_id = $user['user_id'];

//Scoreboard information
$csr         = database_find_by("sb_cache", ['user_id' => $user_id]);
$medal       = database_find_by("sb_cache_standard", ['user_id' => $user_id]);
$arcade      = database_find_by("sb_cache_arcade", ['user_id' => $user_id]);
$speedrun    = database_find_by("sb_cache_speedrun", ['user_id' => $user_id]);
$solution    = database_find_by("sb_cache_solution", ['user_id' => $user_id]);
$challenge   = database_find_by("sb_cache_challenge", ['user_id' => $user_id]);
$incremental = database_find_by("sb_cache_incremental", ['user_id' => $user_id]);
$collectible = database_find_by("sb_cache_collectible", ['user_id' => $user_id]);
$subs        = database_find_by("sb_cache_total_subs", ['user_id' => $user_id]);
$proof       = database_find_by("sb_cache_proof", ['user_id' => $user_id]);
$video_proof = database_find_by("sb_cache_video_proof", ['user_id' => $user_id]);
$rainbow     = database_find_by("sb_cache_rainbow", ['user_id' => $user_id]);
$trophy      = database_find_by("sb_cache_trophy", ['user_id' => $user_id]);

//Record status counters
$num_pi = database_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 1", [$user_id]);
$num_ui = database_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 2", [$user_id]);
$num_proofs = database_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 3", [$user_id]);
$num_pending_approval = database_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status >= 4", [$user_id]);

//Other useful info calculations
if ($rainbow['solution_pos'] != NULL &&
    $rainbow['arcade_pos'] != NULL &&
    $rainbow['proof_pos'] != NULL &&
    $rainbow['video_proof_pos'] != NULL &&
    $rainbow['speedrun_pos'] != NULL &&
    $rainbow['collectible_pos'] != NULL &&
    $rainbow['incremental_pos'] != NULL &&
    $rainbow['trophy_pos'] != NULL &&
    $rainbow['medal_pos'] != NULL &&
    $rainbow['starboard_pos'] != NULL &&
    $rainbow['total_submissions_pos'] != NULL &&
    $rainbow['challenge_pos'] != NULL) {
  $avg_rank = round((
    $rainbow['starboard_pos'] +
    $rainbow['medal_pos'] +
    $rainbow['trophy_pos'] +
    $rainbow['arcade_pos'] +
    $rainbow['speedrun_pos'] +
    $rainbow['solution_pos'] +
    $rainbow['challenge_pos'] +
    $rainbow['incremental_pos'] +
    $rainbow['collectible_pos'] +
    $rainbow['proof_pos'] +
    $rainbow['video_proof_pos'] +
    $rainbow['total_submissions_pos']
  ) / 12);
} else {
  $avg_rank = null;
}

//Latest submissions
$latest_subs = database_fetch_all("
  SELECT
    records.*,
    POW((records.csp) / 100,4) AS csr,
    chart_modifiers2.*,
    levels.*,
    users.username,
    users.country_code,
    users.country_id,
    users.user_groups,
    level_groups.ranked
  FROM records
  JOIN users USING(user_id)
  JOIN levels USING(level_id)
  JOIN chart_modifiers2 USING(level_id)
  JOIN level_groups USING(group_id)
  WHERE records.user_id = ?
  ORDER BY records.last_update DESC
  LIMIT 10", [$user_id]);

// Dynamic tables that reshape for each user
foreach ($latest_subs as &$latest) {
  $t->CacheGame($latest['game_id']);

  $latest['awards'] = chart_awards(...Modifiers::ChartModifiers($latest));
  $latest['game_name'] = $t->GetGameName($latest['game_id']);
  $latest['group_name'] = $t->GetGroupName($latest['group_id']);
  $latest['chart_name'] = $t->GetChartName($latest['level_id']);
  $latest['colour'] = Modifiers::ChartFlagColouration($latest);
  $latest['group_colour'] = Modifiers::GroupColouration($latest['ranked']);
}
unset($latest);

render_component_template('user-stats/submissions', [
  'user' => $user,
  'csr' => $csr,
  'medal' => $medal,
  'trophy' => $trophy,
  'rainbow' => $rainbow,
  'avg_rank' => $avg_rank,
  'speedrun' => $speedrun,
  'arcade' => $arcade,
  'solution' => $solution,
  'challenge' => $challenge,
  'collectible' => $collectible,
  'incremental' => $incremental,
  'subs' => $subs,
  'num_pending_approval' => $num_pending_approval,
  'num_ui' => $num_ui,
  'num_pi' => $num_pi,
  'proof' => $proof,
  'video_proof' => $video_proof,
  'latest_subs' => $latest_subs,
]);
