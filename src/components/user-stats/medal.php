<?php

$sort = $_GET['sort'] ?? "percentage";

switch ($sort) {
  case 'position':       $order = 'scoreboard_pos ASC, percentage DESC'; break;
  case 'num_subs':       $order = 'num_subs DESC'; break;
  case 'platinum':       $order = 'platinum DESC, gold DESC, silver DESC, bronze DESC'; break;
  case 'num_approved':   $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
  case 'num_approved_v': $order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
  case 'gold':           $order = 'gold DESC'; break;
  case 'silver':         $order = 'silver DESC'; break;
  case 'bronze':         $order = 'bronze DESC'; break;
  case 'percentage':     $order = 'percentage DESC, platinum DESC, gold DESC, silver DESC, bronze DESC'; break;
  default:               $order = 'percentage DESC, platinum DESC, gold DESC, silver DESC, bronze DESC'; break;
}

$user_games = database_get_all(database_select("
  SELECT
  games.*,
  games.standard_charts as eligible_charts,
  num_subs, scoreboard_pos, medal_points, platinum, gold, silver, bronze, rpc, percentage, num_approved, num_approved_v
  FROM gsb_cache_medals
  LEFT JOIN games USING (game_id)
  WHERE gsb_cache_medals.user_id = ?
  ORDER BY $order
", 'i', [$user['user_id']]));

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache_standard', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => 'subs_bar_other', 'value' => 100 * $game['num_subs'] / $game['eligible_charts']],
    ['class' => 'subs_bar_bronze', 'value' => 100 * ($game['gold'] + $game['silver'] + $game['bronze']) / $game['eligible_charts']],
    ['class' => 'subs_bar_silver', 'value' => 100 * ($game['gold'] + $game['silver']) / $game['eligible_charts']],
    ['class' => 'subs_bar_gold', 'value' => 100 * $game['gold'] / $game['eligible_charts']],
    ['class' => 'subs_bar_plat', 'value' => 100 * $game['platinum'] / $game['eligible_charts']],
  ];
  $values []= [
    ['width' => 3, 'value' => number_format($game['platinum'], 0)],
    ['width' => 3, 'value' => number_format($game['gold'], 0)],
    ['width' => 3, 'value' => number_format($game['silver'], 0)],
    ['width' => 3, 'value' => number_format($game['bronze'], 0)],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,

  'chart_status' => 'standard',
  'scoreboard' => 'medal',
  'default_color' => 'standard',
  'scoreboard_label' => t('general_medal_table'),
  'columns' => [
    ["width" => 3, "icon" => "/images/icon-platinumstar.png",  "alt" => "Platinum", "sort" => "platinum"],
    ["width" => 3, "icon" => "/images/icon-goldstar-v2.png",   "alt" => "Gold", "sort" => "gold"],
    ["width" => 3, "icon" => "/images/icon-silverstar-v2.png", "alt" => "Silver", "sort" => "silver"],
    ["width" => 3, "icon" => "/images/icon-bronzestar-v2.png", "alt" => "Bronze", "sort" => "bronze"],
  ],
  'totals' => [
    number_format($cache['platinum'], 0),
    number_format($cache['gold'], 0),
    number_format($cache['silver'], 0),
    number_format($cache['bronze'], 0),
  ],
  'total_bars' => [
    ['class' => 'subs_bar_other', 'value' => 100],
    ['class' => 'subs_bar_bronze', 'value' => 100 * ($cache['gold'] + $cache['silver'] + $cache['bronze']) / $cache['num_subs']],
    ['class' => 'subs_bar_silver', 'value' => 100 * ($cache['gold'] + $cache['silver']) / $cache['num_subs']],
    ['class' => 'subs_bar_gold', 'value' => 100 * $cache['gold'] / $cache['num_subs']],
    ['class' => 'subs_bar_plat', 'value' => 100 * $cache['platinum'] / $cache['num_subs']],
  ],
  'bars' => $bars,
  'values' => $values,
]);
