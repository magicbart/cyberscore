<?php

$flairs = ChartIcon::all_selections($user);

$flairs = array_map(fn ($x) => [
  'visible' => array_count($x[1], fn($f) => $f['available']) > 0,
  'label' => $x[0],
  'selection' => $x[1],
], $flairs);

$monthly_badges = database_get_all(database_select("
  SELECT * FROM new_chart_badges WHERE user_id = ?
", 's', [$user['user_id']]));

render_component_template('user-stats/badges', [
  'flairs' => $flairs,
  'monthly_badges' => $monthly_badges,
]);
