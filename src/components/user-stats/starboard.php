<?php

$sort = $_GET['sort'] ?? "percentage";

$order = 'percentage DESC, scoreboard_pos ASC, total_csp DESC, total_csr DESC, bonus_csr DESC';

switch ($sort) {
case 'position':        $order = 'scoreboard_pos ASC, percentage DESC'; break;
case 'num_subs':        $order = 'num_subs DESC'; break;
case 'num_approved':	  $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
case 'num_approved_v':	$order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
case 'csp':             $order = 'total_csp DESC, total_csr DESC, bonus_csr DESC'; break;
case 'total_csr':       $order = 'total_csr DESC, bonus_csr DESC'; break;
case 'bonus_csr':       $order = 'bonus_csr DESC, total_csr DESC'; break;
case 'percentage':      $order = 'percentage DESC, scoreboard_pos ASC, total_csp DESC, total_csr DESC, bonus_csr DESC'; break;
default:                $order = 'percentage DESC, scoreboard_pos ASC, total_csp DESC, total_csr DESC, bonus_csr DESC'; break;
}

$user_games = database_get_all(database_select("
  SELECT games.*,
    games.csp_charts as eligible_charts,
    scoreboard_pos, num_subs, total_csp, total_csr, bonus_csr, percentage, num_approved, num_approved_v
  FROM gsb_cache_csp
  LEFT JOIN games USING (game_id)
  WHERE gsb_cache_csp.user_id = ?
  ORDER BY $order
", 's', [$user['user_id']]));

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => 'subs_bar_standard', 'value' => round(100 * $game['num_subs'] / $game['eligible_charts'], 2)],
  ];
  $values []= [
    ['width' => 22, 'value' => number_format($game['total_csp'], 2)],
    ['width' => 22, 'value' => number_format($game['total_csr'], 2) . " <small>(" . number_format($game['bonus_csr'], 2) . ")</small>"],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,
  'default_color' => 'standard',
  'chart_status' => 'all',
  'scoreboard' => 'starboard',
  'scoreboard_label' => t('general_mainboard'),
  'columns' => [
    ['width' => 22, 'icon' => '/images/CSStar.png', 'alt' => t('general_csp'), 'sort' => 'csp', 'label' => t('general_csp')],
    ['width' => 22, 'icon' => '/images/CSStar.png', 'alt' => t('general_csr'), 'sort' => 'total_csr', 'label' => t('general_csr'), 'label2' => t('general_bonus_csr_short'), 'sort2' => 'bonus_csr'],
  ],
  'totals' => [
    '',
    number_format($cache['total_csr'], 2) . " <small>(" . number_format($cache['bonus_csr'], 2) . ")</small>",
  ],
  'total_bars' => [
    ['class' => 'subs_bar_standard', 'value' => 100],
  ],
  'bars' => $bars,
  'values' => $values,
]);
