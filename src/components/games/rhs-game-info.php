<?php
// requires: $game, $chart_id, $chart_name, $rank, $group_id


$boxarts = $cs->GetBoxArts($game['game_id']);
$discord_servers = database_fetch_all("SELECT server_id, text, link FROM discord_servers WHERE game_id = ?", [$game['game_id']]);
$num_topics = database_value("SELECT COUNT(*) FROM game_board_topics WHERE game_id = ?", [$game['game_id']]);

// Do we need to show the game rebuild countdown? If so, include the cron update calculator
$rebuild = database_value("SELECT TRUE FROM cacherebuilder WHERE game_id = ?", [$game['game_id']]);
if ($rebuild) { require_once("includes/cron_update_calculator.php"); }

$series_list = database_fetch_all("
  SELECT series_id, series_name
  FROM series
  LEFT JOIN series_info USING (series_id)
  WHERE game_id = ?
", [$game['game_id']]);
?>

<div class='sidebar-section sidebar-section--game-info'>
  <h2 class="sidebar-title sidebar-title--<?= $rank ?? 'solution' ?>">
    <?= h($t['game_info']) ?>
  </h2>

  <div class='sidebar-section-body'>
    <?php if (count($boxarts) > 0) { ?>
      <?= boxart_image_tag($boxarts[0]) ?>
    <?php } ?>

    <div style='text-align: center'>
      <h1 class="gamename"><?= link_to_game($game) ?></h1>
      <?php if (isset($chart_id)) { ?>
        <h3><?= h($chart_name) ?></h3>
      <?php } ?>

      <!-- Displays a game's addition date -->
      <?php if ($game['date_published'] != "0000-00-00 00:00:00" && $game['date_published'] != NULL) { // Game publish date & time ?>
        <strong>
          <?= h($t['game_date_published']) ?>
          <?= h($t->FormatDateTime($game['date_published'], 'date')) ?>
        </strong>
      <?php } ?>
    </div>

    <!-- Game series -->
    <?php if (count($series_list) > 0) { ?>
      <ul class="arrows">
        <?php foreach ($series_list as $series) { ?>
          <li><a href="/scoreboards/starboard?series=<?= $series['series_id'] ?>"><?= h($series['series_name']) ?></a></li>
        <?php } ?>
      </ul>
    <?php } ?>

    <!-- Discord servers -->
    <?php if (count($discord_servers) > 0) { ?>
      <ul class="discord">
        <?php foreach ($discord_servers as $server) { ?>
          <li><a href="<?= h($server['link']) ?>"><?= h($server['text']) ?></a></li>
        <?php } ?>
      </ul>
    <?php } ?>

    <!-- Game board & submission/proof listings -->
    <ul class="pencil">
      <li><a href="/game-boards/<?= h($game['game_id']) ?>"><?= $num_topics == 1 ? $t['game_topic_singular'] : str_replace('[topics]', $num_topics, $t['game_topic_plural']) ?></a></li>
      <!-- We don't show these on charts -->
      <?php if(!isset($rank)) { ?>
        <li><a href="/latest-submissions?game_id=<?= h($game['game_id']) ?>"><?= h($t['general_latest_subs']) ?></a></li>
        <li><a href="/game-proofs/<?= h($game['game_id']) ?>"><?= $t['game_view_proofs'] ?></a></li>
      <?php } ?>
    </ul>

    <!-- This TR houses the game's platform icons -->
    <div class='platforms'>
      <?php foreach($game['platforms'] AS $platform) { ?>
        <a href="/games.php?platform_id=<?= h($platform['platform_id']) ?>">
          <img class='platform-image' src="/uploads/platforms/<?= h($platform['platform_id'])?>.gif" alt="<?= h($platform['platform_name']) ?>" title="<?= h($platform['platform_name']) ?>" />
        </a>
      <?php } ?>
    </div>

    <!-- Game rebuilder! -->
    <?php if ($rebuild) { // Game rebuild countdown ?>
      <p><?= str_replace("[time]", Rebuilders::game()->time_to_next(), $t['games_rebuild']) ?></p>
      <?php if (Authorization::has_access('GameMod')) { ?>
        <form action="/admin/rebuild_cache" method="post">
          <input type="hidden" name="game_id" value="<?= $game['game_id'];?>">
          <input type="submit" name="submit" value="Rebuild manually">
        </form><br>
      <?php } ?>
    <?php } ?>
  </div>
</div>
