<?php
$discord_servers = database_get_all(database_select("
  SELECT server_id, text, link, game_id
  FROM discord_servers
  WHERE game_id = ?
", 'i', [$game_id]));
?>
<?php if (count($discord_servers) > 0) { ?>
  <h2>Existing Discord Servers</h2>
  <ul class="arrows">
    <?php foreach ($discord_servers as $server) { ?>
      <li>
        <form action="/discords/delete" method="post">
          <?= hidden_field('server_id', $server) ?>
          <a href="<?= h($server['link']) ?>"><?= h($server['text']) ?></a>
          <input type="image" alt="submit" src="/images/delete.png" width="11" height="11" name="submit" value="Delete Server">
        </form>

        <form action="/discords/update" method="post">
          <?= hidden_field('server_id', $server) ?>
          <b>Text: </b><input type="text" name="text" value="<?= h($server['text']) ?>" />
          <br />
          <b>Link: </b><input type="text" name="link" value="<?= h($server['link']) ?>" />
          <br />
          <input type="submit" name="submit" value="Make changes">
          <br />
          <br />
        </form>
      </li>
    <?php } ?>
  </ul>
<?php } ?>

<h2>Add new Discord Server</h2>
<form action="/discords/create" method="post">
  <?= hidden_field('game_id', ['game_id' => $game_id]) ?>
  Text: <input type="text" name="text" /><br />
  Link: <input type="text" name="link" /><br /><br />
  <input type="submit" alt="submit" value="Add Server">
</form>
