<details class="sidebar-section" data-id="data-summary" open="" ontoggle="rememberDetailsState(this)">
  <summary class="sidebar-title">Data breakdown</summary><br>

  <table>
    <tr>
      <th></th>
      <th style="text-align: center;">Charts</th>
      <th style="text-align: center;">Players</th>
      <th style="text-align: center;">Subs</th>
      <th style="text-align: center;">RPC</th>
    </tr>
    <?php if($game['all_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/CSStar.png" alt="All charts" title="All charts"></td>
        <td style="text-align: center;"><b><?= number_format($game['all_charts'],0) ?></b></td>
        <td style="text-align: center;"><b><?= number_format($game['all_players'],0) ?></b></td>
        <td style="text-align: center;"><b><?= number_format($game['all_subs'],0) ?></b></td>
        <td style="text-align: center;"><b><?= number_format($game['all_subs'] / $game['all_charts'], 2) ?></b></td>
      </tr>
    <?php } ?>
    <?php if($game['csp_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_trophy.png" alt="CSP-awarding charts" title="CSP-awarding charts"></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['csp_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['csp_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['csp_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['csp_subs'] / $game['csp_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['standard_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_ranked.png" alt="<?= $t['chart_ranked_upper'] ?>" title="<?= $t['chart_ranked_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['standard_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['standard_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['standard_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_standard"><b><?= number_format($game['standard_subs'] / $game['standard_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['arcade_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_arcade.png" alt="<?= $t['chart_arcade_upper'] ?>" title="<?= $t['chart_arcade_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_arcade"><b><?= number_format($game['arcade_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_arcade"><b><?= number_format($game['arcade_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_arcade"><b><?= number_format($game['arcade_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_arcade"><b><?= number_format($game['arcade_subs'] / $game['arcade_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['speedrun_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_speedrun.png" alt="<?= $t['chart_speedrun_upper'] ?>" title="<?= $t['chart_speedrun_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_speedrun"><b><?= number_format($game['speedrun_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_speedrun"><b><?= number_format($game['speedrun_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_speedrun"><b><?= number_format($game['speedrun_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_speedrun"><b><?= number_format($game['speedrun_subs'] / $game['speedrun_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['challenge_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_challenge.png" alt="<?= $t['chart_challenge_upper'] ?>" title="<?= $t['chart_challenge_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_challenge"><b><?= number_format($game['challenge_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_challenge"><b><?= number_format($game['challenge_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_challenge"><b><?= number_format($game['challenge_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_challenge"><b><?= number_format($game['challenge_subs'] / $game['challenge_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['solution_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_solution.png" alt="<?= $t['chart_solution_upper'] ?>" title="<?= $t['chart_solution_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_solution"><b><?= number_format($game['solution_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_solution"><b><?= number_format($game['solution_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_solution"><b><?= number_format($game['solution_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_solution"><b><?= number_format($game['solution_subs'] / $game['solution_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['collectible_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_collectible.png" alt="<?= $t['chart_collectible_upper'] ?>" title="<?= $t['chart_collectible_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_collectible"><b><?= number_format($game['collectible_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_collectible"><b><?= number_format($game['collectible_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_collectible"><b><?= number_format($game['collectible_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_collectible"><b><?= number_format($game['collectible_subs'] / $game['collectible_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if ($game['incremental_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_incremental.png" alt="<?= $t['chart_incremental_upper'] ?>" title="<?= $t['chart_incremental_upper'] ?>"></td>
        <td style="text-align: center;"><span class="text_incremental"><b><?= number_format($game['incremental_charts'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_incremental"><b><?= number_format($game['incremental_players'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_incremental"><b><?= number_format($game['incremental_subs'],0) ?></b></span></td>
        <td style="text-align: center;"><span class="text_incremental"><b><?= number_format($game['incremental_subs'] / $game['incremental_charts'], 2) ?></b></span></td>
      </tr>
    <?php } ?>
    <?php if($game['unranked_charts'] > 0) { ?>
      <tr>
        <td style="text-align: center; vertical-align: center;"><img class="stats_small" src="/images/icon_unranked.png" alt="<?= $t['chart_unranked_upper'] ?>" title="<?= $t['chart_unranked_upper'] ?>"></td>
        <td style="text-align: center;"><?= number_format($game['unranked_charts'],0) ?></td>
        <td style="text-align: center;"><?= number_format($game['unranked_players'],0) ?></td>
        <td style="text-align: center;"><?= number_format($game['unranked_subs'],0) ?></td>
        <td style="text-align: center;"><?= number_format($game['unranked_subs'] / $game['unranked_charts'], 2) ?></td>
      </tr>
    <?php } ?>
  </table>
</details>
