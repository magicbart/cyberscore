<tr>
  <th colspan="4"></th>

  <th class="medals">
    <img src=" <?= skin_image_url('Dropdown_icons/icon_ranked_speedrun.png') ?>" height="100" alt="<?= $t['general_speedrun_time'] ?>" title="<?= $t['general_speedrun_time'] ?>">
    <?= h($t['general_speedrun_time']) ?>
  </th>
  <th class="medals" style="padding-left: 50px;">
    <img src="/images/icon_speedrun.png" height="100" alt="<?= $t['general_speedrun_points'] ?>" title="<?= $t['general_speedrun_points'] ?>">
    <?= h($t['general_speedrun_points']) ?>
  </th>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="medals"><?= h($entry['speedrun_time']) ?></td>
    <td class="medals" style="padding-left: 50px;"><?= h(number_format($entry['speedrun_points'])) ?></td>
  </tr>
<?php } ?>
