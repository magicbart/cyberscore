<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="scoreboardCSR">
      <?= h(number_format($entry['confirmed'], 0)) ?>
      <?= h($entry['confirmed'] == 1 ? $t['scoreboards_referral_single'] : $t['scoreboards_referral_plural']) ?>
      <small>(<?= h($entry['unconfirmed']) ?> <?= h($t['scoreboards_referral_unconfirmed']) ?>)</small>
    </td>
  </tr>
<?php } ?>
