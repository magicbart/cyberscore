<?php
  $template_entry = [
    'user_id' => 0,
    'num_subs' => 0,
    'cxp' => 0,
    'base_level' => 0,
    'next' => 0,
    'remaining' => 0,
    'last' => 0,
  ];
?>
<tr>
<th colspan="4"></th>
  <th>
    <a href="<?= current_url_with_query(['manual_sort' => 'vxp']) ?>">
    <?= h($t['general_vs_cxp']) ?>
    </a>
  </th>

  <th style="text-align: center;">
    <a href="<?= current_url_with_query(['manual_sort' => 'cxp']) ?>">
    <?= h($t['general_cxp']) ?>
    </a>
  </th>
</tr>
<?php foreach($entries as $entry) { 
  
  // Doing PHP here because of the restricted structure of the scoreboards
  $entry['last'] = Award::ExperienceFromLevel($entry['base_level']);
  $entry['next'] = Award::ExperienceFromLevel($entry['base_level'] + 1);
  $entry['remaining'] = $entry['next'] - $entry['cxp']; ?>

  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="scoreboardCSR"><?= h(number_format($entry['vs_cxp'])) ?> <?= h($t['general_vs_cxp']) ?> </td>
    <td class="scoreboardCSR" style="text-align: center;"><br><?= h($t['general_level']) ?> <?= h(number_format($entry['base_level'])) ?>
    <small> (<?= h(number_format($entry['cxp'],1)) ?> <?= h($t['general_cxp']) ?>)</small><br>
    <canvas
          class="proof_chart"
          style="display: inline-block; text-align: centre; max-height: 43px; max-width: 180px;"
          width="180"
          height="43"
          data-json="<?= h(json_encode(array_intersect_key($entry, $template_entry))) ?>"
      ></canvas></td>
  </tr>
<?php } ?>
<script>
const labels = [
  "Progress",
  "Remaining",
];

const colors = [
  "#ceba05", // incremental
  "rgba(0, 0, 0, 0.3)",    // unproven
];

const best = Math.max(...Array.from(document.querySelectorAll(".proof_chart")).map((chart) =>
JSON.parse(chart.dataset.json).next - JSON.parse(chart.dataset.json).last
));

Array.from(document.querySelectorAll(".proof_chart")).forEach((chart) => {
  const entry = JSON.parse(chart.dataset.json);

  const data = [
    entry.cxp - entry.last,
    entry.remaining,
  ];

  // alternative representation using a stacked horizontal bar chart
  if (true) {
    var datasets = [];
    for (var i = 0; i < labels.length; i++) {
      datasets[i] = {
        label: labels[i],
        data: [data[i]],
        backgroundColor: [colors[i]],
        hoverBackgroundColor: [colors[i]],
      };
    }

    new Chart(chart, {
      type: 'horizontalBar',
      data: { datasets },
      options: {
        layout: { padding: { left: 45, bottom: 20, top: 5, right: 55} },
        legend: { display: false },
        tooltips: { mode: 'point' },
        scales: {
          xAxes: [{
            barPercentage: 1.0,
            categoryPercentage: 1.0,
            stacked: true,
            gridLines: {
                display: false
            },
            ticks: {
              display: false,
              min: 0,
              padding: 0,
              max: Number(data.reduce((a,b) => Number(a) + Number(b))),
            }
          }],
          yAxes: [{
            barPercentage: 1.0,
            categoryPercentage: 1.0,
            stacked: true,
            gridLines: {
                display: false
            },
            ticks: { display: false, padding: 0 },
          }],
        },
      }
    });
  } else {
    const datasets = [{
      data,
      backgroundColor: colors,
    }];

    new Chart(chart, {
      type: 'doughnut',
      data: { labels, datasets },
      options: {
        rotation: 0.9 * Math.PI,
        circumference: 1.2 * Math.PI,
        cutoutPercentage: 60,
        legend: {
          display: false
        },
        animation: {
          animateRotate: true
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        }
      }
    });
  }
});
</script>
