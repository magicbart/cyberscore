<?php [$latest_cache_date, $cache_before_last_date] = $scoreboard->dates(); ?>
<tr>
  <td colspan="6">
    <p>Showing changes from <?= h($t->FormatDateTime($cache_before_last_date, 'date')) ?> to <?= h($t->FormatDateTime($latest_cache_date, 'date')) ?> on the Mainboard.</p>
  </td>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr<?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <?= link_to_user($entry) ?>
      <?= user_star_image_tags($entry) ?>
      <br />
      <?php $new_subs = $entry['num_subs'] - $entry['previous_num_subs']; ?>
      <?php if (isset($entry['num_subs'])) { ?>
        <small>
          <?= number_format($entry['num_subs']) ?>
          <?= plural_word($entry['num_subs'], 'submission') ?>
          <?php if ($new_subs == 0) { ?>
            (±0)
          <?php } else if ($new_subs < 0) { ?>
            (<span style="color: #FF0000;"><?= h($new_subs) ?></span>)
          <?php } else { ?>
            (<span style="color: #008800;">+<?= h($new_subs) ?></span>)
          <?php } ?>
        </small>
      <?php } ?>
    </td>
    <?php $CSR_change = number_format($entry['csr'] - $entry['previous_csr'], 2); ?>
    <td class="scoreboardCSR">
      <?= h(number_format($entry['csr'], 2)) ?>
      <?php if ($CSR_change == 0) { ?>
        (±0)
      <?php } else if ($CSR_change < 0) { ?>
        (<span style="color: #FF0000;"><?= h($CSR_change) ?></span>)
      <?php } else { ?>
        (<span style="color: #008800;">+<?= h($CSR_change) ?></span>)
      <?php } ?>
    </td>

    <?php if (!array_key_exists('previous_position', $entry)) { ?>
      <td style="font-size:1.4em; color:#23538A; font-family: 'Trebuchet MS',Verdana,Arial,Helvetica,sans-serif; font-weight:bold; vertical-align:center">
        New user!
      </td>
    <?php } else { ?>
      <?php $movement = $entry['previous_position'] - $entry['position']; ?>
      <?php if ($movement == 0) { ?>
        <td style="vertical-align:center">
          <img src="<?= skin_image_url('equals.png') ?>" style="height:25px"/>
        </td>
      <?php } elseif ($movement < 0) { ?>
        <td style="font-size:30px; color:#ff0000; font-family: 'Trebuchet MS',Verdana,Arial,Helvetica,sans-serif; font-weight:bold; vertical-align:center">
          <img src="<?= skin_image_url('red_arrow.png') ?>" style="height:30px"/> <?php echo str_replace('-','',$movement);?>
        </td>
      <?php } elseif ($movement > 0) { ?>
        <td style="font-size:30px; color:#008800; font-family: 'Trebuchet MS',Verdana,Arial,Helvetica,sans-serif; font-weight:bold; vertical-align:center">
          <img src="<?= skin_image_url('green_arrow.png') ?>" style="height:30px"/> <?php echo $movement;?>
        </td>
      <?php } ?>
    <?php } ?>
  </tr>
<?php } ?>
