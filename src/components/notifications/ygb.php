<div class="notification-item__icon">
  <span class='fa-stack fa-1x'>
    <i class="xxx fas fa-stack-1x fa-slash"></i>
    <i class="xxx far fa-stack-1x fa-star"></i>
  </span>
</div>

<?php $obsolence_check = database_find_by('records', ['user_id' => $current_user['user_id'], 'level_id' => $notification['chart_id']]); ?>

<div class="notification-item__message">
  <strong><?= h(str_replace('[user]', $notification['sender_username'], $t['notifications_type_ygb'])) ?></strong>
    <?php if($obsolence_check['chart_pos'] == 1) { ?> 
      <span style="color: green">
      However, you seem to no longer be beat... 
    </span>
      <?php } ?>
  <br>
  <?= h($notification['game_name']) ?> &gt;
  <?= h($notification['group_name']) ?> &gt;
  <?= h($notification['level_name']) ?>
</div>
