<div class="notification-item__icon">
  <span class='fa-stack fa-1x'>
  <i class="xxx far fa-check-circle"></i>
  </span>
</div>
<?php $type_string = Trophies::TypeHandling($notification['note_details']); ?>

<div class="notification-item__message">
  <strong><?= h(str_replace('[user]', $notification['sender_username'], $t['notifications_type_trophy_lost'])) ?></strong>
  <br>
  <?= h($notification['game_name']) ?> &gt;
  <?= h($type_string['text']) ?> &gt;
  <?= h(nth($notification['position'])) ?> &rarr;
  <?= h(nth($notification['position'] + 1)) ?>
</div>
