<div class="notification-item__icon">
  <i class="xxx fas fa-gamepad"></i>
</div>

<div class="notification-item__message">
  <strong><?= str_replace('[user]', $notification['sender_username'], $t['notifications_type_new_game']) ?></strong>
  <br>
  <?= h(first_line($notification['game_name'])) ?>
</div>
