<div class="notification-item__icon">
  <i class="xxx fas fa-user-friends"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_referral_confirmed_3'] ?></strong>
  <br>
  <?php if ($notification['referral_id']) { ?>
    <?php if ($notification['target_user_id'] != $notification['referrer_user_id']) { ?>
      <?= h(str_replace('[user]', $notification['referred_username'], $t['notifications_type_referral_confirmed_1'])) ?>
    <?php } else { ?>
      <?= h(str_replace('[user]', $notification['referrer_username'], $t['notifications_type_referral_confirmed_2'])) ?>
    <?php } ?>
  <?php } else { ?>
    <?= $t['notifications_type_referral_confirmed_3'] ?>
  <?php } ?>
</div>
