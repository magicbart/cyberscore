<div class="notification-item__icon">
  <i class="xxx fas fa-exclamation-circle"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_reported'] ?></strong>
  <br>
  <?= h($notification['game_name']); ?> &gt;
  <?= h($notification['group_name']); ?> &gt;
  <?= h($notification['level_name']); ?>
</div>
