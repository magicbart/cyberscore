<div class="notification-item__icon">
  <i class="xxx far fa-check-circle"></i>
</div>

<div class="notification-item__message">
  <strong>
    <?= h(str_replace('[user]', $notification['sender_username'], $t['notifications_type_approved'])) ?>
  </strong>

  <br>
  <?= h($notification['game_name']) ?> &gt;
  <?= h($notification['group_name']) ?> &gt;
  <?= h($notification['level_name']) ?>
</div>
