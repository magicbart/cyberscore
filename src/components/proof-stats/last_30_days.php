<?php

$recent_users = database_get_all(database_select("
  SELECT users.username, COUNT(*) AS num_approved
  FROM record_approvals
  JOIN users USING(user_id)
  WHERE record_approvals.approval_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()
  GROUP BY users.user_id, users.username
  ORDER BY num_approved DESC
", '', []));

render_component_template('proof-stats/last_30_days', [
  'recent_users' => $recent_users,
]);
