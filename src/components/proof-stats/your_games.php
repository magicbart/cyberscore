<?php

// TODO: why is this not just using current_user?
$moderator = UsersRepository::get($current_user['user_id']);

if (!$moderator) {
  exit;
}

$approved_games = database_get_all(database_select("
  SELECT COUNT(*) AS num, games.*
  FROM record_approvals
  JOIN records USING(record_id)
  JOIN games ON records.game_id = games.game_id
  WHERE record_approvals.user_id = ?
  GROUP BY games.game_id
  ORDER BY num DESC
", 's', [$moderator['user_id']]));

render_component_template('proof-stats/your_games', [
  'approved_games' => $approved_games,
]);
