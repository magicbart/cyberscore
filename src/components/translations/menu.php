<?php
$pending_country_names_count = database_single_value("
  SELECT COUNT(1)
  FROM translation_db
  WHERE translation_db.table_field = 'country_name' AND translation_db.is_active = 'n'
", '', []);

$pending_rule_count = database_single_value("
  SELECT COUNT(1)
  FROM translation_db
  WHERE translation_db.table_field = 'rule_text' AND translation_db.is_active = 'n'
", '', []);

$pending_proof_rule_count = database_single_value("
  SELECT COUNT(1)
  FROM translation_db
  WHERE translation_db.table_field = 'proof_rule_text' AND translation_db.is_active = 'n'
", '', []);


$languages = database_get_all(database_select("
  SELECT language_id, language_name, language_code
  FROM languages
  WHERE sub_language = 'n' AND language_id != 1
  ORDER BY language_code ASC
", '', []));

$translate_lang_id = $t->GetTranslateLang();

render_component_template('translations/menu', [
  'translate_lang_id' => $t->GetTranslateLang(),
  'pending_country_names_count' => $pending_country_names_count,
  'pending_rule_count' => $pending_rule_count,
  'pending_proof_rule_count' => $pending_proof_rule_count,
  'languages' => $languages,
]);
