<form action="/game_scoreboard.php" id="customdropdown" method="get">
  <select name="board" onchange="this.form.action=this.form.submit();">
    <?php foreach ($scoreboards as $scoreboard) { ?>
      <?php if (count($scoreboard['entries']) > 0) { ?>
        <option
          value="<?= h($scoreboard['scoreboard']) ?>"
          data-image="<?= skin_image_url("Dropdown_icons/{$scoreboard['icon']}.png") ?>"
          <?= option_selected($scoreboard['scoreboard'], $board) ?>
        ><?= h($t[$scoreboard["label"]]) ?></option>
      <?php } ?>
    <?php } ?>
  </select>
  <input type="hidden" name="game_id" value="<?= h($game['game_id']) ?>">
</form>
<br />
<br />
<script language="javascript">
$(document).ready(function(e) {
  try {
    $("#customdropdown select").msDropDown();
  } catch(e) {}
});
</script>
