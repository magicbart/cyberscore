<?php

$api_tokens = ApiTokenRepository::where(['user_id' => $current_user['user_id']]);

render_component_template('settings/password', [
  'api_tokens' => $api_tokens,
]);
