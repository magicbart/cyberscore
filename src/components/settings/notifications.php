<?php

$preferences = database_find_by('notification_prefs', ['user_id' => $current_user['user_id']]);

render_component_template('settings/notifications', [
  'preferences' => $preferences,
]);
