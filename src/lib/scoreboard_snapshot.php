<?php

class ScoreboardSnapshot {
  public static function SnapshotAll() {
    wikilog("snapshot mainboard");
    self::SnapshotMainboard();
    wikilog("snapshot standard");
    self::SnapshotStandard();
    wikilog("snapshot arcade");
    self::SnapshotArcade();
    wikilog("snapshot speedrun");
    self::SnapshotSpeedrun();
    wikilog("snapshot solution");
    self::SnapshotSolution();
    wikilog("snapshot challenge");
    self::SnapshotChallenge();
    wikilog("snapshot collectible");
    self::SnapshotCollectible();
    wikilog("snapshot incremental");
    self::SnapshotIncremental();
  }

  public static function SnapshotMainboard() {
    ScoreboardRebuildClass::RebuildMainboard();

    $entries = database_fetch_all("SELECT * FROM sb_cache", []);

    self::Snapshot("mainboard", [
      'user_id',
      'scoreboard_pos',
      'total_csr',
      'bonus_csr',
      'num_subs',
    ]);
  }

  public static function SnapshotStandard() {
    ScoreboardRebuildClass::RebuildMedalTable();
    self::Snapshot("standard", [
      'user_id',
      'scoreboard_pos',
      'medal_points',
      'platinum',
      'gold',
      'silver',
      'bronze',
      'num_subs',
    ]);
  }

  public static function SnapshotArcade() {
    ScoreboardRebuildClass::RebuildArcadeCache();
    self::Snapshot("arcade", [
      'user_id',
      'scoreboard_pos',
      'arcade_points',
      'num_subs',
    ]);
  }

  public static function SnapshotSpeedrun() {
    ScoreboardRebuildClass::RebuildSpeedrunCache();
    self::Snapshot("speedrun", [
      'user_id',
      'scoreboard_pos',
      'speedrun_points',
      'platinum',
      'gold',
      'silver',
      'bronze',
      'num_subs',
    ]);
  }

  public static function SnapshotSolution() {
    ScoreboardRebuildClass::RebuildSolutionCache();
    self::Snapshot('solution', [
      'user_id',
      'scoreboard_pos',
      'brain_power',
      'num_subs',
    ]);
  }

  public static function SnapshotChallenge() {
    ScoreboardRebuildClass::RebuildChallengeCache();
    self::Snapshot('challenge', [
      'user_id',
      'scoreboard_pos',
      'cp',
      'num_subs',
    ]);
  }

  public static function SnapshotCollectible() {
    ScoreboardRebuildClass::RebuildCollectibleCache();
    self::Snapshot('collectible', [
      'user_id',
      'scoreboard_pos',
      'cyberstars',
      'num_subs',
    ]);
  }

  public static function SnapshotIncremental() {
    ScoreboardRebuildClass::RebuildIncrementalCache();
    self::Snapshot('incremental', [
      'user_id',
      'scoreboard_pos',
      'cxp',
      'vs_cxp',
      'base_level',
      'num_subs',
    ]);
  }

  public static function Snapshot($board, $fields) {
    // TODO: Some of these tables don't match the pattern,
    //       they should maybe be renamed.
    if ($board == 'mainboard') {
      $source = 'sb_cache';
      $target = 'sb_history';
    } else {
      $source = "sb_cache_$board";
      $target = "sb_history_$board";
    }

    $entries = database_fetch_all("SELECT * FROM $source", []);

    $values = [];
    foreach ($entries as $entry) {
      $record = ['history_date' => database_now()];

      foreach ($fields as $field) {
        $record[$field] = $entry[$field];
      }

      $values []= $record;
    }

    database_insert_all($target, $values);
  }
}
