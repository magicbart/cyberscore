<?php

require_once('includes/base_repository.php');

class GroupsRepository extends BaseRepository {
  protected static $table_name = 'level_groups';
  protected static $primary_key = 'group_id';
}

