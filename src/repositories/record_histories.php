<?php

require_once('includes/base_repository.php');

class RecordHistoriesRepository extends BaseRepository {
  protected static $table_name = 'record_history';
  protected static $primary_key = 'history_id';
}
