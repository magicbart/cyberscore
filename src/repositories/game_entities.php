<?php

require_once('includes/base_repository.php');

class GameEntitiesRepository extends BaseRepository {
  protected static $table_name = 'game_entities';
  public static $primary_key = 'game_entity_id';
  protected static $relations = [
    'inherited_entity' => ['GameEntitiesRepository', 'inherited_entity_id'],
  ];
}


// GameEntitiesRepository::get(1)['inherited_entity']['game_entity_id'];
// g = select * from game_entities where pkey = 1
// select * from game_entities where pkey = g.inherited_entity_id
