<?php

require_once('includes/base_repository.php');

class DiscordServersRepository extends BaseRepository {
  protected static $table_name = 'discord_servers';
  protected static $primary_key = 'server_id';
}
