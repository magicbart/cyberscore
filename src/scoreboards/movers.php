<?php

require_once("src/scoreboards/filters.php");

class MoversAndLosersScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['country'];
  }

  public function dates() {
    if (!isset($this->dates)) {
      $this->dates = pluck(database_get_all(database_select("
        SELECT DISTINCT history_date
        FROM sb_history
        ORDER BY history_date DESC
        LIMIT 2
      ", '', [])), 'history_date');
    }

    return $this->dates;
  }

  public function count() {
    [$current_date, ] = $this->dates();

    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM sb_history
      JOIN users USING (user_id)
      WHERE history_date = ? AND $filters
     ", 's', [$current_date]);
  }

  public function fetch($limit, $offset) {
    [$current_date, $previous_date] = $this->dates();

    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        sb_history.scoreboard_pos AS position,
        sb_history.num_subs,
        sb_history.total_csr + sb_history.bonus_csr AS csr,
        previous.scoreboard_pos AS previous_position,
        previous.num_subs AS previous_num_subs,
        previous.total_csr + previous.bonus_csr AS previous_csr
      FROM sb_history
      JOIN users USING (user_id)
      LEFT JOIN sb_history previous ON (previous.user_id = sb_history.user_id AND previous.history_date = ?)
      WHERE sb_history.history_date = ? AND $filters
      ORDER BY position
      LIMIT $limit
      OFFSET $offset
    ", 'ss', [$previous_date, $current_date]));
  }
}
