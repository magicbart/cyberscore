<?php

require_once("src/scoreboards/filters.php");

class StaffScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM sb_cache_staff
      JOIN users USING (user_id)
      WHERE 1=1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case 'total_tasks':               return 'total_tasks DESC, teamwork_power DESC';
      case 'games_added':               return 'games_added DESC, teamwork_power DESC';
      case 'record_investigated':       return 'record_investigated DESC, teamwork_power DESC';
      case 'record_moved':              return 'record_moved DESC, teamwork_power DESC';
      case 'record_deleted':            return 'record_deleted DESC, teamwork_power DESC';
      case 'photo_proof_approved':      return 'photo_proof_approved DESC, teamwork_power DESC';
      case 'video_proof_approved':      return 'video_proof_approved DESC, teamwork_power DESC';
      case 'proof_refused':             return 'proof_refused DESC, teamwork_power DESC';
      case 'rankbutton_uploaded':       return 'rankbutton_uploaded DESC, teamwork_power DESC';
      case 'support_request_archived':  return 'support_request_archived DESC, teamwork_power DESC';
      case 'support_request_deleted':   return 'support_request_deleted DESC, teamwork_power DESC';
      case 'news_article_written':      return 'news_article_written DESC, teamwork_power DESC';
      case 'record_reinstated':         return 'record_reinstated DESC, teamwork_power DESC';
      case 'record_reverted':           return 'record_reverted DESC, teamwork_power DESC';
      default:                          return 'teamwork_power DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        teamwork_power,
        total_tasks,
        games_added,
        record_investigated,
        record_moved,
        record_deleted,
        photo_proof_approved,
        video_proof_approved,
        proof_refused,
        rankbutton_uploaded,
        support_request_archived,
        support_request_deleted,
        news_article_written,
        record_reinstated,
        record_reverted
      FROM sb_cache_staff
      JOIN users USING (user_id)
      WHERE 1=1 AND $filters
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}

//| teamwork_power           | int                | NO   |     | NULL    |       |
//| total_tasks              | int                | NO   |     | NULL    |       |
//| games_added              | int                | NO   |     | NULL    |       |
//| record_investigated      | int                | NO   |     | NULL    |       |
//| record_moved             | int                | NO   |     | NULL    |       |
//| record_deleted           | int                | NO   |     | NULL    |       |
//| photo_proof_approved     | int                | NO   |     | NULL    |       |
//| video_proof_approved     | int                | NO   |     | NULL    |       |
//| proof_refused            | int                | NO   |     | NULL    |       |
//| rankbutton_uploaded      | int                | NO   |     | NULL    |       |
//| support_request_archived | int                | NO   |     | NULL    |       |
//| support_request_deleted  | int                | NO   |     | NULL    |       |
//| news_article_written     | int                | NO   |     | NULL    |       |
//| record_reinstated        | int                | NO   |     | NULL    |       |
//| record_reverted          | int                | NO   |     | NULL    |       |
