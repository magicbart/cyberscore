<?php

require_once("src/scoreboards/filters.php");

class RainbowScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return [];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM sb_cache_rainbow
      WHERE 1=1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    $entries = database_get_all(database_select("
      SELECT
        users.*,
        sb_cache_rainbow.*
      FROM sb_cache_rainbow
      JOIN users USING(user_id)
      WHERE users.user_id > 0 AND $filters
      ORDER BY scoreboard_pos ASC
      LIMIT $limit
      OFFSET $offset
    ", '', []));

    // TODO: add these to sb_cache_rainbow
    foreach ($entries as &$entry) {
      $entry['has_bonus'] = (
        $entry['solution_pos'] != NULL &&
        $entry['arcade_pos'] != NULL &&
        $entry['proof_pos'] != NULL &&
        $entry['video_proof_pos'] != NULL &&
        $entry['speedrun_pos'] != NULL &&
        $entry['collectible_pos'] != NULL &&
        $entry['incremental_pos'] != NULL &&
        $entry['trophy_pos'] != NULL &&
        $entry['medal_pos'] != NULL &&
        $entry['starboard_pos'] != NULL &&
        $entry['total_submissions_pos'] != NULL &&
        $entry['challenge_pos'] != NULL
      );

      if ($entry['has_bonus']) {
        $entry['avg_pos'] = (
          $entry['solution_pos'] +
          $entry['arcade_pos'] +
          $entry['proof_pos'] +
          $entry['video_proof_pos'] +
          $entry['speedrun_pos'] +
          $entry['collectible_pos'] +
          $entry['incremental_pos'] +
          $entry['trophy_pos'] +
          $entry['medal_pos'] +
          $entry['starboard_pos'] +
          $entry['total_submissions_pos'] +
          $entry['challenge_pos']) / 12;

        $entry['multiplier'] = max(2.01 - ($entry['avg_pos'] / 100), 1);
      } else {
        $entry['avg_pos'] = NULL;
        $entry['multiplier'] = NULL;
      }
    }
    unset($entry);

    return $entries;
  }
}
