<?php

require_once("src/scoreboards/filters.php");

class FirstScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;
  public $date;

  public function __construct($game_ids, $country_ids, $date, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
    $this->date = $date;
  }

  public function filters() {
    return ['game', 'country', 'ym'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_first
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(points) AS points
      FROM gsb_cache_first
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY points DESC
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}

