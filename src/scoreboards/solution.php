<?php

require_once("src/scoreboards/filters.php");

class SolutionScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_solution
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(brain_power) AS brain_power
      FROM gsb_cache_solution
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY brain_power DESC
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
