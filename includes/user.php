<?php

class User {
  public static function SendConfirmationEmail($user_id) {
    global $config, $t;

    UsersRepository::update($user_id, [
      'registration_token' => bin2hex(random_bytes(32)),
      'registration_token_expiration' => strtotime('+1 day'),
    ]);

    $user = database_find_by('users', ['user_id' => $user_id]);
    if ($user == NULL) {
      // TODO: how do we handle errors?
    }

    $auth_url = $config['app']['base_url'] . "/accounts/confirm/" . $user['registration_token'];

    return send_email(
      [$user['email']],
      'emails/registration',
      $t['register_mail_subject'],
      [
        'message' => str_replace(
          ['[username]', '[link]'],
          [$user['username'], $auth_url],
          $t['register_mail_message']
        ),
        'signature' => $t['general_automatic_email_1'],
        'automated_warning' => $t['general_automatic_email_2'],
      ]
    );
  }

  public static function ResetAllRecordReports() {
    database_update_by('users', ['recreport' => 10], []);
  }

  public static function IsVerified($user) {
    global $config;
    if ($config['app']['email_confirmation_required'] &&
      $user['email_confirmation_required'] &&
      $user['auth_verified'] == 'n') return false;

    return true;
  }
}
