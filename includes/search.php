<?php

function contains_word($text, $word) {
  $text = str_replace(
    ['.', '?', ':', '!'],
    [' ', ' ', ' '],
    $text);

  return in_array(strtolower($word), explode(" ", strtolower($text)));
}

function search($term) {
  global $t;
  global $cs;

  $term = str_replace(
    ['.', ':', '&amp;', '_',  '%',  "'", '’', ' ', 'Okami', 'okami', 'Ouendan', 'ouendan'],
    ['',  '',  '&',    '\_', '\%', '',  '',   '%', 'Ōkami', 'Ōkami', 'Ōuendan', 'Ōuendan'],
    $term
  );

  $user_results = database_fetch_all("
    SELECT username, user_id
    FROM users
    LEFT JOIN sb_cache USING (user_id)
    WHERE username LIKE ?
    ORDER by sb_cache.num_subs DESC, username ASC
  ", ["%$term%"]);

  foreach ($user_results as &$user) {
    $user['avatar'] = $cs->GetUserPic($user);
  }
  unset($user);

  $game_results = database_fetch_all("
    (
      SELECT
        game_id,
        game_name,
        '' AS language_code,
        1 as language_id,
        game_name AS english_name,
        all_subs / all_charts AS rpc
      FROM games
      WHERE REPLACE(REPLACE(REPLACE(REPLACE(game_name, '.', ''), ':', ''), '’', ''), '\'', '') LIKE ?
      AND site_id < 4
    )
    UNION
    (
      SELECT
        translation_games.game_id,
        translation_games.translation AS game_name,
        languages.language_code,
        languages.language_id,
        games.game_name AS english_name,
        all_subs / all_charts AS rpc
      FROM translation_games
      JOIN languages USING (language_id)
      JOIN games USING (game_id)
      WHERE translation_games.table_field = 'game_name'
      AND translation_games.translation LIKE ?
    )
    ORDER BY rpc DESC, language_id = ? DESC
  ", ["%$term%", "%$term%", $t->GetGameLang()]);

  $preferred_language = $t->GetGameLang();
  usort($game_results, fn ($a, $b) =>
    [-contains_word($a['game_name'], $term), -$a['rpc'], $a['language_id'] != $preferred_language] <=>
    [-contains_word($b['game_name'], $term), -$b['rpc'], $b['language_id'] != $preferred_language]);

  $game_results = array_values(array_map(fn($games) => $games[0], group_by($game_results, fn($g) => $g['game_id'])));

  foreach ($game_results as &$game) {
    $game['boxart'] = $cs->GetBoxArts($game['game_id'])[0];
  }
  unset($game);

  return [$user_results, $game_results];
}
