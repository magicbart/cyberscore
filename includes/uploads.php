<?php

class ImageConverter {
  private $dimensions;
  private $target_format;
  private $prefix;

  public function __construct($dimensions, $target_format, $prefix) {
    $this->dimensions = $dimensions;
    $this->target_format = $target_format;
    $this->prefix = $prefix;
  }

  public function convert($image, $type, $basename) {
    $resized = imagescale($image, $this->dimensions[0], $this->dimensions[1]);

    $prefix = $this->prefix;
    $filename = Uploads::path($type, "$prefix$basename", 'legacy');
    if ($this->target_format == 'jpg') {
      imagejpeg($resized, $filename, 100);
    }

    imagedestroy($resized);
  }
}

class Uploads {
  static $config = [
    'user-banner' => [
      'path' => 'public/userpics/banner',
      'converters' => [
        [[590, 170], 'jpg', ""],
      ],
    ],
    'user-avatar' => [
      'path' => 'public/userpics',
      'converters' => [
        [[100, 100], 'jpg', ""],
        [[50, 50], 'jpg', "t"],
      ],
    ],
  ];

  public static function path($type, $basename, $variant) {
    global $config;
    $root = $config['app']['root'];

    $type_config = self::$config[$type];

    if (basename($basename) != $basename) {
      return NULL;
    } else {
      switch ($variant) {
        case 'directory':
          return "$root/{$type_config['path']}/$basename/";
        case 'original':
          return "$root/{$type_config['path']}/$basename/original";
        default:
          return "$root/{$type_config['path']}/$basename.jpg";
      }
    }
  }

  public static function exists($type, $basename, $variant) {
    return file_exists(self::path($type, $basename, $variant));
  }

  public static function remove($type, $basename) {
    foreach (self::$config[$type]['converters'] as $converter) {
      $prefix = $converter[2];
      $path = self::path($type, "$prefix$basename", 'legacy');
      if (file_exists($path)) {
        unlink($path);
      }
    }

    $path = self::path($type, $basename, 'original');
    if (file_exists($path)) {
      unlink($path);
    }

    $path = self::path($type, $basename, 'directory');
    if (file_exists($path)) {
      rmdir($path);
    }
  }

  public static function save($type, $basename, $uploaded_file) {
    if (!is_uploaded_file($uploaded_file['tmp_name'])) {
      return false;
    }

    $filename = $uploaded_file['tmp_name'];
    $file = getimagesize($filename);

    $loaders = [
      IMAGETYPE_WEBP => 'imagecreatefromwebp',
      IMAGETYPE_JPEG => 'imagecreatefromjpeg',
      IMAGETYPE_GIF => 'imagecreatefromgif',
      IMAGETYPE_PNG => 'imagecreatefrompng',
      IMAGETYPE_BMP => 'imagecreatefrombmp',
    ];

    $loader = $loaders[$file[2]];
    if (!$loader) {
      return false;
    }

    $image = $loader($filename);

    if (!file_exists(self::path($type, $basename, 'directory'))) {
      mkdir(self::path($type, $basename, 'directory'), 0744, true);
    }

    move_uploaded_file($uploaded_file['tmp_name'], self::path($type, $basename, 'original'));

    foreach (self::$config[$type]['converters'] as $converter) {
      (new ImageConverter(...$converter))->convert($image, $type, $basename);
    }

    imagedestroy($image);
    return true;
  }
}
