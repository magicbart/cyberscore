<?php
// Determines time remaining for automatic rebuilds to occur

class Cron {
  public $mcron;
  public $hcron;

  // $mcron must be an array of numbers, like: [0, 30].
  // $hcron must be a string like '*' or '*/3', meaning every hour or every N hours.
  function __construct($mcron, $hcron) {
    $this->mcron = $mcron;
    $this->hcron = $hcron;
  }

  function time_to_next($now = null) {
    $mcron = $this->mcron;
    $hcron = $this->hcron;

    if ($now === null) {
      $now = time();
    }

    $when = $now;

    $m = intval(date('i', $now));
    $h = intval(date('H', $now));

    if ($mcron != '*') {
      if (is_string($mcron) && str_starts_with($mcron, '*/')) {
        $period = intval(substr($mcron, 2));
        $mcron = range(0, 59, $period);
      }

      $when += min(array_map(function($c)use($m){ return 60*((($c - $m) + 60) % 60); }, $mcron));
    }

    if ($hcron != '*') {
      if (is_string($hcron) && str_starts_with($hcron, '*/')) {
        $period = intval(substr($hcron, 2));
        $hcron = range(0, 23, $period);
      }

      $when += min(array_map(function($c)use($h){ return 3600*((($c - $h) + 24) % 24); }, $hcron));
    }

    return ($when - $now) / 60;
  }

  function description() {
    $parts = [];

    if ($this->hcron == '*') {
      if (count($this->mcron) == 2) {
        $parts []= "twice an hour";
      } else {
        $parts []= "every hour";
      }
    } else if ($this->hcron == '*/3') {
      $parts []= "every three hours";
    }

    $minutes = implode(" and ", $this->mcron);
    $parts []= "at $minutes minutes past the hour";

    return implode(", ", $parts);
  }
}

class Rebuilders {
  static function chart() { return new Cron([30], '*'); }
  static function game() { return new Cron([15, 45], '*'); }
  static function global() { return new Cron("*/5", '*'); }
}
