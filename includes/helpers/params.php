<?php

function filter_params($params, $input) {
  $results = [];
  array_walk(
    $params,
    function($param) use(&$results, $input) {
      list($key, $value) = $param($input);

      if ($key) {
        $results[$key] = $value;
      }
    }
  );

  return $results;
}

function filter_post_params($params) {
  global $_POST;

  return filter_params($params, $_POST);
}

class Params {
  public static function str($key) {
    return function($params) use($key) {
      if (array_search($key, array_keys($params)) !== false) {
        return [$key, $params[$key]];
      }
    };
  }

  public static function array_of_strs($key) {
    return function($params) use ($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if (is_array($params[$key])) {
          return [$key, $params[$key]];
        } else {
          return [$key, []];
        }
      } else {
        return [$key, []];
      }
    };
  }

  public static function array_of_numbers($key) {
    return function($params) use ($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if (is_array($params[$key])) {
          return [$key, array_map(function($x){ return self::numval($x); }, $params[$key])];
        } else {
          return [$key, []];
        }
      } else {
        return [$key, []];
      }
    };
  }

  public static function number($key) {
    return function($params) use($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if ($params[$key] === "") {
          return [$key, NULL];
        } else {
          return [$key, self::numval($params[$key])];
        }
      }
    };
  }

  public static function enum($key, $values) {
    return function($params) use($key, $values) {
      if (array_search($key, array_keys($params)) !== false) {
        $value = $params[$key];
        return [$key, in_array($value, $values) ? $value : NULL];
      }
    };
  }

  public static function checkbox($key, $yes, $no) {
    return function($params) use($key, $yes, $no) {
      if (array_search($key, array_keys($params)) !== false) {
        return [$key, $params[$key] == "1" ? $yes : $no];
      }
    };
  }

  public static function partial_date($key) {
    return function($params) use($key) {
      if (array_search("{$key}_day", array_keys($params)) !== false) {
        $day = intval($params["{$key}_day"]);
        $month = intval($params["{$key}_month"]);
        $year = intval($params["{$key}_year"]);

        if ($day == 0 && $month == 0 && $year == 0) {
          return [$key, NULL];
        } else {
          return [$key, sprintf("%04d-%02d-%02d", $year, $month, $day)];
        }
      }
    };
  }

  private static function numval($str) {
    if (mb_strpos($str, ".") !== false) {
      return floatval($str);
    } else {
      return intval($str);
    }
  }
}
