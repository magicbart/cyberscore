<?php

// Full pages are rendered with render_with, which includes an application
// layout and opengraph metadata. Partials are html snippets with no PHP logic
// behind them, to be used by htmx for example. Components are pieces of PHP
// with a template attached.

// We have render_* and rendered_*_result functions. The latter returns the
// rendered string, while the former outputs the rendered string immediately.
//
// The render_* function is necessary because we're using an "imperative" PHP
// style, where page code is top level. There's no Response object.

// Renders a twig template wrapped in the application layout, to
// be used in src/pages/.
function render_with($template, $variables) {
  global $twig;
  global $current_user;
  global $config;

  // This allows us to customise the metadata for a template without placing a hard requirement every single page on the website to declare key variable metadata.

  // Description
  if (!isset($variables['metadata']['description'])) {
    $variables['description'] = "Cyberscore – Video game high scores and fast times";
  } else {
    $variables['description'] = $variables['metadata']['description'];
  }

  // Title; This is set up so that the page_title property used for the PHP renderer will also slot in nicely.
  if(!isset($variables['metadata']['title'])
  && !isset($variables['page_title']))                  $variables['page_title'] = $variables['description'];
  else if (!isset($variables['page_title']))            $variables['page_title'] = $variables['metadata']['title'];

  // Image
  if (!isset($variables['metadata']['image'])) {
    $variables['image'] = "https://cdn.discordapp.com/attachments/768325007288827944/1085874972486025247/csstar.png";
  } else {
    $variables['image'] = $variables['metadata']['image'];
  }

  $variables['template'] = $template;
  $variables['page_class'] = str_replace('/', '-', $template);
  $variables['config'] = $config;
  $variables['current_user'] = $current_user;

  $twig->load("layouts/application.html.twig")->display($variables);
}

function render_json($contents) {
  header("Content-Type: application/json");

  echo json_encode($contents);
}

function render_partial($template, $variables) {
  global $twig;
  global $current_user;

  $variables['current_user'] = $current_user;

  return $twig->load("$template.html.twig")->render($variables);
}

function display_partial($template, $variables) {
  echo render_partial($template, $variables);
}

// Sometimes, we need to have page components that are not just pure HTML, but include
// some PHP code (like fetching something from the database). For this, we have components.
// You call them from your template files using the render_component_with twig helper and
// the src/components/<name>.php file will be invoked. This file must call render_component_template()
// to output some html.
//
// Right now, components are "flat files", meaning they're required directly here and don't have a
// wrapping function. This is just to avoid the boilerplate of having to name these functions and
// registering them into a components list somewhere. This means that we can't "return" the html
// from the component file, so we use a global variable to store the rendered output temporarily.

function render_component($template, $variables) {
  global $__rendered_component_result;

  render_component_with($template, $variables);
  $result = $__rendered_component_result;
  $__rendered_component_result = null;

  return $result;
}

function display_component($template, $variables) {
  echo render_component($template, $variables);
}

/* Variables are all double underscored to prevent cloberring of user provided variables.
 *
 * It also declares some functions as global, which might be accidentally
 * clobbered in our php component files. Ideally we should get rid of these
 * variables and use static functions instead.
 *
 * TODO: This is used directly in some components that haven't been converted to twig yet.
 */
function render_component_with($__template, $__variables) {
  global $t;
  global $cs;
  global $current_user;
  global $config;

  foreach ($__variables as $__name => $__value) {
    $$__name = $__value;
  }
  unset($__variables);

  require("src/components/$__template.php");
}

// Renders a twig template without a wrapping layout. To be used in src/components/.
// It stores the rendered result in a global variable to work around the issue mentioned
// in render_component().
function render_component_template($template, $variables) {
  global $__rendered_component_result;
  $__rendered_component_result = render_partial("components/$template", $variables);
}
