<?php

require_once("src/repositories/referrals.php");

function referral_confirm($referral_id, $reviewer_id) {
  $referral = ReferralsRepository::get($referral_id);

  ReferralsRepository::update($referral_id, ['confirmed' => 1]);

  Notification::ReferralConfirmed($referral_id, $reviewer_id)
    ->DeliverToUsers([$referral['user_id'], $referral['referrer_id']]);
}

function referral_unconfirm($referral_id, $reviewer_id) {
  ReferralsRepository::update($referral_id, ['confirmed' => 0]);
}

function referral_reject($referral_id, $reviewer_id) {
  $referral = ReferralsRepository::get($referral_id);

  Notification::ReferralRejected($referral_id, $reviewer_id)
    ->DeliverToUsers([$referral['user_id'], $referral['referrer_id']]);

  ReferralsRepository::delete($referral_id);
}

function referral_create($referrer_id, $referred_id) {
  $referral_id = ReferralsRepository::create([
    'referrer_id' => $referrer_id,
    'user_id' => $referred_id,
    'confirmed' => 0,
  ]);

  Notification::ReferralCreated($referral_id)->DeliverToGroups(['GlobalMod']);
}
