<?php
class Authentication {
  public static function authenticate($entered_username, $entered_password) {
    // Remove spaces from the entered username,
    // so that the few people with former spaces can login without problems
    $entered_username = str_replace(' ', '', $entered_username);

    $user = database_find_by('users', ['username' => $entered_username]);

    if ($user && self::password_verify($entered_password, $user)) {
      return $user;
    } else {
      return NULL;
    }
  }

  public static function password_verify($entered_password, $user) {
    // Post January 2021, changing password no longer adds single quotes.
    $check1 = $user['pword_new'] && password_verify($entered_password, rtrim($user['pword_new']));

    // Pre January 2021, Passwords were stored with an extra pair of single quotes
    // during registration and password change.
    $check2 = $user['pword_new'] && password_verify("'$entered_password'", rtrim($user['pword_new']));

    // Pre January 2016
    $check3 = $user['pword'] && hash_equals(md5($entered_password), $user['pword']);

    return $check1 || $check2 || $check3;
  }

  public static function store_password($entered_password, $user) {
    database_update_by(
      'users',
      [
        'pword_new' => password_hash($entered_password, PASSWORD_DEFAULT),
        'pword' => 'unused'
      ],
      ['user_id' => $user['user_id']]
    );
  }

  public static function validate_password($password, $confirmation) {
    if ($password !== $confirmation) {
      return [false, 'New passwords do not match'];
    }

    if (strlen($password) < 8) {
      return [false, 'Your new password must be at least 8 characters long'];
    }

    return [true, ''];
  }
}
