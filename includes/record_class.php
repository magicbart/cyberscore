<?php

require_once("src/lib/SimpleImage.php");
require_once("includes/class_scoreboard_rebuilder.php");
require_once("includes/class_chart_rebuilder.php");
require_once("includes/class_game_rebuilder.php");

/* * ****************************************************************
  CLASS RECORDMANAGER
 * **************************************************************** */

function array_diff_debug($a, $b) {
  if ($a != $b) {
    for ($i = 0; $i < count($a); $i++) {
      if ($a[$i] != $b[$i]) {
        wikilog(json_encode($a[$i]));
        wikilog(json_encode($b[$i]));
      }
    }
  }
}

class Maximum {
  public $max = 0;

  public function update($value) {
    $this->max = max($this->max, $value);
    return $this->max;
  }

  // What percentage of maximum is the given value?
  public function percentage($value) {
    return $this->max == 0 ? 0.0 : $value / $this->max;
  }
}

class Rank {
  public $rank = 0;
  public $position = 0;
  public $previous_value = NULL;

  public function update($value) {
    if ($this->position > 0 && $this->previous_value == $value) {
      $this->position++;
    } else {
      $this->position++;
      $this->rank = $this->position;
      $this->previous_value = $value;
    }

    return $this->rank;
  }
}

// This assumes $data is already sorted
function FindMedian($data) {
  // If it's an empty array, return FALSE.
  if (empty($data)) {
    return false;
  }

  // Count how many elements are in the array.
  $num = count($data);

  // Determine the middle value of the array.
  $middleVal = floor(($num - 1) / 2);

  if ($num % 2) {
    // If the size of the array is an odd number, then the middle value is the
    // median.
    return $data[$middleVal];
  } else {
    // If the size of the array is an even number, then we have to get the two
    // middle values and get their average

    // The $middleVal var will be the low end of the middle
    $lowMid = $data[$middleVal];
    $highMid = $data[$middleVal + 1];

    // Return the average of the low and high.
    return (($lowMid + $highMid) / 2);
  }
}

class RecordManager {
  private function CheckForExistingRecord($chart_id, $user_id) {
    $record = database_find_by('records', ['user_id' => $user_id, 'level_id' => $chart_id]);
    if ($record) {
      return $record;
    }

    return false;
  }

  public static function IsUnderInvestigation($record) {
    $rec_status = $record['rec_status'] ?? 0;
    return ($rec_status == 1 || $rec_status == 2 || $rec_status == 5 || $rec_status == 6);
  }

  public static function IsApproved($record) {
    return $record['rec_status'] == 3 || $record['rec_status'] == 4;
  }

  public static function IsApprovedVideo($record) {
    return $record['rec_status'] == 3 && ($record['proof_type'] == 'v' || $record['proof_type'] == 's' || $record['proof_type'] == 'r');
  }

  public static function IsValidScore($chart, $record) {
    if ($chart['conversion_factor'] != 0) {
      return isset($record['submission']) != isset($record['submission2']);
    } else {
      return isset($record['submission']) || isset($record['submission2']);
    }
  }

  public static function SubmitQueuedRecord($queued_record) {
    $chart = LevelsRepository::get($queued_record['chart_id']);
    $record = RecordsRepository::find_by(['level_id' => $queued_record['chart_id'], 'user_id' => $queued_record['user_id']]);

    return self::SubmitRecord(
      $chart,
      $record,
      $queued_record['entity_id'],
      $queued_record['entity_id2'],
      $queued_record['entity_id3'],
      $queued_record['entity_id4'],
      $queued_record['entity_id5'],
      $queued_record['entity_id6'],
      $queued_record['platform_id'],
      $queued_record['submission'],
      $queued_record['submission2'],
      $queued_record['comment'],
      $queued_record['user_id'],
      '',
      $queued_record['extra1'],
      $queued_record['extra2'],
      $queued_record['extra3'],
      $queued_record['game_patch']
    );
  }

  public static function SubmitRecord(
    $chart,
    $existing_record,
    $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6,
    $platform_id, $submission, $submission2, $comment, $user_id, $other_user,
    $extra1, $extra2, $extra3, $game_patch
  ) {
    $valid_entities = pluck(database_fetch_all("SELECT game_entity_id FROM game_entities WHERE game_id = ?", [$chart['game_id']]), 'game_entity_id');
    $available_platform_ids = pluck(database_fetch_all("SELECT platform_id FROM game_platforms WHERE game_id = ?", [$chart['game_id']]), 'platform_id');

    // Just make sure everything is okay
    $min_entities = $chart['min_entities'];
    $max_entities = $chart['max_entities'];
    $entity_lock  = $chart['entity_lock'];

    if (count($valid_entities) == 0) {
        $min_entities = 0;
    }

    if ($entity_lock != '' && $min_entities != 0) {
      $allowed_entities = explode(',', $entity_lock);
      if (!in_array($entity_id, $allowed_entities)) {
        return ['error_entity', NULL, NULL];
      }
    }

    // Truncate comments that are too long. We remove trailing and leading whitespace first to retain the highest amount of actual content possible.
    $comment = trim($comment);
    if (strlen($comment) > 300) {
      $comment = substr($comment, 0, 300);
    }

    if (($entity_id  !== NULL && !in_array($entity_id,  $valid_entities)) ||
      ($entity_id2 !== NULL && !in_array($entity_id2, $valid_entities)) ||
      ($entity_id3 !== NULL && !in_array($entity_id3, $valid_entities)) ||
      ($entity_id4 !== NULL && !in_array($entity_id4, $valid_entities)) ||
      ($entity_id5 !== NULL && !in_array($entity_id5, $valid_entities)) ||
      ($entity_id6 !== NULL && !in_array($entity_id6, $valid_entities))) {
      // invalid entities
      return ['error_entity', NULL, NULL];
    }

    if (($entity_id  === NULL && $min_entities >= 1) ||
      ($entity_id2 === NULL && $min_entities >= 2) ||
      ($entity_id3 === NULL && $min_entities >= 3) ||
      ($entity_id4 === NULL && $min_entities >= 4) ||
      ($entity_id5 === NULL && $min_entities >= 5) ||
      ($entity_id6 === NULL && $min_entities >= 6)) {
      // needs more entities
      return ['error_entity', NULL, NULL];
    }

    if ($platform_id === NULL && count($available_platform_ids) == 1) {
      $platform_id = $available_platform_ids[0];
    }

    if (!in_array($platform_id, $available_platform_ids)) {
      return ['error_platform', NULL, NULL];
    }

    // We have a few options here:
    // - no previous record
    // - previous record had different score
    // - previous record had same score but different metadata
    // - everything is the same, what are you doing?

    if ($existing_record == null) {
      $record_id = self::AddRecord($chart, $user_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);

      ChartRebuildClass::RebuildChart($chart['level_id']);
      self::HandleYGB($chart['game_id'], $chart['level_id'], $record_id);
      self::AddRecordHistoryEntry($record_id, 'f');
      return ['success', 'add', $record_id];
    } else if ($submission != $existing_record['submission'] || ($submission2 != NULL && $submission2 != $existing_record['submission2'])) {
      $old_leaders = database_value("SELECT COUNT(record_id) FROM records WHERE chart_pos = 1 AND level_id = ?", [$chart['level_id']]);

      self::UpdateRecord($existing_record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);

      ChartRebuildClass::RebuildChart($chart['level_id']);
      if ($existing_record['chart_pos'] != 1 || $old_leaders > 1) {
        self::HandleYGB($chart['game_id'], $chart['level_id'], $existing_record['record_id']);
      }
      // Store the old comment in the history and insert a new entry
      self::StoreCommentInHistory($existing_record['record_id'], $existing_record['comment']);
      self::AddRecordHistoryEntry($existing_record['record_id'], 'u');
      return ['success', 'update', $existing_record['record_id']];
    } else if ($entity_id != $existing_record['entity_id'] || $entity_id2 != $existing_record['entity_id2'] ||
      $entity_id3 != $existing_record['entity_id3'] || $entity_id4 != $existing_record['entity_id4'] ||
      $entity_id5 != $existing_record['entity_id5'] || $entity_id6 != $existing_record['entity_id6'] ||
      $platform_id != $existing_record['platform_id'] || $comment != $existing_record['comment'] ||
      (($extra1 != NULL || $existing_record['extra1']) && $extra1 != $existing_record['extra1']) ||
      (($extra2 != NULL || $existing_record['extra2']) && $extra2 != $existing_record['extra2']) ||
      (($extra3 != NULL || $existing_record['extra3']) && $extra3 != $existing_record['extra3']) ||
      $other_user != $existing_record['other_user'] || $game_patch != $existing_record['game_patch']) {
      self::ChangeRecord($existing_record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);
      return ['success', 'change', $existing_record['record_id']];
    } else {
      return ['success', 'no change', $existing_record['record_id']];
    }
  }

  public function ModEditRecord($chart_id, $record_id, $entity_ids, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $linked_proof, $game_patch) {
    [$entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6] = $entity_ids;

    $record = database_fetch_one("SELECT submission, submission2 FROM records WHERE record_id = ?", [$record_id]);

    $old_submission = $record['submission'];
    $old_submission2 = $record['submission2'];

    $this->EditUpdateRecord(
      $record_id,
      $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6,
      $platform_id,
      $submission,
      $submission2,
      $comment,
      $extra1, $extra2, $extra3,
      $linked_proof,
      $game_patch
    );

    if ($old_submission != $submission || ($submission2 != NULL && $old_submission2 != $submission2)) {
      ChartRebuildClass::RebuildChart($chart_id);
      self::AddRecordHistoryEntry($record_id, 'e');
      return 'update';
    } else {
      return 'change';
    }
  }

    //---------------------------------------------------------------------------
    private static function AddRecord($chart, $user_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      return RecordsRepository::create([
        'user_id' => $user_id,
        'other_user' => $other_user,
        'game_id' => $chart['game_id'],
        'level_id' => $chart['level_id'],
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'entity_id_original' => 0,
        'report_reason' => '',
        'platform_id' => $platform_id,
        'submission' => $submission,
        'submission2' => $submission2,
        'original_date' => database_now(),
        'last_update' => database_now(),
        'rec_status' => 0,
        'proof_type' => '',
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'ranked' => $chart['ranked'],
        'game_patch' => $game_patch,
      ]);
    }

    //---------------------------------------------------------------------------
    private static function UpdateRecord($record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      RecordsRepository::update($record['record_id'], [
        'other_user' => $other_user,
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'submission' => $submission,
        'submission2' => $submission2,
        'last_update' => database_now(),
        'linked_proof' => '',
        'proof_type' => '',
        'rec_status' => 0,
        'game_patch' => $game_patch
      ]);
    }

    //---------------------------------------------------------------------------
    private static function ChangeRecord($record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      RecordsRepository::update($record['record_id'], [
        'other_user' => $other_user,
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'game_patch' => $game_patch,
      ]);
    }

    //---------------------------------------------------------------------------
    private function EditUpdateRecord($record_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $linked_proof, $game_patch) {
      RecordsRepository::update($record_id, [
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'submission' => $submission,
        'submission2' => $submission2,
        'linked_proof' => $linked_proof,
        'game_patch' => $game_patch,
      ]);
    }


    //----------------------------------------------------------------------------
    public static function MoveRecord($record_id, $new_chart_id, $mod_id) {
      $old_chart_id = database_value("SELECT level_id FROM records WHERE record_id = ?", [$record_id]);
      $new_game_id = database_value("SELECT game_id FROM levels WHERE level_id = ?", [$new_chart_id]);
      $ranked = database_value("SELECT ranked FROM levels WHERE level_id = ?", [$new_chart_id]);

      RecordsRepository::update($record_id, [
        'level_id' => $new_chart_id,
        'game_id' => $new_game_id,
        'ranked' => $ranked,
      ]);

      database_insert('staff_tasks', [
        'user_id' => $mod_id,
        'tasks_type' => 'record_moved',
        'task_id' => $record_id,
        'result' => 'Record moved',
      ]);

      self::AddRecordHistoryEntry($record_id, 'm');
      ChartRebuildClass::RebuildChart($old_chart_id);
      ChartRebuildClass::RebuildChart($new_chart_id);
    }

    //----------------------------------------------------------------------------
    public static function MoveDeletedRecord($record_id, $new_chart_id, $mod_id) {
      $new_game_id = database_value("SELECT game_id FROM levels WHERE level_id = ?", [$new_chart_id]);
      $ranked = database_value("SELECT ranked FROM levels WHERE level_id = ?", [$new_chart_id]);

      DeletedRecordsRepository::update($record_id, [
        'level_id' => $new_chart_id,
        'game_id' => $new_game_id,
        'ranked' => $ranked,
      ]);

      database_insert('staff_tasks', [
        'user_id' => $mod_id,
        'tasks_type' => 'record_moved',
        'task_id' => $record_id,
        'result' => 'Record moved',
      ]);
    }

    public static function HandleYGB($game_id, $chart_id, $record_id) {
      // We only send YGB notifications if:
      // - this record is the sole leader
      // - this record is a *new* leader (this is checked before calling this function, not here)

      $leader_user_id = database_value("SELECT user_id FROM records WHERE record_id = ?", [$record_id]);
      $chart_pos = database_value("SELECT chart_pos FROM records WHERE record_id = ?", [$record_id]);
      $sole_leader = database_value("SELECT COUNT(record_id) = 1 FROM records WHERE chart_pos = 1 AND level_id = ?", [$chart_id]);

      if ($chart_pos == 1 && $sole_leader) {
        // Get the user ids of the folks that were downgraded to second place
        $beaten_user_ids = array_map(
          fn($user) => $user['user_id'],
          database_fetch_all("SELECT user_id FROM records WHERE chart_pos = 2 AND level_id = ?", [$chart_id])
        );

        Notification::YGB($game_id, $chart_id, $leader_user_id)->DeliverToUsers($beaten_user_ids);
      }
    }

    //---------------------------------------------------------------------------
    private static function StoreCommentInHistory($record_id, $comment) {
        $history_id = database_value("SELECT MAX(history_id) FROM record_history WHERE record_id = ? AND update_type IN('f', 'u')", [$record_id]);

        RecordHistoriesRepository::update($history_id, ['comment' => $comment]);
    }

    //---------------------------------------------------------------------------
    public static function AddRecordHistoryEntry($record_id, $history_mode) {
      $record = RecordsRepository::get($record_id);

      return RecordHistoriesRepository::create([
        'record_id' => $record_id,
        'update_type' => $history_mode,
        'update_date' => database_now(),
        'submission' => $record['submission'],
        'submission2' => $record['submission2'],
        'chart_pos' => $record['chart_pos'],
        'game_id' => $record['game_id'],
        'user_id' => $record['user_id'],
      ]);
    }

    public static function ResetRecordStatus($record_id) {
      database_update_by('records', ['rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => ''], ['record_id' => $record_id]);
      database_update_by('staff_tasks', ['result' => 'Record status reset'], ['tasks_type' => 'record_investigated', 'task_id' => $record_id]);

      self::AddRecordHistoryEntry($record_id, 'r');
    }

    //---------------------------------------------------------------------------
    public static function TrackApproval($record_id, $mod_id) {
      database_insert('record_approvals', ['record_id' => $record_id, 'approval_date' => database_now(), 'user_id' => $mod_id]);
    }

    //---------------------------------------------------------------------------
    public static function ApproveRecord($record_id, $proof_type) {
      database_update_by('records', ['rec_status' => 3, 'proof_type' => $proof_type, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => null, 'proof_mod' => ''], ['record_id' => $record_id]);
    }

    public static function DeleteRecord($record_id, $user_id, $chart_id) {
      database_select("INSERT INTO records_del(SELECT * FROM records WHERE record_id = ?)", 's', [$record_id]);
      database_insert('staff_tasks', ['user_id' => $user_id, 'tasks_type' => 'record_deleted', 'task_id' => $record_id, 'result' => 'Record deleted']);
      database_update_by('staff_tasks', ['result' => 'Record deleted'], ['tasks_type' => 'record_investigated', 'task_id' => $record_id]);

      $deleted_count = database_value("SELECT COUNT(*) FROM records_del WHERE record_id = ?", [$record_id]);

      // We only really delete records if we managed to store a copy in records_del
      if ($deleted_count == 1) {
        // Who deleted it and when?
        database_insert('records_del_info', ['record_id' => $record_id, 'deleter_id' => $user_id, 'delete_date' => database_now()]);
        database_delete_by('records', ['record_id' => $record_id]);

        global $chart_rebuild;
        $chart_rebuild->RebuildChart($chart_id);
      }
    }

    public function ReinstateRecord($record_id, $mod_id, $user_id, $chart_id) {
      $existing_record = self::CheckForExistingRecord($chart_id, $user_id);

      if ($existing_record == false) {
        // Move the record from the deleted table to the live table
        database_select("INSERT INTO records(SELECT * FROM records_del WHERE record_id = ?)", 's', [$record_id]);

        // Reset the record status
        database_update_by('records', ['rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => '0000-00-00 00:00:00', 'proof_mod' => ''], ['record_id' => $record_id]);

        database_insert('staff_tasks', ['user_id' => $mod_id, 'tasks_type' => 'record_reinstated', 'task_id' => $record_id, 'result' => 'Record reinstated']);
        database_update_by('staff_tasks', ['result' => 'Record reinstated'], ['tasks_type' => 'record_deleted', 'task_id' => $record_id]);
        database_delete_by('records_del', ['record_id' => $record_id]);

        $comment = database_value("SELECT comment FROM records WHERE record_id = ?", [$record_id]);

        ChartRebuildClass::RebuildChart($chart_id);
        self::StoreCommentInHistory($record_id, $comment);
        self::AddRecordHistoryEntry($record_id, 's');

        return "reinstated";
      } else {
        return "failed";
      }
    }

    public static function TerminateRecord($record_id, $chart_id, $user_id) {
      database_insert('staff_tasks', [
        'user_id' => $user_id,
        'tasks_type' => 'record_terminated',
        'task_id' => $record_id,
        'result' => 'Record terminated',
      ]);

      database_delete_by('records', ['record_id' => $record_id]);
      database_delete_by('record_history', ['record_id' => $record_id]);

      ChartRebuildClass::RebuildChart($chart_id);
    }

    //---Reverts to a prior record value of choice---
    public static function RevertRecord($record_id, $history_id, $mod_id) {
      $reverted_submission = database_value("SELECT submission FROM record_history WHERE history_id = ?", [$history_id]);
      $reverted_submission2 = database_value("SELECT submission2 FROM record_history WHERE history_id = ?", [$history_id]);
      $reverted_submissions_date = database_value("SELECT update_date FROM record_history WHERE history_id = ?", [$history_id]);

      if ($reverted_submission2 == null) {
        database_update_by('records', ['submission' => $reverted_submission, 'last_update' => $reverted_submissions_date, 'rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => '', 'linked_proof' => ''], ['record_id' => $record_id]);
      } else {
        database_update_by('records', ['submission' => $reverted_submission, 'submission2' => $reverted_submission2, 'last_update' => $reverted_submissions_date, 'rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => '', 'linked_proof' => ''], ['record_id' => $record_id]);
      }

      database_insert('staff_tasks', ['user_id' => $mod_id, 'tasks_type' => 'record_reverted', 'task_id' => $record_id, 'result' => 'Record reverted']);

      $chart_id = database_value("SELECT level_id FROM records WHERE record_id = ?", [$record_id]);
      $game_id  = database_value("SELECT game_id  FROM records WHERE record_id = ?", [$record_id]);
      $comment  = database_value("SELECT comment  FROM records WHERE record_id = ?", [$record_id]);

      ChartRebuildClass::RebuildChart($chart_id);
      self::StoreCommentInHistory($record_id, $comment);
      self::AddRecordHistoryEntry($record_id, 'v');
    }

    public static function SortChartsAlphabetically($game_id, $group_id) {
      $charts = database_fetch_all("
        SELECT level_id FROM levels WHERE game_id = ? AND group_id = ? ORDER BY level_name ASC
      ", [$game_id, $group_id]);

      foreach ($charts as $i => $chart) {
        database_update_by('levels', ['level_pos' => $i + 1], ['level_id' => $chart['level_id']]);
      }
    }

  /** Game cache rebuilding functions.
   *
   * Below are a bunch of functions to recalculate gsb_cache_* tables. This
   * includes some helper methods to calculate things like medal points,
   * challenge points, etc.
   *
   * If a function name starts with Build, it will calculate the information
   * that should go into a cache table, without storing it. StoreCache saves
   * the data to the database. If it starts with Fetch, it will make a database
   * query. When creating functions that have complex formulas, it is
   * recommended to put them in a queryless function and create another
   * function called Fetch* that makes any queries required. This allows us to
   * mass compute these values, and potentially call them from other places in
   * the codebase without automatically taking the performance penalty of extra
   * queries.
   *
   * Ideally these should be mostly a roll-up of the chart caches, but the
   * records table, which contains the chart cache, does not calculate medal
   * points, speedrun points, nor challenge points. It calculates brain power
   * and tokens, but it does so indiscriminately, without looking at the
   * chart modifiers. This means that we can't just do a SUM() by game_id here.
   * The records table is also missing the bonus_csr.
   **/

  public static function StoreCache($cache, $game_id, $records) {
    database_delete_by("gsb_cache_$cache", ['game_id' => $game_id]);
    database_insert_all("gsb_cache_$cache", $records);
  }

  public static function FetchRecordsWithModifiers($game_id) {
    $records = database_filter_by('records', ['game_id' => $game_id]);

    // $levels information is not loaded in $records using a join on purpose
    // to reduce memory usage.
    $levels = index_by(database_fetch_all("
      SELECT chart_modifiers2.*
      FROM levels
      JOIN chart_modifiers2 USING (level_id)
      WHERE game_id = ?
    ", [$game_id]), 'level_id');

    foreach ($records as &$record) {
      $record['level'] = $levels[$record['level_id']];
    }
    unset($record);

    return $records;
  }

  public static function RebuildGameCaches($game_id) {
    $game = database_find_by('games', ['game_id' => $game_id]);
    $records = self::FetchRecordsWithModifiers($game_id);

    $caches = [];
    $caches['proof']       = self::BuildGameProofCache($game_id, $records);
    $caches['vproof']      = self::BuildGameVideoProofCache($game_id, $records);
    $caches['subs']        = self::BuildGameSubmissionsCache($game_id, $records);
    $caches['csp']         = self::BuildGameCSPCache($game, $records);
    $caches['standard']    = self::BuildGameMedalsCache($game, $records);
    $caches['arcade']      = self::BuildGameArcadeCache($game_id, $records);
    $caches['incremental'] = self::BuildGameIncrementalCache($game_id, $records);
    $caches['collectible'] = self::BuildGameCollectibleCache($game_id, $records);
    $caches['speedrun']    = self::BuildGameSpeedrunCache($game_id, $records);
    $caches['solution']    = self::BuildGameSolutionCache($game_id, $records);
    $caches['challenge']   = self::BuildGameUserChallengeCache($game, $records);

    $trophies = self::BuildGameTrophyCache($game, $caches);

    database_transaction(function() use ($game, $game_id, &$caches, &$trophies) {
      self::StoreCache("proof", $game_id, $caches['proof']);
      self::StoreCache("vproof", $game_id, $caches['vproof']);
      self::StoreCache("submissions", $game_id, $caches['subs']);
      self::StoreCache("csp", $game_id, $caches['csp']);
      self::StoreCache("medals", $game_id, $caches['standard']);
      self::StoreCache("arcade", $game_id, $caches['arcade']);
      self::StoreCache("incremental", $game_id, $caches['incremental']);
      self::StoreCache("collectible", $game_id, $caches['collectible']);
      self::StoreCache("speedrun", $game_id, $caches['speedrun']);
      self::StoreCache("solution", $game_id, $caches['solution']);
      self::StoreCache("userchallenge", $game_id, $caches['challenge']);

      self::StoreCache("trophy", $game_id, $trophies);
    });
  }

  public static function BuildGameProofCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));

    $users = array_map(function($rs) {
      $approved = array_filter($rs, fn($r) => self::IsApproved($r));
      $counts = [
        'user_id' => $rs[0]['user_id'],
        'num_subs' => count($rs),
        'total'       => count($approved),
        'standard' => 0,
        'speedrun' => 0,
        'solution' => 0,
        'unranked' => 0,
        'challenge' => 0,
        'incremental' => 0,
        'collectible' => 0,
        'arcade' => 0,
      ];
      foreach ($approved as $r) {
        $level = $r['level'];
        if ($level['standard']) $counts['standard'] += 1;
        if ($level['speedrun']) $counts['speedrun'] += 1;
        if ($level['solution']) $counts['solution'] += 1;
        if ($level['unranked']) $counts['unranked'] += 1;
        if ($level['challenge']) $counts['challenge'] += 1;
        if ($level['collectible']) $counts['collectible'] += 1;
        if ($level['incremental']) $counts['incremental'] += 1;
        if ($level['arcade']) $counts['arcade'] += 1;
      }

      return $counts;
    }, $users);

    usort($users, function($a, $b) {
      return [-$a['total'], $a['user_id']] <=> [-$b['total'], $b['user_id']];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);
      $percentage = $top_score == 0 ? 0.0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => round($percentage, 6),
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameVideoProofCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      // TODO: optimize all these array_counts into a single loop. It's uglier but faster.
      'total'       => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
      'standard'    => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['standard']),
      'speedrun'    => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['speedrun']),
      'solution'    => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['solution']),
      'unranked'    => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['unranked']),
      'challenge'   => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['challenge']),
      'collectible' => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['collectible']),
      'incremental' => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['incremental']),
      'arcade'      => array_count($rs, fn($r) => self::IsApprovedVideo($r) && $r['level']['arcade']),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total'] > $b['total']) return -1;
      if ($a['total'] < $b['total']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);

      $percentage = $top_score == 0 ? 0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => $percentage,
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSubmissionsCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total'    => count($rs),
      'standard'    => array_count($rs, fn($r) => $r['level']['standard']),
      'speedrun'    => array_count($rs, fn($r) => $r['level']['speedrun']),
      'solution'    => array_count($rs, fn($r) => $r['level']['solution']),
      'unranked'    => array_count($rs, fn($r) => $r['level']['unranked']),
      'challenge'   => array_count($rs, fn($r) => $r['level']['challenge']),
      'collectible' => array_count($rs, fn($r) => $r['level']['collectible']),
      'incremental' => array_count($rs, fn($r) => $r['level']['incremental']),
      'arcade'      => array_count($rs, fn($r) => $r['level']['arcade']),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total'] > $b['total']) return -1;
      if ($a['total'] < $b['total']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);

      $percentage = $top_score == 0 ? 0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => round($percentage, 6),
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameCSPCache($game, $submissions) {
    $users = array_filter($submissions, fn($r) => !$r['level']['unranked']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total_csp' => round(array_sum(array_map(fn($r) => $r['csp'], $rs)), 3),
      'total_csr' => array_sum(array_map(fn($r) => pow($r['csp'] / 100, 4), $rs)),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total_csp'] > $b['total_csp']) return -1;
      if ($a['total_csp'] < $b['total_csp']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    if (empty($users)) {
      return [];
    }

    $num_subs = array_sum(pluck($users, 'num_subs'));
    $num_players = count($users);
    $num_charts = $game['csp_charts'];
    $rpc = $num_subs / $num_charts;
    $first = 1;
    $rank = new Rank();
    $records = [];
    $first_place_count = 0;

    foreach ($users as &$user) {
      $user['scoreboard_pos'] = $rank->update($user['total_csp']);

      if ($first == 1) {
        $top_score = $user['total_csp'];
        $first = 0;
      }

      if ($top_score == $user['total_csp']) {
        $first_place_count++;
      }
    }
    unset($user);

    $platinum = Trophies::ValidatePlatinumTrophy($first_place_count, $rpc, 'csp');
    foreach ($users as $user) {
      // This series of declarations builds data to fetch trophy information for this scoreboard.
      $user['platinum']      = $platinum && $user['scoreboard_pos'] == 1 ? 1 : 0;
      $user['gold']          = $user['scoreboard_pos'] == 1 ? 1 : 0;
      $user['trophy_points'] = Trophies::TrophyPoints($platinum, $user, $num_players, $rpc, $first_place_count, 0.5);

      $percentage = $top_score == 0 ? 0 : $user['total_csp'] / $top_score;

      [$pos_factor, $sub_factor, $proof_factor, $video_proof_factor] = self::CalculateBonusCSR($rpc, $user['scoreboard_pos'], $user['num_subs'], $num_charts, $user['num_approved'], $user['num_approved_v']);
      $user['bonus_csr'] = $user['total_csr'] * $pos_factor * ($sub_factor + $proof_factor + $video_proof_factor);

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $user['scoreboard_pos'],
        'percentage' => round($percentage, 6),
        'rpc' => $rpc,
        'bonus_csr' => $user['bonus_csr'],
        'total_csr' => $user['total_csr'],
        'total_csp' => $user['total_csp'],
        'trophy_points' => $user['trophy_points'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function CalculateBonusCSR($rpc, $scoreboard_position, $user_num_subs, $num_charts, $num_proofs, $num_video_proofs) {
    $pos_factor = 0.1 * (log(4 + $rpc, 2) / sqrt($scoreboard_position));
    $sub_factor = ($user_num_subs / $num_charts);
    $proof_factor = ($num_proofs / $user_num_subs / 3);
    $video_proof_factor = ($num_video_proofs / $user_num_subs / 6);

    return [$pos_factor, $sub_factor, $proof_factor, $video_proof_factor];
  }

  public static function BuildGameMedalsCache($game, $submissions) {
    $cmp = function($user) {
      return [
        $user['platinum'],
        $user['gold'],
        $user['silver'],
        $user['bronze'],
      ];
    };

    $users = array_filter($submissions, fn($r) => $r['level']['standard']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'platinum' => array_count($rs, fn($r) => $r['platinum']),
      'gold' => array_count($rs, fn($r) => $r['chart_pos'] == 1),
      'silver' => array_count($rs, fn($r) => $r['chart_pos'] == 2),
      'bronze' => array_count($rs, fn($r) => $r['chart_pos'] == 3),
      'medal_points' => array_sum(array_map(fn($r) => pow($r['csp'] / 100, 4), $rs)),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) use ($cmp) { return $cmp($b) <=> $cmp($a); });

    if (empty($users)) {
      return [];
    }

    $maximum = new Maximum();
    foreach ($users as $user) {
      $maximum->update($user['medal_points']);
    }

    $rpc = $game['standard_subs'] / $game['standard_charts'];
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      $scoreboard_position = $rank->update($cmp($user));

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $scoreboard_position,
        'medal_points' => round($user['medal_points'], 6),
        'percentage' => round($maximum->percentage($user['medal_points']), 6),
        'platinum' => $user['platinum'],
        'gold' => $user['gold'],
        'silver' => $user['silver'],
        'bronze' => $user['bronze'],
        'max' => $maximum->max,
        'rpc' => $rpc,
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSpeedrunCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['speedrun']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'medal_points' => array_sum(pluck($rs, 'medal_points')),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['medal_points'] > $b['medal_points']) return -1;
      if ($a['medal_points'] < $b['medal_points']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['medal_points'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['medal_points']);

      $percentage = $top_score == 0 ? 0 : $user['medal_points'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'medal_points' => round($user['medal_points'], 1),
        'percentage' => round($percentage, 6),
        'platinum' => 0,
        'gold' => 0,
        'silver' => 0,
        'bronze' => 0,
        'max' => 0.0,
        'rpc' => 0.0,
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameArcadeCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['arcade']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'tokens' => array_sum(pluck($rs, 'arcade_points')),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['tokens'] > $b['tokens']) return -1;
      if ($a['tokens'] < $b['tokens']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['tokens'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['tokens']);

      $percentage = $top_score == 0 ? 0 : $user['tokens'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'tokens' => round($user['tokens'], 6),
        'percentage' => round($percentage, 6),
        'num_subs' => $user['num_subs'],
        'num_approved' => intval($user['num_approved']),
        'num_approved_v' => intval($user['num_approved_v']),
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameIncrementalCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['incremental']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'cxp' => array_sum(pluck($rs, 'cxp')),
      'vs_cxp' => array_sum(pluck($rs, 'vs_cxp')),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['cxp'] > $b['cxp']) return -1;
      if ($a['cxp'] < $b['cxp']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
            $top_score = $user['cxp'];
            $first = 0;
        }

        $scoreboard_position = $rank->update($user['cxp']);

        $percentage = $top_score == 0 ? 0 : $user['cxp'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'cxp' => intval(round($user['cxp'])),
          'vs_cxp' => intval($user['vs_cxp']),
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameCollectibleCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['collectible']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total_stars' => array_sum(array_map(fn($r) => $r['cyberstars'] + self::IsApproved($r) + self::IsApprovedVideo($r), $rs)),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total_stars'] > $b['total_stars']) return -1;
      if ($a['total_stars'] < $b['total_stars']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
          $top_score = $user['total_stars'];
          $first = 0;
        }

        $scoreboard_position = $rank->update($user['total_stars']);

        $percentage = $top_score == 0 ? 0 : $user['total_stars'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'cyberstars' => intval(round($user['total_stars'])),
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSolutionCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['solution']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'brain_power' => array_sum(pluck($rs, 'brain_power')),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['brain_power'] > $b['brain_power']) return -1;
      if ($a['brain_power'] < $b['brain_power']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
            $top_score = $user['brain_power'];
            $first = 0;
        }

        $scoreboard_position = $rank->update($user['brain_power']);

        $percentage = $top_score == 0 ? 0 : $user['brain_power'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'brain_power' => round($user['brain_power'], 6),
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameTrophyCache($game, $scoreboards) {
    $game_id = $game['game_id'];

    $boards = ['csp', 'standard', 'speedrun', 'challenge', 'solution', 'arcade', 'incremental', 'collectible'];

    // This is not the same as csp_charts or all_charts.
    // Some charts belong to more than one chart flag, so they should be counted twice.
    $total_charts = 0;
    foreach ($boards as $board) {
      if ($board != 'csp') {
        $total_charts += $game["{$board}_charts"];
      }
    }

    $users = [];
    $trophies = [];
    foreach ($boards as $board) {
      $trophies[$board] = [];
      if ($game["{$board}_charts"] > 0) {
        $num_players = count($scoreboards[$board]);
        $first_place_count = array_count($scoreboards[$board], fn($u) => $u['scoreboard_pos'] == 1);
        $rpc = $game["{$board}_subs"] / $game["{$board}_charts"];
        $platinum = Trophies::ValidatePlatinumTrophy($first_place_count, $rpc, $board);

        foreach ($scoreboards[$board] as $user) {
          switch ($user['scoreboard_pos']) {
            case 1: $trophy = $platinum ? 'platinum' : 'gold'; break;
            case 2: $trophy = 'silver'; break;
            case 3: $trophy = 'bronze'; break;
            case 4: $trophy = 'fourth'; break;
            case 5: $trophy = 'fifth'; break;
            default: $trophy = $user['scoreboard_pos']; break;
          }

          $users []= $user['user_id'];

          // If this board is the medal table and the user has no medal score, omit them.
          if ($board == 'standard' &&
             $user['platinum'] == 0 &&
             $user['gold'] == 0 &&
             $user['silver'] == 0 &&
             $user['bronze'] == 0) {
            $trophies[$board][$user['user_id']] = [
              'trophy' => "N/A",
              'points' => 0,
            ];
          } else {
            $trophies[$board][$user['user_id']] = [
              'trophy' => $trophy,
              'points' => Trophies::TrophyPoints($platinum, $user, $num_players, $rpc, $first_place_count, max(0.01, $board == 'csp' ? 0.5 : ($game["{$board}_charts"] / $total_charts * 0.5))),
            ];
          }
        }
      }
    }

    $users = array_unique($users);

    $records = [];
    foreach ($users as $user_id) {
      $record = [
        'game_id' => $game_id,
        'user_id' => $user_id,
        'platinum' => 0,
        'gold' => 0,
        'silver' => 0,
        'bronze' => 0,
        'fourth' => 0,
        'fifth' => 0,
        'trophy_points' => 0,
      ];

      foreach ($boards as $board) {
        if (array_key_exists($user_id, $trophies[$board])) {
          $record["{$board}_trophy"]        = $trophies[$board][$user_id]['trophy'];
          $record["{$board}_trophy_points"] = $trophies[$board][$user_id]['points'];

          $record['trophy_points'] += $trophies[$board][$user_id]['points'];
          if ($trophies[$board][$user_id]['trophy'] != NULL && $trophies[$board][$user_id]['trophy'] != "N/A" && !is_numeric($trophies[$board][$user_id]['trophy'])) {
            $record[$trophies[$board][$user_id]['trophy']] += 1;
            if ($trophies[$board][$user_id]['trophy'] == 'platinum') {
              $record['gold'] += 1;
            }
          }
        } else if(count($trophies[$board]) > 0) {
          $record["{$board}_trophy"]        = "N/A";
          $record["{$board}_trophy_points"] = 0.0;
        } else {
          $record["{$board}_trophy"]        = NULL;
          $record["{$board}_trophy_points"] = 0.0;
        }
      }

      $records []= $record;
    }

    usort($records, fn($a, $b) => -$a['trophy_points'] <=> -$b['trophy_points']);
    $rank = new Rank();
    foreach ($records as &$record) {
      $scoreboard_position = $rank->update($record['trophy_points']);
      $record['scoreboard_pos'] = $scoreboard_position;
    }
    unset($record);

    return $records;
  }

  public static function BuildGameUserChallengeCache($game, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['challenge']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'style_points' => array_sum(pluck($rs, 'style_points')),
      'num_approved' => array_count($rs, fn($r) => self::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => self::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['style_points'] > $b['style_points']) return -1;
      if ($a['style_points'] < $b['style_points']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    if (empty($users)) {
      return [];
    }

    $first = 1;
    $rank = new Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['style_points'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['style_points']);

      $percentage = $top_score == 0 ? 0 : $user['style_points'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $scoreboard_position,
        'style_points' => $user['style_points'],
        'percentage' => round($percentage, 6),
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function RebuildGameBoard($game_id) {
    // Rebuild game information (chart counts, submission counts etc.)
    Modifiers::UpdateChartFlooder($game_id);
    GameRebuildClass::BuildGameNumCharts($game_id);
    self::RebuildGameCaches($game_id);
    GameRebuildClass::BuildGameNumSubs($game_id);
  }

  // This function is the same as RebuildGameBoard but limited to rebuilding
  // the incremental caches. It is used in `bin/rebuild` to recalculate the
  // vs_cxp after someone levels up.
  public static function RebuildGameVersusExperience($game_id) {
    $records = self::FetchRecordsWithModifiers($game_id);
    self::BuildGameIncrementalCache($game_id, $records);
  }
}
