<?php

function recaptcha_verify() {
  global $config;

  $key = $config['recaptcha']['key'];

  if (empty($key)) {
    log_event('captcha/skip');
    return true;
  }

  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $response = $_POST['g-recaptcha-response'];
  $remote_ip = get_ip();

  $data = json_decode(file_get_contents("$url?secret=$key&response=$response&remoteip=$remote_ip"), true);
  $success = ($data['success'] ?? NULL) == true;

  if ($success) {
    log_event('captcha/success', ['g-response' => $response, 'response' => $data]);
  } else {
    log_event('captcha/fail', ['g-response' => $response, 'response' => $data]);
  }

  return $success;
}
