<?php

class Trophies {
  public static function TrackChanges($game_id, $updated_scoreboard, $type, $limit = 10) {
    // First, handle the enum and receive the correct information for the type.
    $enum = Trophies::TypeHandling($type);
    $changes = [];

    // We need to fetch the existing top 10 to compare to
    $existing_scoreboard = database_fetch_one("
      SELECT user_id, scoreboard_pos
      FROM {$enum['cache']}
      WHERE game_id = ?
      ORDER BY scoreboard_pos DESC, user_id ASC
      LIMIT ?
    ", [$game_id, $limit]);

    // Compare the old scoreboard to the new scoreboard!
    for ($i = 1; $i <= count($existing_scoreboard); $i++) {
      if ($existing_scoreboard[$i]['user_id'] != $updated_scoreboard[$i]['user_id'] &&
        $existing_scoreboard[$i]['scoreboard_pos'] == $updated_scoreboard[$i]['scoreboard_pos']) {

        // Record a scoreboard change to insert in the game_scoreboard_changes database table
        $changes []= [
          'game_id' => $game_id,
          'type' => $type,
          'user_id' => $updated_scoreboard[$i]['user_id'],
          'scoreboard_pos' => $i
        ];

        // Send a sad notification to the person who just lost their position
        Notification::TrophyLost($game_id, $i, $updated_scoreboard[$i]['user_id'], $type)->DeliverToUser($existing_scoreboard[$i]['user_id']);
        // Send a happy notification to the person who just gained a position
        Notification::TrophyGained($game_id, $i, $type)->DeliverToUser($updated_scoreboard[$i]['user_id']);
      }
    }

    // Input all tracked changes to the database table
    database_insert_all('game_scoreboard_changes', $changes);
  }

  //Takes a scoreboard type and returns the correct relevant information
  public static function TypeHandling($type) {
    $enum = [];

    switch($type) {
    case "csp":
      $enum['cache'] = "gsb_cache_csp";
      $enum['text'] = t('game_csp_scoreboard');
      break;
    case "medal":
      $enum['cache'] = "gsb_cache_medals";
      $enum['text'] = t('general_medal_table');
      break;
    case "arcade":
      $enum['cache'] = "gsb_cache_arcade";
      $enum['text'] = t('game_arcadeboard_leader');
      break;
    case "speedrun":
      $enum['cache'] = "gsb_cache_speedrun";
      $enum['text'] = t('general_speedrun_awards');
      break;
    case "solution":
      $enum['cache'] = "gsb_cache_solution";
      $enum['text'] = t('general_solution_hub');
      break;
    case "collectible":
      $enum['cache'] = "gsb_cache_collectible";
      $enum['text'] = t('general_collectors_cache');
      break;
    case "incremental":
      $enum['cache'] = "gsb_cache_incremental";
      $enum['text'] = t('general_incremental');
      break;
    case "challenge":
      $enum['cache'] = "gsb_cache_userchallenge";
      $enum['text'] = t('general_challenge_table');
      break;
    case "trophy":
    }

    return $enum;
  }

  public static function TrophyPoints($platinum, $user, $num_players, $rpc, $first_ties, $scoreboard_multiplier = 0.5) {
    $scoreboard_pos = $user['scoreboard_pos'];

    // Start at 0. Many numbers are prepared so that they can be fine-tuned for balance.
    $trophy_points = 0;

    // Trophy points are distributed from top-to-bottom via a percentile ranking.
    // We have a softcap on the number of players that are included in this calculation. This value is an integer threshold.
    // After this softcap, the impact of additional players on the distribution is reduced. This value is a power.
    $player_softcap = 100;
    $player_softcap_root = 0.5;

    // Prepare the modifier player count. Or don't. I'm not your mum.
    if ($num_players > $player_softcap) {
      $adjusted_player_count = $player_softcap + pow(($num_players-$player_softcap), $player_softcap_root);
    } else {
      $adjusted_player_count = $num_players;
    }

    // We need to work out where in the percentile ranking this user is. 
    // The adjusted player count can make this value dip into the negatives. 
    // However, if it's below 0, we just don't need to bother at all. Send em' to the gulag.
    $percentile_ranking = 1 - (($scoreboard_pos - 1) / $adjusted_player_count);
    if ($percentile_ranking <= 0) {
      return 0;
    }

    //Number go up = more top-heavy rewards. This value is an exponent.
    $scaling_exponent = 7;
    //First-place trophies get a bonus. Platinum trophies get a higher reward! This value is a multiplier.
    $first_place_reward = 1.0625;
    if ($platinum && $scoreboard_pos == 1) { $first_place_reward = 1.125; }
    // Number go up = higher trophy points all around (maintaining the same ratios). This value is a multiplier.
    $base_multiplier = 200;
    // Number approach 1 = bigger games worth more. This value is a power.
    $bonus_root = 0.5;
    // The bigger the number is, the more relatively valuable RPC is compared to player count. This value is a divisor.
    $player_divisor = 4;

    // Bonus wrangling - start with the base.
    $bonus = ($rpc + $num_players / $player_divisor);

    if ($first_ties != 1) {
        // Proportionally reduce the bonus if first place is tied.
        $bonus -= 2 * $first_ties;

        // Ensure the bonus doesn't bottom out too badly or become negative.
        $bonus = MAX($bonus, (1.25 * ($num_players - $first_ties + 1)));
    }

    //Apply the root to the bonus
    $bonus = pow($bonus, $bonus_root);

    // Gold trophies get a first-place reward multiplier. This number is higher for a platinum trophy!
    if ($scoreboard_pos == 1) { $bonus *= $first_place_reward; }

    // Trophy points are scaling based on percentile ranking; which is brought to a power to make the formula more top-heavy. 
    // We also multiply by a constant to have a nice size of number.
    $trophy_points += $bonus * pow($percentile_ranking, $scaling_exponent) * $base_multiplier;

    // Round to the nearest integer (Trophy points are integers)
    return floor($trophy_points * $scoreboard_multiplier);
}

public static function TrophyPointsFraction($secondary_scoreboards_list) {
    return 0.5 / count($secondary_scoreboards_list);
}

public static function ValidatePlatinumTrophy($first_place_count, $rpc, $type = 'csp') {
    // At the beginning, all things are created equal
    $platinum = TRUE;

    if ($type != 'csp') {
      // Only the game scoreboard may be platinum as it is superior in all ways.
      $platinum = FALSE;
    } else if ($first_place_count > 1) {
      // If there is more than one first place, no platinum is awarded.
      $platinum = FALSE;
    } else if ($rpc < 5) {
      // No platinum if the RPC is too low
      $platinum = FALSE;
    }

    return $platinum;
  }
}
