<?php

function load_config($ini) {
  $config = parse_ini_file($ini, true);

  // Defaults
  $config['app'] ??= [];
  $config['app']['root'] = realpath(__DIR__ . '/..');
  $config['app']['base_url'] ??= "https://cyberscore.me.uk";
  $config['app']['verbose'] ??= false;
  $config['app']['debug'] ??= false;

  // Side effects
  if ($config['app']['debug']) {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
  }

  return $config;
}

function config_domain() {
  global $config;

  return parse_url($config['app']['base_url'])['host'];
}
