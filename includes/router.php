<?php

global $route_url_prefix;
global $route_page_prefix;
global $route_middlewares;
global $routes;

$route_url_prefix = "";
$route_page_prefix = "";
$route_middlewares = [[]];
$routes = [
  'GET' => [],
  'POST' => [],
];

function serve($method, $path, $file, $params = []) {
  global $route_url_prefix;
  global $route_page_prefix;

  global $route_middlewares;
  global $routes;

  if ($file == NULL) {
    $file = $path;
  }
  if (is_string($file)) {
    $file = ltrim($file, "/");
    $file = "$route_page_prefix$file";
  }

  $path = "/" . ltrim($path, "/");
  $path = "$route_url_prefix$path";

  $regexp = preg_replace(
    '/\\\{([^\/]*)\\\}/',
    '(?P<\1>[^\/]*?)',
    preg_quote($path, '/')
  );

  $routes[$method] []= [$regexp, $file, $params, $route_middlewares[0]];
}

function get($path, $file = NULL, $params = []) {
  serve('GET', $path, $file, $params);
}

function post($path, $file = NULL) {
  serve('POST', $path, $file);
}

class Route {
  static function get($path, $file = null, $params = []) {
    get($path, $file, $params);
  }
  static function post($path, $file = null, $params = []) {
    post($path, $file, $params);
  }
  static function redirect($path, $target) {
    serve('GET', $path, new RouteRedirect($target));
  }
}

class RouteRedirect {
  private $target = null;

  public function __construct($target) {
    $this->target = $target;
  }

  public function target($params) {
    return preg_replace_callback(
      '/\{([^\/]*)\}/',
      function($m) use ($params) { return $params[$m[1]]; },
      $this->target
    );
  }
}

function resources($resource, $options = []) {
  global $route_url_prefix;
  global $route_page_prefix;

  foreach ($options['only'] ?? ['index', 'show', 'new', 'edit', 'create', 'update', 'delete'] as $action) {
    switch ($action) {
    case 'index':
      get("$resource", "$resource/index");
      get("$resource/index", "$resource/index");
      break;
    case 'show':
      get("$resource/{id}", "$resource/show");
      break;
    case 'new':
      get("$resource/new", "$resource/new");
      break;
    case 'edit':
      get("$resource/{id}/edit", "$resource/edit");
      break;
    case 'create':
      post("$resource", "$resource/create");
      break;
    case 'update':
      post("$resource/{id}", "$resource/update");
      break;
    case 'delete':
      post("$resource/{id}/delete", "$resource/delete");
      break;
    }
  }
}

function scope($prefix, $definitions) {
  global $route_url_prefix;
  global $route_page_prefix;
  global $route_middlewares;

  $route_url_prefix = "/$prefix";
  $route_page_prefix = "$prefix/";
  array_unshift($route_middlewares, $route_middlewares[0]);
  $definitions();
  array_shift($route_middlewares);
  $route_url_prefix = "";
  $route_page_prefix = "";
}

function before($middleware) {
  global $route_middlewares;

  $route_middlewares[0] []= $middleware;
}

function route_match($method, $uri) {
  global $routes;

  $url = parse_url($uri);

  foreach ($routes[strtoupper($method)] as $match) {
    [$regexp, $file, $params, $middlewares] = $match;

    if (preg_match("/^$regexp(\.(?P<format>.+))?$/", $url['path'], $matches)) {
      $matches = array_filter(
        $matches,
        function ($k) { return is_string($k); },
        ARRAY_FILTER_USE_KEY
      );

      if (is_string($file)) {
        return [
          __DIR__ . "/../src/pages/$file.php",
          array_merge($matches, $params),
          $middlewares,
        ];
      } else {
        return [
          $file,
          array_merge($matches, $params),
          $middlewares,
        ];
      }
    }
  }

  return NULL;
}

function extract_request_params($params) {
  $_GET = array_merge($_GET, $params);
  if (($_SERVER["CONTENT_TYPE"] ?? "") == "application/json") {
    $_POST = json_decode(file_get_contents('php://input'), true);
  }
}
