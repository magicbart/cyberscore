<?php

class Comment {
  static $tables = [
    'news' => 'news_comments',
    'game_requests' => 'request_comments',
    'userpage' => 'user_comments',
  ];

  static $pkey = [
    'news' => 'news_comments_id',
    'game_requests' => 'comment_id',
    'userpage' => 'comment_id',
  ];

  static $commentable_id = [
    'news' => 'news_id',
    'game_requests' => 'gamereq_id',
    'userpage' => 'userpage_id',
  ];

  static $commented_at = [
    'news' => 'comment_date',
    'game_requests' => 'comment_time',
    'userpage' => 'comment_time',
  ];

  public static function get($type, $id) {
    return static::find_by($type, ['comment_id' => $id]);
  }

  public static function internal_params($type, $params) {
    if (array_key_exists('comment_id', $params)) {
      $comment_id = $params['comment_id'];
      unset($params['comment_id']);
      $params[static::$pkey[$type]] = $comment_id;
    }

    if (array_key_exists('commentable_id', $params)) {
      $commentable_id = $params['commentable_id'];
      unset($params['commentable_id']);
      $params[static::$commentable_id[$type]] = $commentable_id;
    }

    if (array_key_exists('comment_date', $params)) {
      $commented_at = $params['comment_date'];
      unset($params['comment_date']);
      $params[static::$commented_at[$type]] = $commented_at;
    }


    return $params;
  }

  public static function find_by($type, $params) {
    $params = static::internal_params($type, $params);

    $comment = database_find_by(static::$tables[$type], $params);

    /*
     * Add normalized attributes so that we can treat comments the same,
     * independently of where they came from.
     *
     * - commentable_type is the type of the object being commented on
     * - commentable_id is the primary key of the object being commented on
     * - comment_id is the primary key of the comment
     *
     * Since comments are spread through many tables, comments on objects of
     * different types may have the same comment_id. The pair
     * (commentable_type, comment_id) should be unique, though.
     */
    if ($comment) {
      $comment['commentable_type'] = $type;
      $comment['commentable_id'] = $comment[static::$commentable_id[$type]];
      $comment['comment_id'] = $comment[static::$pkey[$type]];
    }

    return $comment;
  }

  public static function update($type, $id, $params) {
    $params = static::internal_params($type, $params);

    database_update_by(static::$tables[$type], $params, [static::$pkey[$type] => $id]);
  }

  public static function create($type, $params) {
    $params = static::internal_params($type, $params);

    return database_insert(static::$tables[$type], $params);
  }

  public static function delete($type, $id) {
    return database_delete_by(static::$tables[$type], [static::$pkey[$type] => $id]);
  }
}

class Commentable {
  static $tables = [
    'news' => 'news',
    'game_requests' => 'game_requests',
    'userpage' => 'users',
  ];

  static $pkey = [
    'news' => 'news_id',
    'game_requests' => 'gamereq_id',
    'userpage' => 'user_id',
  ];

  public static function get($type, $id) {
    $commentable = database_find_by(static::$tables[$type], [static::$pkey[$type] => $id]);

    $commentable['commentable_type'] = $type;
    $commentable['commentable_id'] = $commentable[static::$pkey[$type]];

    return $commentable;
  }
}
