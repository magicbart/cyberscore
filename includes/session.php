<?php

class Session {
  public static function Store($key, $value) {
    if (session_status() == PHP_SESSION_NONE) {
      self::Start();
    }

    $_SESSION[$key] = $value;
  }

  public static function Fetch($key) {
    if (session_status() == PHP_SESSION_NONE && isset($_COOKIE[session_name()])) {
      self::Start();
    }

    return $_SESSION[$key] ?? null;
  }

  public static function Clear($key) {
    if (session_status() == PHP_SESSION_NONE && isset($_COOKIE[session_name()])) {
      self::Start();
    }

    unset($_SESSION[$key]);
  }

  private static function Start() {
    session_set_cookie_params([
      'samesite' => 'lax',
      'secure'   => true,
      'httponly' => true,
      'domain'   => config_domain(),
      'path'     => '/',
    ]);

    session_start();
  }
}
