SET SESSION sql_mode = replace(@@sql_mode, "STRICT_TRANS_TABLES,", "");
ALTER TABLE users MODIFY date_joined DATETIME NULL DEFAULT NULL;
ALTER TABLE users MODIFY date_lastseen DATETIME NULL DEFAULT NULL;
ALTER TABLE users MODIFY dob DATE NULL DEFAULT NULL;
UPDATE users SET dob = NULL WHERE dob = '0000-00-00 00:00:00';

ALTER TABLE users ADD COLUMN email_confirmation_required BOOLEAN DEFAULT FALSE;
ALTER TABLE users ADD COLUMN registration_token VARCHAR(128);
ALTER TABLE users ADD COLUMN registration_token_expiration BIGINT UNSIGNED NOT NULL DEFAULT 0;
CREATE INDEX users_registration_token ON users(registration_token);
