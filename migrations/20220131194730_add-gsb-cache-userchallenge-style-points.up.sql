ALTER TABLE gsb_cache_userchallenge ADD COLUMN style_points DOUBLE AFTER challenge_points;
ALTER TABLE gsb_cache_userchallenge DROP COLUMN challenge_points;
