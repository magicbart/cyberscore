CREATE TABLE proof_rules (
    rule_id smallint NOT NULL AUTO_INCREMENT,
    rule_text text,
    PRIMARY KEY (rule_id)
);
