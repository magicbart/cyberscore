ALTER TABLE gsb_cache_incremental ADD COLUMN percentage DOUBLE AFTER vs_cxp;
ALTER TABLE gsb_cache_speedrun ADD COLUMN percentage DOUBLE AFTER bronze;
