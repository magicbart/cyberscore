SET SESSION sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
ALTER TABLE records MODIFY original_date DATETIME NULL DEFAULT NULL;
UPDATE records SET original_date = NULL WHERE original_date = '0000-00-00 00:00:00';
