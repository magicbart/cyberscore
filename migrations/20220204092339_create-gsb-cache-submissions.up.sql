CREATE TABLE gsb_cache_proof(
  cache_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  user_id        MEDIUMINT(8) UNSIGNED NOT NULL,
  game_id        SMALLINT(5) UNSIGNED NOT NULL,
  split_index    INT(10) UNSIGNED NOT NULL,
  dlc_index      TINYINT(4) NOT NULL,

  scoreboard_pos INT NOT NULL,
  total          INT NOT NULL,
  percentage     DOUBLE NOT NULL,
  num_subs       INT UNSIGNED NOT NULL,

  medal       INT NOT NULL,
  speedrun    INT NOT NULL,
  solution    INT NOT NULL,
  unranked    INT NOT NULL,
  challenge   INT NOT NULL,
  collectible INT NOT NULL,
  incremental INT NOT NULL,
  arcade      INT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (game_id) REFERENCES games(game_id) ON DELETE RESTRICT
);

CREATE TABLE gsb_cache_vproof(
  cache_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  user_id        MEDIUMINT(8) UNSIGNED NOT NULL,
  game_id        SMALLINT(5) UNSIGNED NOT NULL,
  split_index    INT(10) UNSIGNED NOT NULL,
  dlc_index      TINYINT(4) NOT NULL,

  scoreboard_pos INT NOT NULL,
  total          INT NOT NULL,
  percentage     DOUBLE NOT NULL,
  num_subs       INT UNSIGNED NOT NULL,

  medal       INT NOT NULL,
  speedrun    INT NOT NULL,
  solution    INT NOT NULL,
  unranked    INT NOT NULL,
  challenge   INT NOT NULL,
  collectible INT NOT NULL,
  incremental INT NOT NULL,
  arcade      INT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (game_id) REFERENCES games(game_id) ON DELETE RESTRICT
);

CREATE TABLE gsb_cache_submissions(
  cache_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  user_id        MEDIUMINT(8) UNSIGNED NOT NULL,
  game_id        SMALLINT(5) UNSIGNED NOT NULL,
  split_index    INT(10) UNSIGNED NOT NULL,
  dlc_index      TINYINT(4) NOT NULL,

  scoreboard_pos INT NOT NULL,
  total          INT NOT NULL,
  percentage     DOUBLE NOT NULL,
  num_subs       INT UNSIGNED NOT NULL,

  medal       INT NOT NULL,
  speedrun    INT NOT NULL,
  solution    INT NOT NULL,
  unranked    INT NOT NULL,
  challenge   INT NOT NULL,
  collectible INT NOT NULL,
  incremental INT NOT NULL,
  arcade      INT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (game_id) REFERENCES games(game_id) ON DELETE RESTRICT
);
