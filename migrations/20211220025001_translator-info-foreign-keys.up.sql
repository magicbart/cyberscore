ALTER TABLE translator_info ADD CONSTRAINT FOREIGN KEY(user_id) REFERENCES user(user_id);
ALTER TABLE translator_info ADD CONSTRAINT FOREIGN KEY(language_code) REFERENCES languages(language_code);
ALTER TABLE translator_info ENGINE=InnoDB;
