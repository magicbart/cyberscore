<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/home/index', function () {
  global $_SESSION;
  $_SESSION = [];

  ob_start();
  require("src/pages/home/index.php");

  $contents = ob_get_contents();
  ob_end_clean();

  $crawler = new Crawler($contents);

  expect($crawler->filter('main > .index-dashboard .scoreboard-widget')->count())->toBe(15);
});
