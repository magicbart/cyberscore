export function registerScores(result, addScore, game) {
  if (result.photo) {
    const dex = result.photo.dex.toString().padStart(3, "0");

    const charts = charts_in_groups(game, `${result.photo.stars}★ Photos`);

    addScore(charts_with_name(charts, dex), result.photo.score);
  }

  if (result.photodex) {
    const dex = result.photodex.dex.toString().padStart(3, "0");

    const charts = charts_in_groups(game, "Photodex Scores");

    addScore(charts_with_name(charts, dex), result.photodex.score);
  }
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects Photodex scores and 1-4★ Photos scores',
  ],
  limitations: [
    'does not detect scores from screen "Which would you like to keep?", only from screenshots inside the photodex',
  ],
};
