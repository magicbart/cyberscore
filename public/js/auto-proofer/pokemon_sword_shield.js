export function registerScores(result, addScore, game) {
  if (result.league) {
    const charts = charts_in_groups(game, "League Card");

    addScore(charts_with_name(charts, "Curry Dex"), result.league.curry_dex);
    addScore(charts_with_name(charts, "Best Rally Score"), result.league.best_rally_score);
    addScore(charts_with_name(charts, "Pokémon Caught"), result.league.pokemon_caught);
    addScore(charts_with_name(charts, "Shiny Pokémon Found"), result.league.shiny_pokemon_found);
  }

  if (result.pokedex) {
    const dex = `${result.pokedex.dex.toString().padStart(3, "0")}`;
    const region = {
      galar: "Galar",
      isle_of_armor: "Isle of Armor",
      crown_tundra: "Crown Tundra",
    }[result.pokedex.region];

    addScore(
      charts_with_name(charts_in_groups(game, `${region} Pokédex`), dex),
      result.pokedex.number_battled
    );
  }

  if (result.cover) {
    const charts = charts_in_groups(game, "Pokédex Completion");

    const galar = charts_with_name(charts, "Galar");
    const isle = charts_with_name(charts, "Isle");
    const tundra = charts_with_name(charts, "Tundra");

    addScore(galar,  result.cover.galar_caught,         result.cover.galar_seen);
    addScore(isle,   result.cover.isle_of_armor_caught, result.cover.isle_of_armor_seen);
    addScore(tundra, result.cover.crown_tundra_caught,  result.cover.crown_tundra_seen);
  }
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects league card scores',
    'detects pokédex caught/seen values from pokédex cover page',
    'detects number battled from pokédex pages',
  ],
  limitations: [
    '"number battled" detection only works with english pokémon names and some japanese names',
  ],
};
