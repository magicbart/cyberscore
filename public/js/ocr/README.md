# Cyberscore auto-proofer tool

This tool automatically detects scores from game screenshots.

Used in [Cyberscore](https://cyberscore.me.uk)

## Supported games

- Pokémon Legends: Arceus
- Pokémon Let's Go, Pikachu/Eevee!
- Pokémon Sword/Shield
- Picross S4
- New Pokémon Snap
- Pokémon Go

## Technical details

This uses a custom built OCR module that works by pattern matching symbols from
sample PNG files. It considers that characters are contiguous, so symbols like
`ジ` will be read as 5 different symbols. To work around this issue, the tool
uses a bunch of `str.replace` to combine those characters into a single one.
You can see this in action in Pokémon Sword/Shield.

## WASM limitations

wasm-bindgen doesn't support things like returning `Vec<T>`, or even tuples, so
the Proof objects may seem a bit weird thanks to that.
