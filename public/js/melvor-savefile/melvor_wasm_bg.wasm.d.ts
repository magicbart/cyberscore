/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_score_free(a: number): void;
export function __wbg_get_score_group(a: number, b: number): void;
export function __wbg_set_score_group(a: number, b: number, c: number): void;
export function __wbg_get_score_chart(a: number, b: number): void;
export function __wbg_set_score_chart(a: number, b: number, c: number): void;
export function __wbg_get_score_value1(a: number, b: number): void;
export function __wbg_set_score_value1(a: number, b: number, c: number): void;
export function __wbg_get_score_value2(a: number, b: number): void;
export function __wbg_set_score_value2(a: number, b: number, c: number): void;
export function scores_wasm(a: number, b: number, c: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_free(a: number, b: number, c: number): void;
export function __wbindgen_malloc(a: number, b: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number, d: number): number;
