function spaceship(a,b) {
  if (a < b) {
    return -1;
  } else if (b < a) {
    return 1;
  } else {
    return 0;
  }
}

function render_score_component(value, chartType) {
  if (value === null) {
    return "--";
  }

  switch (chartType) {
    case 1:
    case 2:
      const y = Math.floor(value / 60 / 60 / 24 / 365);
      const d = Math.floor(value / 60 / 60 / 24) % 365;
      const h = Math.floor(value / 60 / 60) % 24;
      const m = Math.floor(value / 60) % 60;
      const s = Math.floor(value) % 60;

      const parts = [];
      if (y > 0) { parts.push(`${y}y`); }
      if (d > 0) { parts.push(`${d}d`); }
      if (h > 0) { parts.push(`${h}h`); }
      if (m > 0) { parts.push(`${m}m`); }
      if (s > 0) { parts.push(`${s}s`); }
      return parts.join(" ");

    case 3:
    case 4:
      return `${value}`.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
  }
}

export function render_score(record, chart) {
  if (chart.chart_type2 !== 0) {
    return `${render_score_component(record.value1, chart.chart_type)} / ${render_score_component(record.value2, chart.chart_type2)}`
  } else {
    return `${render_score_component(record.value1, chart.chart_type)}`
  }
}

// -1: new record is worse
//  1: new record is better
//  0: it's the same record
export function compare_with_previous(new_record, chart) {
  // no previous record, anything is better than that
  if (chart.record.record_id === null) {
    return 1;
  }

  let ps1 = chart.record.submission;
  let ps2 = chart.record.submission2;

  let s1 = new_record.value1;
  let s2 = new_record.value2;

  if (chart.conversion_factor != 0) {
    if (ps1 === null && ps2 != null) { ps1 = (ps2 / chart.conversion_factor).toFixed(chart.num_decimals); }
    if (ps2 === null && ps1 != null) { ps2 = (ps1 * chart.conversion_factor).toFixed(chart.num_decimals2); }

    if (s1 === null && s2 != null) { s1 = (s2 / chart.conversion_factor).toFixed(chart.num_decimals); }
    if (s2 === null && s1 != null) { s2 = (s1 * chart.conversion_factor).toFixed(chart.num_decimals2); }
  }

  let cmp1 = spaceship(ps1, s1);
  let cmp2 = spaceship(ps2, s2);

  const ascending1 = (chart.chart_type  == 1 || chart.chart_type  == 3);
  const ascending2 = (chart.chart_type2 == 1 || chart.chart_type2 == 3);

  if (!ascending1) cmp1 = -cmp1;
  if (!ascending2) cmp2 = -cmp2;

  if (cmp1 != 0) return cmp1;
  return cmp2;
}
